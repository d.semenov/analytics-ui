import React from 'react';
import './AnalyticMain.css';
import { Button, Divider, Layout, Menu, Space, Tabs } from 'antd';
import Sider from 'antd/es/layout/Sider';
import { Content, Header } from 'antd/es/layout/layout';
import { Link, Route, Switch, withRouter } from 'react-router-dom';
import {
  GroupOutlined,
  BlockOutlined,
  FilterOutlined,
  DeploymentUnitOutlined,
  DiffOutlined,
  ExportOutlined,
  FileSearchOutlined,
  LeftOutlined,
  MenuUnfoldOutlined,
  PoweroffOutlined,
  RightOutlined,
  SearchOutlined,
  UnorderedListOutlined,
  AppstoreAddOutlined,
  FundOutlined,
  FileTextOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import Title from 'antd/es/typography/Title';

import WorkspaceModule from '../modules/workspace/workspaceModule';
import SearchDocModule from '../modules/searchDoc/searchDocModule';
import SearchTextModule from '../modules/searchText/searchTextModule';
import IndexingModule from '../modules/indexing/indexingModule';
import SearchEntitiesModule from '../modules/searchEntities/searchEntitiesModule';
import QualifierModule from '../modules/qualifier/qualifierModule';
import FiltersListModule from '../modules/filtersList/filtersListModule';
import FiltersCreateModule from '../modules/filtersCreate/filterCreateModule';
import ResultsModule from '../modules/results/resultsModule';
import { Redirect } from 'react-router-dom';
import RuleCheckReport from '../modules/ruleCheckReport/RuleCheckReport';

import logotype from './logotype.svg';

const { TabPane } = Tabs;

class EPDMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      isDrawerVisible: false,
      readOnly: 1,
      profileUser: false,
      selectedKeys: [],
      updateModal: false,
      testId: '',
      taskId: '',
      currentDoc: '',
      menuActual: [],
      unstructuredMenu: [],
    };
  }

  componentDidMount = () => {
    this.getMenu();
    this.setState({ selectedKeys: [this.props.location.pathname.replace('/', '')] });
  };
  getMenu = () => {
    //ApiDiContainer.ProxyApiMenu.menuByRole(id).then((res) => {
    var actual = [
      {
        id: 1,
        name: 'Документы',
        descr: 'документы',
        path: 'workspace',
        isActive: 1,
        parentId: null,
        icon: 'FileTextOutlined',
        num: 1,
        readOnly: null,
        child: [],
      },
      {
        id: 2,
        name: 'Антиплагиат',
        descr: 'Антиплагиат',
        path: 'antiplag',
        isActive: 1,
        parentId: null,
        icon: 'DiffOutlined',
        num: 2,
        readOnly: null,
        child: [
          {
            id: 21,
            name: 'Поиск документов',
            descr: '',
            path: 'bydoc',
            isActive: 1,
            parentId: 1,
            icon: 'FileSearchOutlined',
            num: 1,
            readOnly: null,
            child: [],
          },
          {
            id: 22,
            name: 'Поиск абзаца',
            descr: '',
            path: 'bytext',
            isActive: 1,
            parentId: 1,
            icon: 'SearchOutlined',
            num: 2,
            readOnly: null,
            child: [],
          },
        ],
      },
      {
        id: 2,
        name: 'Классификатор',
        descr: 'Классификация документов',
        path: 'qualifier',
        isActive: 1,
        parentId: null,
        icon: 'GroupOutlined',
        num: 1,
        readOnly: null,
        child: [],
      },
      {
        id: 3,
        name: 'Выявление сущностей',
        descr: 'Выявление сущностей в документе',
        path: 'entities',
        isActive: 1,
        parentId: null,
        icon: 'DeploymentUnitOutlined',
        num: 3,
        readOnly: null,
        child: [],
      },
      {
        id: 4,
        name: 'Настройка правил',
        descr: 'Настройка правил',
        path: 'filters',
        isActive: 1,
        parentId: null,
        icon: 'FilterOutlined',
        num: 1,
        readOnly: null,
        child: [
          {
            id: 41,
            name: 'Список правил',
            descr: 'Список правил',
            path: 'rules-list',
            isActive: 1,
            parentId: null,
            icon: 'UnorderedListOutlined',
            num: 1,
            readOnly: null,
            child: [],
          },
          {
            id: 42,
            name: 'Управление правилами',
            descr: 'Управление правилами',
            path: 'create-rule',
            isActive: 1,
            parentId: null,
            icon: 'AppstoreAddOutlined',
            num: 1,
            readOnly: null,
            child: [],
          },
        ],
      },
      {
        id: 4,
        name: 'Результаты анализа',
        descr: 'Результаты анализа',
        path: 'results',
        isActive: 1,
        parentId: null,
        icon: 'FundOutlined',
        num: 1,
        readOnly: null,
        child: [],
      },
    ];

    if (this.props.isAdmin) {
      actual.push({
        id: 5,
        name: 'Администрирование',
        descr: 'Администрирование',
        path: 'admin',
        isActive: 1,
        parentId: null,
        icon: 'SettingOutlined',
        num: 1,
        readOnly: null,
        child: [
          {
            id: 51,
            name: 'Индексация',
            descr: 'Индексация документов',
            path: 'indexing',
            isActive: 1,
            parentId: 5,
            icon: 'ExportOutlined',
            num: 1,
            readOnly: null,
            child: [],
          },
        ],
      });
    }

    this.setState({
      menuActual: actual,
    });

    var unstructured = [
      {
        id: 1,
        name: 'Антиплагиат',
        descr: 'Антиплагиат',
        path: 'aniplag',
        isActive: 1,
        parentId: null,
        icon: 'DiffOutlined',
        num: 1,
        readOnly: null,
        child: [],
      },
      {
        id: 11,
        name: 'Индексация',
        descr: 'Индексация документов',
        path: 'indexing',
        isActive: 1,
        parentId: 1,
        icon: 'ExportOutlined',
        num: 1,
        readOnly: null,
        child: [],
      },
      {
        id: 12,
        name: 'Поиск по документу',
        descr: '',
        path: 'bydoc',
        isActive: 1,
        parentId: 1,
        icon: 'FileSearchOutlined',
        num: 1,
        readOnly: null,
        child: [],
      },
      {
        id: 13,
        name: 'Поиск по абзацу',
        descr: '',
        path: 'bytext',
        isActive: 1,
        parentId: 1,
        icon: 'SearchOutlined',
        num: 1,
        readOnly: null,
        child: [],
      },
      {
        id: 2,
        name: 'Классификатор',
        descr: 'Классификация документов',
        path: 'qualifier',
        isActive: 1,
        parentId: null,
        icon: 'GroupOutlined',
        num: 1,
        readOnly: null,
        child: [],
      },
      {
        id: 3,
        name: 'Выявление сущностей',
        descr: 'Выявление сущностей в документе',
        path: 'entities',
        isActive: 1,
        parentId: null,
        icon: 'DeploymentUnitOutlined',
        num: 1,
        readOnly: null,
        child: [],
      },
      {
        id: 4,
        name: 'Настройка фильтров',
        descr: 'Настройка фильтров',
        path: 'filters',
        isActive: 1,
        parentId: null,
        icon: 'FilterOutlined',
        num: 1,
        readOnly: null,
        child: [
          {
            id: 41,
            name: 'Создать фильтр',
            descr: 'Создать фильтр',
            path: 'filters_list',
            isActive: 1,
            parentId: null,
            icon: 'UnorderedListOutlined',
            num: 1,
            readOnly: null,
            child: [],
          },
          {
            id: 42,
            name: 'Настройка фильтров',
            descr: 'Настройка фильтров',
            path: 'filters_create',
            isActive: 1,
            parentId: null,
            icon: 'AppstoreAddOutlined',
            num: 1,
            readOnly: null,
            child: [],
          },
        ],
      },
    ];

    unstructured.push(
      {
        id: 5,
        name: 'Администрирование',
        descr: 'Администрирование',
        path: 'admin',
        isActive: 1,
        parentId: null,
        icon: 'SettingOutlined',
        num: 1,
        readOnly: null,
        child: [],
      },
      {
        id: 51,
        name: 'Индексация',
        descr: 'Индексация документов',
        path: 'indexing',
        isActive: 1,
        parentId: 5,
        icon: 'ExportOutlined',
        num: 1,
        readOnly: null,
        child: [],
      },
    );

    this.setState({
      unstructuredMenu: unstructured,
    });
    //});
  };

  toogleCollapse = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  routeToMenuMap = (e) => {
    // route will affect the left panel
    ///
    this.props.history.push(e.key);
    ///
    //this.state.unstructuredMenu.map((el) => {
    this.state.menuActual.forEach((el) => {
      if (el.path) {
        if (el.child.length !== 0) {
          el.child.forEach((ch) => {
            if (this.props.history.location.pathname.replace('/', '') === ch.path) {
              if (ch.path === '') {
                this.setState({ selectedKeys: [] });
              }
              this.setState({ selectedKeys: [ch.path] });
            }
          });
        }
        if (this.props.history.location.pathname.replace('/', '') === el.path) {
          if (el.path === '') {
            this.setState({ selectedKeys: [] });
          }
          this.setState({ selectedKeys: [el.path] });
        }
      }
    });
  };

  ///
  testRedirect = (taskId, id) => {
    this.setState({ updateModal: true, testId: id, taskId: taskId });
  };
  ///
  updateTreeData = (arr) => {
    return arr.map((el) => {
      if (el.child.length > 0) {
        return (
          <Menu.SubMenu key={el.menuPath} title={el.label} icon={<BlockOutlined />}>
            {() => this.updateTreeData(el)}
          </Menu.SubMenu>
        );
      }
      return (
        <Menu.Item key={el.menuPath} icon={<MenuUnfoldOutlined />}>
          <Link
            to={el.menuPath}
            onClick={() => {
              //console.log('updateMenu', el.readOnly);
              this.props.history.push('/');
            }}>
            {el.label}
          </Link>
        </Menu.Item>
      );
    });
  };

  getIcon = (name) => {
    // console.log(name);
    switch (name) {
      case 'SearchOutlined':
        return <SearchOutlined />;
      case 'DiffOutlined':
        return <DiffOutlined />;
      case 'FilterOutlined':
        return <FilterOutlined />;
      case 'GroupOutlined':
        return <GroupOutlined />;
      case 'FileSearchOutlined':
        return <FileSearchOutlined />;
      case 'ExportOutlined':
        return <ExportOutlined />;
      case 'DeploymentUnitOutlined':
        return <DeploymentUnitOutlined />;
      case 'UnorderedListOutlined':
        return <UnorderedListOutlined />;
      case 'AppstoreAddOutlined':
        return <AppstoreAddOutlined />;
      case 'FundOutlined':
        return <FundOutlined />;
      case 'FileTextOutlined':
        return <FileTextOutlined />;
      case 'SettingOutlined':
        return <SettingOutlined />;
      default:
        return null;
    }
  };

  result = (arr) => {
    if (arr) {
      return arr.map((el) => {
        if (el.child.length) {
          return (
            <Menu.SubMenu
              key={el.path}
              title={el.name}
              children={el.child}
              icon={this.getIcon(el.icon)}>
              {this.result(el.child)}
            </Menu.SubMenu>
          );
        }
        return (
          <Menu.Item key={el.path} icon={this.getIcon(el.icon)}>
            <Link
              to={el.path}
              onClick={() => {
                this.props.history.push('/');
              }}>
              {el.name}
            </Link>
          </Menu.Item>
        );
      });
    }
  };

  updateCollasple = () => {
    this.setState({ collapsed: true });
  };

  setCurrentDoc = (currentDoc) => {
    this.setState({ currentDoc: currentDoc });
  };

  currentDocumentTitle = () => {
    let title = 'Выбранный документ: ';
    if (this.state.currentDoc === '') return <></>;

    title += this.state.currentDoc.docLabel;

    if (this.state.currentDoc.docId !== '') {
      title += ' (' + this.state.currentDoc.docId + ')';
    }
    return (
      <Title level={5} style={{ marginBottom: 0, marginRight: 50 }}>
        <Space>
          {title}
          <Button onClick={() => this.setCurrentDoc('')}>Скрыть</Button>
        </Space>
      </Title>
    );
  };

  render() {
    return (
      <>
        <Layout style={{ minWidth: 1200 }}>
          <Sider
            className={'site-layout-background sider-layout-left'}
            width={'20em'}
            breakpoint="xl"
            onBreakpoint={(hz) => {
              this.setState({ collapsed: hz });
            }}
            collapsed={this.state.collapsed}
            style={{
              overflow: 'auto',
              height: '100vh',
              position: 'fixed',
              left: 0,
            }}>
            <Link
              to={'/'}
              onClick={() => {
                this.props.history.push('/');
                this.setState({ selectedKeys: [] });
              }}>
              <Header
                className={
                  'site-layout-background header-layout ' +
                  (this.state.collapsed ? 'header-layout-collapsed' : 'header-layout-uncollapsed')
                }>
                <div hidden={!this.state.collapsed}>
                  { <img src={logotype} alt="logo" width={'40px'} /> }
                </div>
                <div hidden={this.state.collapsed}>
                  <div className="stylesLogo">
                    { <img src={logotype} alt="logo" width={'60px'} /> }
                    <Title level={4} className="titleLogo">
                      {'Росэнергоатом'}
                    </Title>
                  </div>
                </div>
              </Header>
            </Link>
            <Divider />
            <Content>
              <Menu
                collapsed="false"
                selectable={true}
                mode="inline"
                selectedKeys={this.state.selectedKeys}
                onClick={this.routeToMenuMap}
                forceSubMenuRender>
                {this.result(this.state.menuActual)}
              </Menu>
            </Content>
          </Sider>
          <Layout
            className={
              this.state.collapsed
                ? 'content-layout content-layout-collapsed'
                : 'content-layout content-layout-uncollapsed'
            }>
            <Header className={'content-layout-header site-layout-background'}>
              <div className={'title-box'}>
                {React.createElement(this.state.collapsed ? RightOutlined : LeftOutlined, {
                  className: 'trigger',
                  style: { fontSize: '35px' },
                  onClick: this.toogleCollapse,
                })}
                <Title level={4} style={{ marginBottom: 0 }}>
                  Аналитические инструменты
                </Title>
              </div>
              <Space style={{ marginLeft: 20 }}></Space>
              <Space style={{ float: 'right', alignContent: 'center' }}>
                {this.currentDocumentTitle()}
                <Link to={'/'}>
                  <Button
                    shape={'circle'}
                    size="large"
                    icon={<PoweroffOutlined />}
                    onClick={this.props.logout}></Button>
                </Link>
              </Space>
            </Header>
            <Content>
              <Content style={{ padding: 27 }}>
                <Switch>
                  <Route
                    exact
                    path={this.props.match.path + 'workspace'}
                    render={(props) => (
                      <WorkspaceModule
                        {...props}
                        user={this.props.user}
                        logged={this.props.exitUser}
                        setCurrentDoc={this.setCurrentDoc}
                      />
                    )}
                  />
                  <Route
                    exact
                    path={this.props.match.path + 'workspace/:docId'}
                    render={(props) => (
                      <WorkspaceModule
                        {...props}
                        user={this.props.user}
                        logged={this.props.exitUser}
                        setCurrentDoc={this.setCurrentDoc}
                      />
                    )}
                  />
                  <Route
                    exact
                    path={this.props.match.path + 'bydoc'}
                    render={(props) => (
                      <SearchDocModule
                        {...props}
                        user={this.props.user}
                        logged={this.props.exitUser}
                        currentDoc={this.state.currentDoc}
                      />
                    )}
                  />
                  <Route
                    exact
                    path={this.props.match.path + 'indexing'}
                    render={(props) => (
                      <IndexingModule
                        {...props}
                        user={this.props.user}
                        logged={this.props.exitUser}
                      />
                    )}
                  />
                  <Route
                    exact
                    path={this.props.match.path + 'bytext'}
                    render={(props) => (
                      <SearchTextModule
                        {...props}
                        user={this.props.user}
                        logged={this.props.exitUser}
                        currentDoc={this.state.currentDoc}
                      />
                    )}
                  />
                  <Route
                    exact
                    path={this.props.match.path + 'qualifier'}
                    render={(props) => (
                      <QualifierModule
                        {...props}
                        user={this.props.user}
                        isAdmin={this.props.isAdmin}
                        logged={this.props.exitUser}
                        currentDoc={this.state.currentDoc}
                      />
                    )}
                  />
                  <Route
                    exact
                    path={this.props.match.path + 'entities'}
                    render={(props) => (
                      <SearchEntitiesModule
                        {...props}
                        user={this.props.user}
                        logged={this.props.exitUser}
                        currentDoc={this.state.currentDoc}
                      />
                    )}
                  />
                  <Route
                    exact
                    path={this.props.match.path + 'rules-list'}
                    render={(props) => (
                      <FiltersListModule
                        {...props}
                        user={this.props.user}
                        logged={this.props.exitUser}
                        lastUsedRoleId={this.state.lastUsedRoleId || this.props.lastUsedRoleId}
                      />
                    )}
                  />
                  <Route
                    exact
                    path={this.props.match.path + 'create-rule'}
                    render={(props) => (
                      <FiltersCreateModule
                        {...props}
                        user={this.props.user}
                        logged={this.props.exitUser}
                        lastUsedRoleId={this.state.lastUsedRoleId || this.props.lastUsedRoleId}
                      />
                    )}
                  />
                  <Route
                    exact
                    path={this.props.match.path + 'results'}
                    render={(props) => (
                      <ResultsModule
                        {...props}
                        user={this.props.user}
                        logged={this.props.exitUser}
                        lastUsedRoleId={this.state.lastUsedRoleId || this.props.lastUsedRoleId}
                      />
                    )}
                  />
                  <Route
                    exact
                    path={this.props.match.path + 'rule_report'}
                    render={(props) => (
                      <RuleCheckReport
                        {...props}
                        user={this.props.user}
                        logged={this.props.logged}
                        lastUsedRoleId={this.state.lastUsedRoleId || this.props.lastUsedRoleId}
                      />
                    )}
                  />
                  <Redirect to={this.props.match.path + 'workspace'} />
                </Switch>
              </Content>
            </Content>
          </Layout>
        </Layout>
      </>
    );
  }
}

export default withRouter(EPDMain);
