import React from 'react';
import styles from './indexing.module.css';
import {
  Button,
  Card,
  Divider,
  Input,
  message,
  Modal,
  Row,
  Col,
  Space,
  Table,
  DatePicker,
  Tooltip,
} from 'antd';
import Title from 'antd/es/typography/Title';
import {
  PlusOutlined,
  SearchOutlined,
  SyncOutlined,
  DownloadOutlined,
  FileTextOutlined,
} from '@ant-design/icons';
import Highlighter from 'react-highlight-words';
import UploadFileFormModal from '../../Forms/uploadFileForm/uploadFileFormModal';
import ApiDiContainer from '../../../service/apiService/apiDiContainer';

import 'moment/locale/ru';
import ru_RU from 'antd/es/date-picker/locale/ru_RU';

class IndexingModule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      docs: [],
      totalDocs: 0,
      columns: [],
      docLabelHighlighting: '',
      docSourceHighlighting: '',
      loading: false,
      indexing: false,
      dateIndexing: false,
      indexDate: '',
      uploadModal: false,
      viewText: false,
      textLoading: false,
      docText: '',
      docCountIndexed: 0,
      docCountInDB: 0,
      indexStatus: '',
      indexingUpdateInterval: null,
    };
  }

  pagination = {
    current: 1,
    pageSize: 10,
    total: 10,
  };
  filter = {};
  sorter = {};

  componentDidMount() {
    this.getDocsPage(1, 10, 'none', 'none', '');
    this.getIndexedDocs();
    this.getTotalDocs();
    this.updateIndexingStatus();
  }

  componentWillUnmount() {
    clearInterval(this.state.indexingUpdateInterval);
  }

  getIndexedDocs = () => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getDocsCountIndexed()
      .then((response) => {
        this.pagination = { ...this.pagination, total: response.data };
        this.setState({ docCountIndexed: response.data, loading: false });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
            /*message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных о количестве проиндексированных документов');

          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных о количестве проиндексированных документов');
          console.log('Error', error.message);
        }
        this.setState({ loading: false });
      });
  };

  getTotalDocs = () => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getDocsCountInDb()
      .then((response) => {
        this.setState({ docCountInDB: response.data });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
            /*message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных о количестве документов в БД');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных о количестве документов в БД');
          console.log('Error', error.message);
        }
        this.setState({ loading: false });
      });
  };

  getOriginalDoc = (id) => {
    var request = {
      docId: id,
    };
    ApiDiContainer.ProxyApiClickHouse.getOriginalDoc(request)
      .then((response) => {
        const win = window.open(response.data, '_blank');
        if (win != null) {
          win.focus();
        }
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных из БД');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных из БД');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  setUploadModal = (visible) => {
    this.setState({
      uploadModal: visible,
    });
  };

  // фильтры для таблицы
  tableSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Введите запрос`}
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: 'block',
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}>
            Поиск
          </Button>
          <Button
            onClick={() => this.handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}>
            Сброс
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },

    render: (text) => {
      return (
        <Highlighter
          highlightStyle={{
            backgroundColor: '#ffc069',
            padding: 0,
          }}
          searchWords={this.getHighlightingText(dataIndex)}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      );
    },
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    this.filter[dataIndex] = selectedKeys[0];
    confirm();
    this.setHighlightingText(dataIndex, selectedKeys[0]);
  };

  handleReset = (clearFilters, confirm, dataIndex) => {
    for (const filterLabel of Object.keys(this.filter)) {
      this.filter[filterLabel] = null;
    }
    clearFilters();
    confirm();
    this.setHighlightingText(dataIndex, '');
  };

  getHighlightingText = (dataIndex) => {
    switch (dataIndex) {
      case 'docLabel':
        return this.state.docLabelHighlighting !== '' ? [this.state.docLabelHighlighting] : [];
      case 'docSource':
        return this.state.docSourceHighlighting !== '' ? [this.state.docSourceHighlighting] : [];
      default:
        return [];
    }
  };

  setHighlightingText = (dataIndex, text) => {
    switch (dataIndex) {
      case 'docLabel':
        this.setState({
          docLabelHighlighting: text,
        });
        break;
      case 'docSource':
        this.setState({
          docSourceHighlighting: text,
        });
        break;
      default:
        break;
    }
  };

  updateDocsPage = (pagination, filter, sorter) => {
    this.pagination = pagination;
    this.sorter = sorter;

    let searchParams = [];

    for (const filterLabel of Object.keys(filter)) {
      if (filter[filterLabel] === null) continue;
      searchParams.push({
        searchKey: filterLabel,
        searchValue: filter[filterLabel][0],
      });
    }

    let sortField = sorter.field !== undefined ? sorter.field : 'none';
    let sortOrder = sorter.order !== undefined ? sorter.order : 'none';

    this.getDocsPage(
      this.pagination.current,
      this.pagination.pageSize,
      sortField,
      sortOrder,
      searchParams,
    );
  };

  getDocsPage = (pageNumber, pageSize, sortField, sortOrder, searchParams) => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getDocsPage(
      pageNumber,
      pageSize,
      sortField,
      sortOrder,
      searchParams,
    )
      .then((response) => {
        let columns = [
          // {
          //   dataIndex: 'docId',
          //   title: 'Идентификатор',
          //   ...this.tableSearchProps('docId'),
          //   width: 160,
          //   sorter: (a, b) => {
          //     return a['docId'] > b['docId'] ? 1 : -1;
          //   },
          // },
          {
            dataIndex: 'docLabel',
            title: 'Название',
            ...this.tableSearchProps('docLabel'),
            sorter: (a, b) => {
              return a['docLabel'] > b['docLabel'] ? 1 : -1;
            },
          },
          {
            dataIndex: 'docFile',
            title: 'Имя файла',
          },
          {
            dataIndex: 'docSource',
            title: 'Источник',
            ...this.tableSearchProps('docSource'),
            width: 260,
            sorter: (a, b) => {
              return a['docSource'] > b['docSource'] ? 1 : -1;
            },
          },
          {
            dataIndex: 'link',
            title: 'Ссылка',
            width: '6%',
            render: (_, document) =>
              'docId' in document && document.docId !== '' ? (
                <Button
                  type="link"
                  icon={<DownloadOutlined />}
                  onClick={() => {
                    this.getOriginalDoc(document.docId); // message.info('Сервис которого еще нет (Скачивание: ' + document.docLabel + ')');
                  }}>
                  Скачать
                </Button>
              ) : null,
          },
          {
            dataIndex: 'actions',
            title: 'Действия',
            width: 120,
            render: (_, document) => (
              <Space>
                <Tooltip title="Просмотр документа">
                  <Button
                    style={{ color: '#31aa2d' }}
                    shape="circle"
                    type="text"
                    icon={<FileTextOutlined />}
                    onClick={() => {
                      this.setState({ textLoading: true });
                      ApiDiContainer.ProxyApiClickHouse.getDoc({
                        docId: document.docId,
                      })
                        .then((response) => {
                          this.setState({
                            docText: response.data,
                            textLoading: false,
                            viewText: true,
                          });
                        })
                        .catch((error) => {
                          if (error.response) {
                            if (error.response.status === 403) {
                              this.props.logged();
                              /*message.error('Сессия закрыта');
                            this.setState({
                              loading: false,
                              logged: false,
                            });*/
                            }
                            if (error.response.status === 500) {
                              message.error(error.response.data);
                            }
                          } else if (error.request) {
                            message.error('Ошибка при загрузке текста документа');
                            console.log(error.request);
                          } else {
                            message.error('Ошибка при загрузке текста документа');
                            console.log('Error', error.message);
                          }
                          this.setState({ textLoading: false });
                        });
                    }}></Button>
                </Tooltip>
              </Space>
            ),
          },
        ];

        this.setState({
          columns: columns,
          docs: response.data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
            /*message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке страницы документов');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке страницы документов');
          console.log('Error', error.message);
        }
        this.setState({ loading: false });
      });
  };

  indexDocs = () => {
    if (this.state.indexingUpdateInterval !== null) {
      clearInterval(this.state.indexingUpdateInterval);
    }

    ApiDiContainer.ProxyApiAnalytics.reindex()
      .then(() => {
        this.updateIndexingStatus();
        let interval = setInterval(() => this.updateIndexingStatus(), 60000);
        this.setState({ indexingUpdateInterval: interval });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
            /*message.error('Сессия закрыта');
          this.setState({
            loading: false,
            logged: false,
          });*/
          }

          if (error.response.status === 500) {
            message.error('Процесс переиндексации уже запущен');
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных о переиндексации');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных о переиндексации');
          console.log('Error', error.message);
        }
      });
  };

  changeIndexDate = (date, dateString) => {
    this.setState({
      indexDate: dateString,
    });
  };

  dateIndexDocs = () => {
    console.log(this.state.indexDate);
  };

  updateIndexingStatus = () => {
    ApiDiContainer.ProxyApiAnalytics.indexStatus()
      .then((response) => {
        let info = response.data;
        let text =
          'Переиндексация' +
          (info.work ? ' ' : ' не ') +
          'запущена. Проиндексировано документов ' +
          info.indexedDocs +
          ' из ' +
          info.totalDocs +
          '.';

        if (info.work) {
          if (this.state.indexingUpdateInterval !== null) {
            clearInterval(this.state.indexingUpdateInterval);
          }
          let interval = setInterval(() => this.updateIndexingStatus(), 60000);
          this.setState({
            indexStatus: text,
            indexing: info.work,
            indexingUpdateInterval: interval,
          });
        } else {
          this.setState({ indexStatus: text, indexing: info.work });
        }
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при проверке статуса индексации');
          console.log(error.request);
        } else {
          message.error('Ошибка при проверке статуса индексации');
          console.log('Error', error.message);
        }
      });
  };

  resetState = () => {
    this.setState({
      blocks: null,
      code: '',
      id: '',
      isSystem: 0,
      name: '',
    });
  };

  render() {
    const { columns, docs, uploadModal } = this.state;

    return (
      <Card style={{ minHeight: 'calc(100vh - 140px)' }}>
        <div style={{ display: 'grid' }}>
          <Title>Индексированные документы</Title>
          <Row justify="space-between" align="middle" style={{ marginBottom: 16 }}>
            <Col>
              {this.state.indexStatus.includes('не') ? (
                <Title level={5} style={{ color: '#a12e12' }}>
                  {this.state.indexStatus}
                </Title>
              ) : (
                <Title level={5} style={{ color: '#31aa2d' }}>
                  {this.state.indexStatus}
                </Title>
              )}
            </Col>
            <Col>
              <Space>
                <DatePicker locale={ru_RU} format={'DD.MM.YYYY'} onChange={this.changeIndexDate} />
                <Button
                  disabled={this.state.dateIndexing || this.state.indexDate === ''}
                  icon={<SyncOutlined spin={this.state.dateIndexing} />}
                  onClick={this.dateIndexDocs}
                  className={styles.brownButton}>
                  {' '}
                  {this.state.dateIndexing ? 'Индексация по дате' : 'Переиндексировать по дате'}
                </Button>
                <Button
                  disabled={this.state.indexing}
                  icon={<SyncOutlined spin={this.state.indexing} />}
                  onClick={this.indexDocs}
                  className={styles.brownButton}>
                  {' '}
                  {this.state.indexing ? 'Индексация ' : 'Переиндексировать'}
                </Button>
                <Button
                  type="dashed"
                  icon={<SyncOutlined spin={this.state.loading} />}
                  onClick={() => this.getDocsPage(1, 10, 'none', 'none', '')}
                  className={styles.greenButton}>
                  {' '}
                  {this.state.loading ? 'Обновление' : 'Обновить'}
                </Button>
                <Button
                  type="primary"
                  icon={<PlusOutlined />}
                  onClick={() => {
                    this.resetState();
                    this.setState({
                      uploadModal: true,
                    });
                  }}>
                  Добавить
                </Button>
              </Space>

              <UploadFileFormModal visible={uploadModal} setVisible={this.setUploadModal} />
            </Col>
          </Row>
          <Table
            rowKey="id"
            columns={columns}
            dataSource={docs}
            loading={this.state.loading}
            pagination={docs.length > 10 ? this.pagination : false}
            onChange={(pagination, filter, sort) => this.updateDocsPage(pagination, filter, sort)}
          />
        </div>
        <Modal
          title={'Просмотр'}
          open={this.state.viewText}
          width={1500}
          onOk={() => {
            this.setState({
              viewText: false,
              id: '',
              docText: '',
            });
          }}
          onCancel={() => {
            this.setState({
              viewText: false,
              id: '',
              docText: '',
            });
          }}
          footer={[
            <Button
              key="back"
              onClick={() =>
                this.setState({
                  viewText: false,
                  id: '',
                  docText: '',
                })
              }>
              Закрыть
            </Button>,
          ]}>
          <Divider orientation="">Содержимое документа</Divider>
          {this.state.textLoading ? (
            <Card loading={true} />
          ) : (
            <Row gutter={16}>{this.state.docText}</Row>
          )}
        </Modal>
      </Card>
    );
  }
}

export default IndexingModule;
