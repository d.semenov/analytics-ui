import React from 'react';
import styles from './rubric.module.css';
import { Card } from 'antd';
import { withRouter } from 'react-router';
import Title from 'antd/es/typography/Title';

class RubricModule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <Card>
        <div style={{ display: 'grid' }}>
          <div className={styles.centerTitle}>
            <Title style={{ marginBottom: '0' }}>Рубрикация</Title>
          </div>
          <div>
            <p>Страница в процессе создания</p>
          </div>
        </div>
      </Card>
    );
  }
}

export default withRouter(RubricModule);
