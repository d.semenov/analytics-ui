import React from 'react';
import styles from './searchText.module.css';
import {
  Button,
  Card,
  DatePicker,
  Divider,
  Col,
  Input,
  message,
  Modal,
  Spin,
  Row,
  Select,
  Radio,
  Space,
  Table,
  Tooltip,
  Upload,
  Typography,
} from 'antd';
import { withRouter } from 'react-router';
import moment from 'moment';
import Title from 'antd/es/typography/Title';
import {
  EyeOutlined,
  FilePdfOutlined,
  FileWordOutlined,
  SaveOutlined,
  SearchOutlined,
  UploadOutlined,
  UnorderedListOutlined,
  DoubleLeftOutlined,
  DoubleRightOutlined,
  LeftOutlined,
  RightOutlined,
} from '@ant-design/icons';
import Highlighter from 'react-highlight-words';
import ApiDiContainer from '../../../service/apiService/apiDiContainer';
import ParagraphDirectoryModal from '../paragraphDirectoryModal/paragraphDirectoryModal';

const { Option } = Select;

const { RangePicker } = DatePicker;

class SearchTextModule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      docs: [],
      duplicates: [],
      columns: [],
      types: [],
      sources: [],
      //
      loading: false,
      viewText: false,
      sourceText: '',
      currentDoc: '',
      currentSource: [],
      currentType: [],
      fromDate: '',
      toDate: '',
      searchDoc: '',
      destText: '',
      paragraphTitle: '',
      paragraphAccess: '',
      searchText: '',
      actionDoc: '',
      uploadModal: false,
      paragraphIncludeSearchFlag: true,
      paragraphDirectoryModal: false,
      saveParagraphModal: false,
      fileList: [],
      uploading: false,
      searchLaunched: false,
      //
      actionText: '',
      currentParagraph: 0,
      actionMarkup: [],
      searchMarkup: [],
    };
  }

  handleUpload = () => {
    const { fileList } = this.state;
    const formData = new FormData();

    formData.append('file', fileList[0]);

    this.setState({
      uploading: true,
    });

    ApiDiContainer.ProxyApiClickHouse.textFromFile(formData)
      .then((response) => {
        this.setState({
          uploading: false,
        });

        this.setState({
          fileList: [],
          searchText: response.data,
          uploadModal: false,
        });
        message.success('Текст извлечен');
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            message.error('Сессия закрыта');
            this.props.logged();
            /*this.setState({
              loading: false
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка извлечения текста');
          console.log(error.request);
        } else {
          message.error('Ошибка извлечения текста');
          console.log('Error', error.message);
        }
        this.setState({
          uploading: false,
        });
      });
  };

  onRemove = (file) => {
    this.setState({
      fileList: [],
    });
  };

  iconRender = (file) => {
    if (file.name.indexOf('pdf') > 0) {
      return <FilePdfOutlined />;
    }

    if (file.name.indexOf('docx') > 0) {
      return <FileWordOutlined />;
    }
  };

  beforeUpload = (file) => {
    if (file.name.indexOf('pdf') > 0 || file.name.indexOf('docx') > 0) {
      this.setState({
        fileList: [file],
      });
    } else message.error(`${file.name} поддерживаются файлы с расширением docx или pdf`);
    return false;
  };

  componentDidMount() {
    this.setState({
      currentDoc: this.props.currentDoc,
    });

    this.getTypesList();
    this.getSourcesList();
    this.getDocs();
  }

  getTypesList = () => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getTypesList()
      .then((response) => {
        this.setState({
          types: response.data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            message.error('Сессия закрыта');
            this.props.logged();
            /*this.setState({
              loading: false
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  getSourcesList = () => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getSourcesList()
      .then((response) => {
        this.setState({
          sources: response.data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            message.error('Сессия закрыта');
            this.props.logged();
            /*this.setState({
              loading: false
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  getDocs = () => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getDocs()
      .then((response) => {
        this.setState({
          docs: response.data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            message.error('Сессия закрыта');
            this.props.logged();
            /*this.setState({
              loading: false
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  getDuplicates = () => {
    this.setState({ loading: true, searchLaunched: true });
    ApiDiContainer.ProxyApiClickHouse.findText({
      docId: this.state.currentDoc.docId,
      percent: 40,
      include: this.state.paragraphIncludeSearchFlag,
      text: this.state.searchText,
      sourcesFilter: this.state.currentSource.map((el) => el.children),
      typesFilter: this.state.currentType.map((el) => el.children),
      fromDate: this.state.fromDate,
      toDate: this.state.toDate,
    })
      .then((response) => {
        let columns = [
          // {
          //   dataIndex: 'destDocId',
          //   title: 'Идентификатор',
          // },
          {
            dataIndex: 'destDocLabel',
            title: 'Название',
          },
          {
            dataIndex: 'destDocFile',
            title: 'Файл',
          },
          {
            dataIndex: 'destDocSource',
            title: 'Источник',
          },
          {
            dataIndex: 'destDocType',
            title: 'Тип',
          },
          {
            dataIndex: 'destDocDate',
            title: 'Дата документа',
            render: (date) => {
              function transformDate(d) {
                var dateTime = d.split('T');
                var date = dateTime[0].split('-');
                return date[2] + '.' + date[1] + '.' + date[0] + ' ' + dateTime[1];
              }
              return <Space>{transformDate(date)}</Space>;
            },
          },
        ];
        let actions = {
          dataIndex: 'action',
          title: 'Действия',
          width: 140,
          render: (_, document) => (
            <Space>
              <Tooltip title="Просмотр документа">
                <Button
                  type="primary"
                  size="small"
                  shape="circle"
                  ghost
                  onClick={() => {
                    this.setState(
                      {
                        actionDoc: document,
                        viewText: true,
                      },
                      () => {
                        this.getDocText(document);
                      },
                    );
                  }}>
                  <EyeOutlined />
                </Button>
              </Tooltip>
            </Space>
          ),
        };
        if (this.state.paragraphIncludeSearchFlag) {
          columns.push(actions);
        }

        this.setState({
          columns: columns,
          duplicates: response.data,
          loading: false,
        });
        console.log(response.data);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            message.error('Сессия закрыта');
            this.props.logged();
            /*this.setState({
              loading: false
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных ');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  getDocText = (document) => {
    this.setState({ actionTextLoading: true });
    ApiDiContainer.ProxyApiClickHouse.getDoc({ docId: document.destDocId })
      .then((response) => {
        let markupSearchArray = [];
        let markupActionArray = [];
        this.state.actionDoc.items.forEach((el) => {
          markupSearchArray.push({ start: el.sourceStart, stop: el.sourceStop });
          markupActionArray.push({ start: el.destStart, stop: el.destStop });
        });
        this.setState({
          searchMarkup: markupSearchArray,
          actionMarkup: markupActionArray,
          actionText: response.data.replace(/(\r\n|\n|\r)/gm, ' '),
          actionTextLoading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            message.error('Сессия закрыта');
            this.props.logged();
            /*this.setState({
              loading: false
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных документа');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных документа');
          console.log('Error', error.message);
        }
        this.setState({
          actionTextLoading: false,
        });
      });
  };

  createMarkup = (text, coords, prefix) => {
    let shift = 0;
    let id = 0;

    for (const i of coords) {
      let markHead = '<mark-text id="' + prefix + '-' + id + '">';
      let markTail = '</mark-text>';
      text = [text.slice(0, i.start + shift), markHead, text.slice(i.start + shift)].join('');
      shift += markHead.length;
      text = [text.slice(0, i.stop + shift), markTail, text.slice(i.stop + shift)].join('');
      shift += markTail.length;
      id++;
    }

    return { __html: text };
  };

  scrollTo = (prefix, id) => {
    let currentMark = document.getElementById(prefix + '-' + id);
    if (currentMark !== null) currentMark.scrollIntoView({ block: 'center', behavior: 'smooth' });
  };

  setUploadModal = (visible) => {
    this.setState({
      uploadModal: visible,
    });
  };

  setSaveParagraphModal = (visible) => {
    this.setState({
      saveParagraphModal: visible,
    });
  };

  setParagraphDirectoryModal = (visible) => {
    this.setState({
      paragraphDirectoryModal: visible,
    });
  };

  updateCurrentDoc = (el, selector) => {
    let currentDoc = '';
    if (selector !== undefined)
      currentDoc = this.state.docs.filter((el) => el.docId === selector.value)?.pop();
    this.setState({
      currentDoc: currentDoc,
      searchDoc: '',
    });
  };

  setParagraphTitle = (title) => {
    this.setState({
      paragraphTitle: title,
    });
  };

  setParagraphAccess = (access) => {
    this.setState({
      paragraphAccess: access,
    });
  };

  setParagraphIncludeSearchFlag = (flag) => {
    this.setState({
      paragraphIncludeSearchFlag: flag,
    });
  };

  saveParagraph = () => {
    if (this.state.paragraphTitle.length === 0) {
      message.error('Название абзаца не заполнено');
      this.setParagraphTitle('');
      this.setParagraphAccess('');
      return;
    }
    if (this.state.paragraphAccess.length === 0) {
      message.error('Параметры доступа к абзацу не выбраны');
      this.setParagraphTitle('');
      this.setParagraphAccess('');
      return;
    }
    if (this.state.searchText.length === 0) {
      message.error('Текст абзаца не заполнен');
      this.setParagraphTitle('');
      this.setParagraphAccess('');
      return;
    }
    let paragraph = {
      title: this.state.paragraphTitle,
      text: this.state.searchText,
      user: this.state.paragraphAccess,
    };

    ApiDiContainer.ProxyApiClickHouse.addParagraph(paragraph)
      .then((response) => {
        message.success('Абзац успешно добавлен');
        this.setParagraphTitle('');
        this.setParagraphAccess('');
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            message.error('Сессия закрыта');
            this.props.logged();
            /*this.setState({
              loading: false
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при сохранении нового абзаца');
          console.log(error.request);
        } else {
          message.error('Ошибка при сохранении нового абзаца');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });

        this.setParagraphTitle('');
        this.setParagraphAccess('');
      });
  };

  onChangeDateTimeFilter = (dates, dateStrings) => {
    this.setState({
      searchLaunched: false,
      fromDate: dateStrings[0],
      toDate: dateStrings[1],
    });
  };

  updateCurrentSource = (el, selector) => {
    this.setState({
      searchLaunched: false,
      currentSource: selector,
    });
  };

  updateCurrentType = (el, selector) => {
    this.setState({
      searchLaunched: false,
      currentType: selector,
    });
  };

  resetState = () => {
    this.setState({
      duplicates: [],
      searchLaunched: false,
    });
  };

  // фильтры для таблицы
  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Введите запрос`}
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: 'block',
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}>
            Поиск
          </Button>
          <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Сброс
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },

    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: '#ffc069',
            padding: 0,
          }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  onSearch = () => {
    if (this.state.searchText.length === 0) {
      message.warning('Пожалуйста, заполните основное поле текстом');
      return;
    }
    if (this.state.searchText.length < 2000) {
      message.warning('Длина текста для поиска должна быть более 2000 символов');
      return;
    }
    this.resetState();
    this.getDuplicates();
  };

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  handleSearchSelect = (value) => {
    if (value) {
      this.setState({ searchDoc: value });
    } else {
      this.setState({ searchDoc: '' });
    }
  };

  setSearchText = (text) => {
    this.setState({
      searchText: text,
    });
  };

  render() {
    const {
      columns,
      duplicates,
      fileList,
      searchText,
      docs,
      currentDoc,
      uploading,
      viewText,
      searchLaunched,
    } = this.state;
    const { TextArea } = Input;
    const { Text } = Typography;
    return (
      <Card style={{ minHeight: 'calc(100vh - 140px)' }}>
        <div style={{ display: 'grid' }}>
          <Title>Поиск абзаца в документах</Title>
          <Row>
            <Col span={18} style={{ paddingRight: 20 }}>
              <TextArea
                showCount
                style={{ height: 500 }}
                value={searchText}
                onChange={(event) =>
                  this.setState({
                    searchText: event.target.value,
                  })
                }
                placeholder={'Получите из документа или вставьте текст для поиска в текущее поле'}
              />
            </Col>
            <Col span={6}>
              <Divider orientation="center">Искать в документе</Divider>
              <Select
                mode="single"
                showSearch
                allowClear={this.state.currentDoc !== ''}
                filterOption={false}
                onSearch={this.handleSearchSelect}
                notFoundContent={null}
                style={{ width: '100%', minWidth: 240 }}
                value={
                  currentDoc !== ''
                    ? currentDoc.docLabel +
                      (currentDoc.docId === '' ? '' : '(' + currentDoc.docId + ')')
                    : 'Не выбрано'
                }
                onChange={this.updateCurrentDoc}>
                {docs
                  .filter(
                    (el) =>
                      el.docId.toLowerCase().includes(this.state.searchDoc.toLowerCase() || '') ||
                      el.docLabel.toLowerCase().includes(this.state.searchDoc.toLowerCase() || ''),
                  )
                  .sort((a, b) => (a.docId > b.docId ? 1 : -1))
                  .map((el) => (
                    <Option key={el.docId} value={el.docId}>
                      {el.docLabel} ({el.docId})
                    </Option>
                  ))}
              </Select>
              <Divider orientation="center">Получить текст</Divider>
              <Row justify="space-around">
                <Col>
                  <Button
                    color="gray-6"
                    style={{ width: 150 }}
                    icon={<UploadOutlined />}
                    onClick={() => this.setUploadModal(true)}>
                    Из файла
                  </Button>
                </Col>
                <Col>
                  <Button
                    color="gray-6"
                    style={{ minWidth: 150 }}
                    icon={<UnorderedListOutlined />}
                    onClick={() => this.setParagraphDirectoryModal(true)}>
                    Из справочника
                  </Button>
                  <ParagraphDirectoryModal
                    visible={this.state.paragraphDirectoryModal}
                    setVisible={this.setParagraphDirectoryModal}
                    setText={this.setSearchText}
                    {...this.props}
                  />
                </Col>
              </Row>
              <Divider orientation="center">Фильтры на документы</Divider>
              <Space align="left" direction="vertical">
                <Row align="middle">
                  <Col span={7}>Источник</Col>
                  <Col span={17}>
                    <Select
                      mode="multiple"
                      allowClear
                      showSearch
                      optionFilterProp="children"
                      filterOption={(input, option) =>
                        option.props.children.toLowerCase().includes(input.toLowerCase())
                      }
                      style={{ width: '100%', minWidth: 200 }}
                      onChange={this.updateCurrentSource}
                      placeholder="Из всех источников">
                      {this.state.sources.map((t) => (
                        <Option key={t.filterItemId} value={t.filterItemId}>
                          {t.filterValue}
                        </Option>
                      ))}
                    </Select>
                  </Col>
                </Row>
                <Row align="middle">
                  <Col span={7}>Тип</Col>
                  <Col span={17}>
                    <Select
                      mode="multiple"
                      allowClear
                      showSearch
                      optionFilterProp="children"
                      filterOption={(input, option) =>
                        option.props.children.toLowerCase().includes(input.toLowerCase())
                      }
                      style={{ width: '100%', minWidth: 200 }}
                      onChange={this.updateCurrentType}
                      placeholder="Для всех типов">
                      {this.state.types.map((t) => (
                        <Option key={t.filterItemId} value={t.filterItemId}>
                          {t.filterValue}
                        </Option>
                      ))}
                    </Select>
                  </Col>
                </Row>
                <Row align="middle">
                  <Col span={7}>Дата добавления</Col>
                  <Col span={17}>
                    <RangePicker
                      onChange={this.onChangeDateTimeFilter}
                      ranges={{
                        Сегодня: [moment(), moment()],
                        Месяц: [moment().startOf('month'), moment().endOf('month')],
                        Год: [moment().startOf('year'), moment()],
                      }}
                    />
                  </Col>
                </Row>
                <Row align="middle">
                  <Col span={7}>Абзац</Col>
                  <Col span={17}>
                    <Radio.Group
                      onChange={(e) => this.setParagraphIncludeSearchFlag(e.target.value)}
                      value={this.state.paragraphIncludeSearchFlag}>
                      <Space direction="vertical">
                        <Radio value={true}>Содержится в документах</Radio>
                        <Radio value={false}>Не содержится в документах</Radio>
                      </Space>
                    </Radio.Group>
                  </Col>
                </Row>
                <br />
                <Row justify="space-between">
                  <Button
                    style={{ marginBottom: '10px' }}
                    color="gray-6"
                    icon={<SaveOutlined />}
                    disabled={this.state.searchText.length === 0}
                    onClick={() => this.setSaveParagraphModal(true)}>
                    Сохранить текст
                  </Button>
                  <Modal
                    title={'Сохранить новый абзац'}
                    centered
                    width={500}
                    closable={false}
                    visible={this.state.saveParagraphModal}
                    okText={'Добавить абзац'}
                    onOk={() => {
                      this.saveParagraph();
                      this.setSaveParagraphModal(false);
                    }}
                    onCancel={() => {
                      this.setSaveParagraphModal(false);
                    }}>
                    <Row align="middle" gutter={[8, 8]}>
                      <Col span={4}>Название</Col>
                      <Col span={20}>
                        <Input
                          value={this.state.paragraphTitle}
                          onChange={(e) => this.setParagraphTitle(e.target.value)}
                          placeholder={'Введите название абзаца'}></Input>
                      </Col>
                      <Col span={4}>Доступ</Col>
                      <Col span={20}>
                        <Select
                          value={this.state.paragraphAccess}
                          onChange={(value) => this.setParagraphAccess(value)}
                          placeholder="Выберите параметры доступа"
                          style={{ width: '100%' }}>
                          <Option key={this.props.user} value={this.props.user}>
                            Для текущего пользователя
                          </Option>
                          <Option key="public" value={'Публичный'}>
                            Для всех пользователей
                          </Option>
                        </Select>
                      </Col>
                    </Row>
                  </Modal>
                  <Button type="primary" icon={<SearchOutlined />} onClick={() => this.onSearch()}>
                    Найти документы
                  </Button>
                </Row>
              </Space>
            </Col>
          </Row>

          {searchLaunched && (
            <>
              <div className={styles.centerTitle}>
                <Title style={{ marginBottom: '15px', marginTop: '15px' }}>Результаты</Title>
              </div>

              <Table
                rowKey="id"
                bordered
                columns={columns}
                dataSource={duplicates}
                loading={this.state.loading}
                pagination={duplicates.length > 10 ? true : false}
              />
            </>
          )}
        </div>

        <Modal
          title={'Загрузить из файла'}
          visible={this.state.uploadModal}
          width={250}
          onCancel={() => {
            this.setState({
              uploadModal: false,
            });
          }}
          cancelText={'Отмена'}
          okText={'Извлечь текст'}
          onOk={this.handleUpload}>
          <Upload
            fileList={fileList}
            onRemove={this.onRemove}
            beforeUpload={this.beforeUpload}
            iconRender={this.iconRender}
            multiple={false}>
            <Button
              icon={<UploadOutlined />}

              //disabled={fileList.length === 0}
              //loading={uploading}
            >
              Выбор файла
            </Button>
          </Upload>
        </Modal>

        <Modal
          title={`Сравнение документов`}
          open={viewText}
          width="90%"
          onOk={() => {
            this.setState({
              viewText: false,
              actionText: '',
              actionDoc: '',
              currentParagraph: 0,
              markupActionArray: [],
              markupSearchArray: [],
            });
          }}
          onCancel={() => {
            this.setState({
              viewText: false,
              actionText: '',
              actionDoc: '',
              currentParagraph: 0,
              markupActionArray: [],
              markupSearchArray: [],
            });
          }}
          cancelText={'Отмена'}
          footer={[
            <Button
              key="back"
              onClick={() =>
                this.setState({
                  viewText: false,
                  actionText: '',
                  actionDoc: '',
                  currentParagraph: 0,
                  markupActionArray: [],
                  markupSearchArray: [],
                })
              }>
              Закрыть
            </Button>,
          ]}>
          <Row justify="start" align="middle" gutter={16}>
            <Col>
              <Text>
                Навигация (номер фрагмента: {this.state.currentParagraph + 1}{' '}
                {this.state.actionDoc !== '' && ' из ' + this.state.actionDoc?.items?.length}):
              </Text>
            </Col>
            <Col>
              <Space>
                <Button
                  type="primary"
                  icon={<DoubleLeftOutlined />}
                  onClick={() => {
                    this.setState(
                      {
                        currentParagraph: 0,
                      },
                      () => {
                        this.scrollTo('search', 0);
                        this.scrollTo('action', 0);
                      },
                    );
                  }}
                />
                <Button
                  type="primary"
                  icon={<LeftOutlined />}
                  onClick={() => {
                    let next = this.state.currentParagraph - 1;
                    if (next < 0) {
                      next = this.state.actionMarkup.length - 1;
                    }
                    this.setState(
                      {
                        currentParagraph: next,
                      },
                      () => {
                        this.scrollTo('search', next);
                        this.scrollTo('action', next);
                      },
                    );
                  }}
                />
                <Button
                  type="primary"
                  icon={<RightOutlined />}
                  onClick={() => {
                    let next = this.state.currentParagraph + 1;
                    if (next === this.state.actionMarkup.length) {
                      next = this.state.actionMarkup.length - 1;
                    }
                    this.setState(
                      {
                        currentParagraph: next,
                      },
                      () => {
                        this.scrollTo('search', next);
                        this.scrollTo('action', next);
                      },
                    );
                  }}
                />
                <Button
                  type="primary"
                  icon={<DoubleRightOutlined />}
                  onClick={() => {
                    this.setState(
                      {
                        currentParagraph: this.state.actionMarkup.length - 1,
                      },
                      () => {
                        this.scrollTo('search', this.state.actionMarkup.length - 1);
                        this.scrollTo('action', this.state.actionMarkup.length - 1);
                      },
                    );
                  }}
                />
              </Space>
            </Col>
          </Row>
          <br />
          <Row wrap={false}>
            <Col span={12}>
              <Divider style={{ margin: 0, padding: 0 }}>Искомый абзац</Divider>
              <div style={{ maxHeight: 'calc(100vh - 340px)', overflowY: 'auto' }}>
                <div
                  dangerouslySetInnerHTML={this.createMarkup(
                    this.state.searchText,
                    this.state.searchMarkup,
                    'search',
                  )}></div>
              </div>
            </Col>
            <Col span={12}>
              <Divider style={{ margin: 0, padding: 0 }}>
                Содержимое документа {this.state.actionDoc.destDocLabel}
              </Divider>
              {this.state.actionTextLoading ? (
                <Card loading={true} style={{ height: '65vh' }} />
              ) : (
                <div style={{ maxHeight: 'calc(100vh - 340px)', overflowY: 'auto' }}>
                  <div
                    dangerouslySetInnerHTML={this.createMarkup(
                      this.state.actionText,
                      this.state.actionMarkup,
                      'action',
                    )}></div>
                </div>
              )}
            </Col>
          </Row>
        </Modal>
      </Card>
    );
  }
}

export default withRouter(SearchTextModule);
