import React, { useState, useEffect } from 'react';
import { Modal, Button, Row, Col, Table, message } from 'antd';
import ApiDiContainer from '../../../service/apiService/apiDiContainer';

//props: visible, setVisible(), docId, userInfo
const AnalyticsModal = (props) => {
  const [userFilters, setUserFilters] = useState([]);
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 10,
    total: 10,
  });
  const [selectedFilters, selectFilters] = useState([]);
  const [analyticData, setAnalyticData] = useState({});

  useEffect(() => {
    getUserFilters();
  }, []);

  const columns = [
    {
      id: 1,
      key: 'requestName',
      dataIndex: 'requestName',
      title: 'Имя правила',
    },
    {
      id: 2,
      key: 'requestDescription',
      dataIndex: 'requestDescription',
      title: 'Описание',
    },
    {
      id: 3,
      key: 'eventTime',
      dataIndex: 'eventTime',
      title: 'Дата и время создания',
      render: (text) => {
        var dateTime = text.split('T');
        var date = dateTime[0].split('-');
        return date[2] + '.' + date[1] + '.' + date[0] + ' ' + dateTime[1];
      },
    },
  ];

  const getUserFilters = () => {
    ApiDiContainer.ProxyApiClickHouse.getFiltersList()
      .then((response) => {
        setUserFilters(response.data);
        setPagination({ ...pagination, total: response.data.length });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logout();
          }

          if (error.response.status === 500) {
            console.error(error.response.data);
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке правил');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке правил');
          console.log('Error', error.message);
        }
      });
  };

  const rowSelection = {
    type: 'radio',
    onChange: (selectedRowKeys, selectedRows) => {
      selectFilters(selectedRows);
    },
  };

  const updateTable = (pagination, filter, sorter) => {
    setPagination(pagination);
  };

  const getAnalyticsData = () => {
    // ApiDiContainer.ProxyApiClickHouse.getDocAnalytics({ docId: props.docId })
    //   .then((response) => {
    //     setAnalyticData(response.data);
    //     setAnalyzing(false);
    //   })
    //   .catch((er) => {
    //     setAnalyzing(false);
    //     message.error('Ошибка при загрузке аналитики по документу');
    //     console.log(er);
    //   });

    let resultObject = {};
    resultObject.docId = props.docId.uid;
    resultObject.filters = [];
    selectedFilters.map((filter) => resultObject.filters.push(filter.requestId));

    ApiDiContainer.ProxyApiAnalytics.runAnalytic(resultObject)
      .then(() => {
        message.success('Подготовка отчета начата');
        CancelAndClose();
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logout();
          }

          if (error.response.status === 500) {
            message.error(error.response.data.message);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных документа');
          //console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных документа');
          //console.log('Error', error.message);
        }
        CancelAndClose();
      });
  };

  const CancelAndClose = () => {
    props.setVisible(false);
  };

  return (
    <Modal
      title={'Анализ'}
      width={1500}
      open={props.visible}
      onCancel={() => {
        CancelAndClose();
      }}
      footer={[
        <Button
          key="back"
          onClick={() => {
            CancelAndClose();
          }}>
          Закрыть
        </Button>,
      ]}>
      <Row>
        <Col span={24}>
          <Table
            rowKey="requestId"
            rowSelection={{ ...rowSelection }}
            columns={columns}
            dataSource={userFilters}
            pagination={pagination.total > pagination.pageSize ? pagination : false}
            onChange={(pagination, filter, sort) => updateTable(pagination, filter, sort)}
          />
        </Col>
      </Row>
      <br />
      <Row justify="start">
        <Button
          type="primary"
          disabled={selectedFilters.length === 0}
          onClick={() => {
            getAnalyticsData();
          }}>
          {'Начать анализ'}
        </Button>
      </Row>
    </Modal>
  );
};

export default AnalyticsModal;
