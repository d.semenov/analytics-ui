import React, { useRef } from 'react';
import { Table, Row, Tag, Space, Button, Modal } from 'antd';
import styles from './DocReportModal.module.css';
import { jsPDF } from 'jspdf';
import html2canvas from 'html2canvas';

import StatisticPie from '../../../components/statistic/statisticPie';
import StatisticColumn from '../../../components/statistic/statisticColumn';

const DocReportModal = ({
  visible,
  setVisible,
  id,
  label,
  date,
  source,
  type,
  sourceFilter,
  typeFilter,
  fromDate,
  toDate,
  duplicates,
  statisticData,
  ripOffSize,
  totalParts,
}) => {
  const pdfRef = useRef(null);

  const documentCols = [
    {
      title: 'Название документа',
      dataIndex: 'label',
    },
    {
      title: 'Источник документа',
      dataIndex: 'source',
    },
    {
      title: 'Тип документа',
      dataIndex: 'type',
    },
    {
      title: 'Дата документа',
      dataIndex: 'date',
      render: (date) => {
        function transformDate(d) {
          var dateTime = d.split('T');
          var date = dateTime[0].split('-');
          return date[2] + '.' + date[1] + '.' + date[0] + ' ' + dateTime[1];
        }

        return <Space>{transformDate(date)}</Space>;
      },
    },
  ];

  const filterCols = [
    {
      title: 'Фильтры источника',
      dataIndex: 'sFilter',
      render: (_, { sFilter }) => (
        <>
          {sourceFilter.map((f) => {
            return <Tag key={f}>{f.children}</Tag>;
          })}
        </>
      ),
    },
    {
      title: 'Фильтры типа',
      dataIndex: 'tFilter',
      render: (_, { tFilter }) => (
        <>
          {typeFilter.map((f) => {
            return <Tag key={f}>{f.children}</Tag>;
          })}
        </>
      ),
    },
    {
      title: 'Фильтр начальной даты',
      dataIndex: 'fromDate',
      width: 250,
    },
    {
      title: 'Фильтр конечной даты',
      dataIndex: 'toDate',
      width: 250,
    },
  ];

  const statisticCols = [
    // {
    //   title: 'Идентификатор',
    //   dataIndex: 'destDocId',
    // },
    {
      title: 'Название',
      dataIndex: 'destDocLabel',
    },
    {
      title: 'Источник',
      dataIndex: 'destDocSource',
    },
    {
      title: 'Тип',
      dataIndex: 'destDocType',
    },
    {
      title: 'Дата документа',
      dataIndex: 'destDocDate',
      render: (date) => {
        function transformDate(d) {
          var dateTime = d.split('T');
          var date = dateTime[0].split('-');
          return date[2] + '.' + date[1] + '.' + date[0] + ' ' + dateTime[1];
        }

        return <Space>{transformDate(date)}</Space>;
      },
    },
    {
      title: 'Заимствования',
      children: [
        {
          title: 'Части',
          dataIndex: 'paragraphCount',
          width: 120,
          render: (id) => {
            return <Space>{id}</Space>;
          },
        },
        {
          title: 'Абзацы',
          dataIndex: 'partCount',
          width: 120,
          render: (id) => {
            let count = id > totalParts.total ? totalParts.total : id;
            return (
              <Row justify="space-around" align="middle">
                {count} / {totalParts.total}
              </Row>
            );
          },
        },
        {
          title: 'Процент совпадения',
          dataIndex: 'items',
          width: 120,
          render: (id) => {
            let av = Math.round(id.reduce((acc, item) => acc + item.percent, 0) / id.length);
            return (
              <Row justify="space-around" align="middle">
                <StatisticPie percent={av} /> {av}%
              </Row>
            );
          },
        },
      ],
    },
  ];

  const saveReport = async () => {
    const element = pdfRef.current;
    const canvas = await html2canvas(element);
    const data = canvas.toDataURL('image/png');

    const pdf = new jsPDF();
    const imgProperties = pdf.getImageProperties(data);
    const pdfWidth = pdf.internal.pageSize.getWidth();
    const pdfHeight = (imgProperties.height * pdfWidth) / imgProperties.width;

    pdf.addImage(data, 'PNG', 0, 0, pdfWidth, pdfHeight);

    let doc_name = id + '.pdf';
    pdf.save(doc_name);
  };

  return (
    <Modal
      title={'Отчет'}
      visible={visible}
      width={1500}
      onCancel={() => {
        setVisible(false);
      }}
      footer={[
        <Button
          key="back"
          onClick={() => {
            setVisible(false);
          }}>
          Закрыть
        </Button>,
      ]}>
      <div className={styles.wrapper}>
        <Row justify="end">
          <Button type="primary" onClick={saveReport}>
            Сохранить отчет
          </Button>
        </Row>

        <div ref={pdfRef} className={styles.body}>
          <Row justify="center">
            <h3>Отчет по документу: {label}</h3>
          </Row>
          <br />

          <Table
            rowKey="key"
            columns={documentCols}
            dataSource={[
              {
                key: 1,
                label: label,
                source: source,
                type: type,
                date: date,
              },
            ]}
            pagination={false}
          />
          <br />
          <Table
            rowKey="key"
            columns={filterCols}
            dataSource={[
              {
                key: 1,
                sFilter: sourceFilter,
                tFilter: typeFilter,
                fromDate: fromDate,
                toDate: toDate,
              },
            ]}
            pagination={false}
          />
          <br />
          <Row justify="space-around">
            <StatisticColumn data={statisticData} />
          </Row>
          <br />
          <h3>
            Общее число заимствованных абзацев: {totalParts.dubles} из {totalParts.total}. Общий
            процент заимствования: {Math.round((totalParts.dubles * 100) / totalParts.total)}%
          </h3>
          <Table
            rowKey="destDocId"
            columns={statisticCols}
            dataSource={duplicates}
            pagination={{ pageSize: duplicates.length, hideOnSinglePage: true }}
          />
        </div>
      </div>
    </Modal>
  );
};

export default DocReportModal;
