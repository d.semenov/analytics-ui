import React from 'react';
import styles from './searchDoc.module.css';
import {
  Button,
  Card,
  Col,
  Divider,
  Select,
  Input,
  message,
  Modal,
  Row,
  Space,
  Spin,
  Table,
  Tooltip,
  DatePicker,
  Typography,
} from 'antd';
import moment from 'moment';
import Title from 'antd/es/typography/Title';
import {
  EyeOutlined,
  SearchOutlined,
  DoubleLeftOutlined,
  LeftOutlined,
  RightOutlined,
  DoubleRightOutlined,
} from '@ant-design/icons';
import Highlighter from 'react-highlight-words';
import ApiDiContainer from '../../../service/apiService/apiDiContainer';
import StatisticPie from '../../../components/statistic/statisticPie';
import DocReportModal from '../docReportModal/DocReportModal';

const { Text } = Typography;
const { Option } = Select;
const { RangePicker } = DatePicker;

class SearchDocModule extends React.Component {
  constructor(props) {
    super(props);
    this.mySourceRef = React.createRef();
    this.myDestRef = React.createRef();
    this.state = {
      docs: [],
      duplicates: [],
      columns: [],
      types: [],
      sources: [],
      loading: false,
      viewText: false,
      sourceText: '',
      destText: '',
      searchDoc: '',
      currentDoc: '',
      currentSource: [],
      currentType: [],
      fromDate: '',
      toDate: '',
      actionDoc: '',
      currentParagraph: 0,
      ripOffPercent: 0,
      ripOffSize: 0,
      statisticData: [],
      totalparts: '',
      nextDisabled: false,
      searchLaunched: false,
      reportModal: false,
    };
  }

  componentDidMount() {
    let columns = [
      {
        dataIndex: 'destDocLabel',
        title: 'Название',
      },
      {
        dataIndex: 'destDocFile',
        title: 'Файл',
      },
      {
        dataIndex: 'destDocSource',
        title: 'Источник',
      },
      {
        dataIndex: 'destDocType',
        title: 'Тип',
      },
      {
        dataIndex: 'destDocDate',
        title: 'Дата документа',
        render: (date) => {
          function transformDate(d) {
            var dateTime = d.split('T');
            var date = dateTime[0].split('-');
            return date[2] + '.' + date[1] + '.' + date[0] + ' ' + dateTime[1];
          }
          return <Space>{transformDate(date)}</Space>;
        },
      },
      {
        title: 'Заимствования',
        children: [
          {
            dataIndex: 'paragraphCount',
            title: 'Части',
            render: (id) => {
              return <Space>{id}</Space>;
            },
          },
          {
            dataIndex: 'partCount',
            title: 'Абзацы',
            render: (id) => {
              let count = id > this.state.totalparts.total ? this.state.totalparts.total : id;
              return (
                <Row justify="space-around" align="middle">
                  {count} / {this.state.totalparts.total}
                </Row>
              );
            },
          },
          {
            dataIndex: 'items',
            title: 'Процент',
            render: (id) => {
              let av = Math.round(id.reduce((acc, item) => acc + item.percent, 0) / id.length);
              return (
                <Row justify="space-around" align="middle">
                  <StatisticPie percent={av} /> {av}%
                </Row>
              );
            },
          },
        ],
      },
      {
        dataIndex: 'actions',
        title: 'Действия',
        render: (_, document) => (
          <Space>
            <Tooltip title="Просмотр документа">
              <Button
                type="primary"
                size="small"
                shape="circle"
                ghost
                onClick={() => {
                  this.setState(
                    {
                      actionDoc: document,
                      viewText: true,
                    },
                    () => {
                      this.markupText();
                    },
                  );
                }}>
                <EyeOutlined />
              </Button>
            </Tooltip>
          </Space>
        ),
      },
    ];

    this.setState(
      {
        columns: columns,
        currentDoc: this.props.currentDoc,
      },
      function () {
        if (this.state.currentDoc !== '') {
          this.getDuplicates(this.state.currentDoc);
        }
      },
    );

    this.getDocs();
    this.getTypesList();
    this.getSourcesList();
  }

  getTypesList = () => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getTypesList()
      .then((response) => {
        this.setState({
          types: response.data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          this.props.logged();
        } else if (error.request) {
          message.error('Ошибка при загрузке списка типов документов');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке списка типов документов');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  getSourcesList = () => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getSourcesList()
      .then((response) => {
        this.setState({
          sources: response.data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке списка источников');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке списка источников');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  getDocs = () => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getDocs()
      .then((response) => {
        this.setState({
          docs: response.data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке списка документов');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке списка документов');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  getDublicatesText = () => {
    this.setState({ loading: true });
    Promise.all([
      ApiDiContainer.ProxyApiClickHouse.getDoc({
        docId: this.state.currentDoc.docId,
      }),
      ApiDiContainer.ProxyApiClickHouse.getDoc({
        docId: this.state.actionDoc.destDocId,
      }),
    ])
      .then((result) => {
        let markupSourceArray = [];
        let currentMark = this.state.currentParagraph;

        this.state.actionDoc.items.map((el) =>
          markupSourceArray.push({ start: el.sourceStart, stop: el.sourceStop }),
        );

        let markupDestArray = [];
        this.state.actionDoc.items.map((el) =>
          markupDestArray.push({ start: el.destStart, stop: el.destStop }),
        );

        if (currentMark >= markupSourceArray.length) {
          this.setState({ currentParagraph: 0 });
          currentMark = 0;
        }

        this.setState(
          {
            sourceText: this.createMarkup(result[0].data, 'source', markupSourceArray, currentMark),
            destText: this.createMarkup(result[1].data, 'dest', markupSourceArray, currentMark),
          },
          () => {
            return result;
          },
        );
      })
      .then(() => {
        let currentMark = document.getElementById('source');
        if (currentMark !== null) {
          currentMark.scrollIntoView({ block: 'start', behavior: 'smooth' });
        }

        currentMark = document.getElementById('dest');
        if (currentMark !== null) {
          currentMark.scrollIntoView({ block: 'start', behavior: 'smooth' });
        }
        this.setState({ loading: false });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных документа');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных документа');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  markupText = () => {
    let markupSourceArray = [];

    let currentMark = this.state.currentParagraph;

    this.state.actionDoc.items.map((el) =>
      markupSourceArray.push({ start: el.sourceStart, stop: el.sourceStop }),
    );

    let markupDestArray = [];
    this.state.actionDoc.items.map((el) =>
      markupDestArray.push({ start: el.destStart, stop: el.destStop }),
    );

    if (currentMark >= markupSourceArray.length) {
      this.setState({ currentParagraph: 0 });
    }
    if (currentMark < 0) {
      this.setState({ currentParagraph: markupSourceArray.length - 1 });
    }

    ApiDiContainer.ProxyApiClickHouse.getDoc({
      docId: this.state.currentDoc.docId,
    })
      .then((response) => {
        this.setState(
          {
            sourceText: this.createMarkup(
              response.data,
              'source',
              markupSourceArray,
              this.state.currentParagraph,
            ),
          },
          () => {
            this.scrollToSource();
          },
        );
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });

    ApiDiContainer.ProxyApiClickHouse.getDoc({
      docId: this.state.actionDoc.destDocId,
    })
      .then((response) => {
        this.setState(
          {
            destText: this.createMarkup(
              response.data,
              'dest',
              markupDestArray,
              this.state.currentParagraph,
            ),
          },
          () => {
            this.scrollToDestination();
          },
        );
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  createMarkup = (text, id, markupArray, item) => {
    // text = [
    //   text.slice(0, markupArray[item].start),
    //   `<mark id="` + id + `">`,
    //   text.slice(markupArray[item].start),
    // ].join('');
    // text = [
    //   text.slice(0, markupArray[item].stop),
    //   `</mark>`,
    //   text.slice(markupArray[item].stop),
    // ].join('');

    let markHead = '<mark-text id="' + id + '">';
    let markTail = '</mark-text>';
    let shift = 0;

    if (text.includes('<') || text.includes('/>')) {
      text = text.replace('<', ' ');
      text = text.replace('/>', ' ');
    }

    text = [
      text.slice(0, markupArray[item].start + shift),
      markHead,
      text.slice(markupArray[item].start + shift),
    ].join('');
    shift += markHead.length;
    text = [
      text.slice(0, markupArray[item].stop + shift),
      markTail,
      text.slice(markupArray[item].stop + shift),
    ].join('');

    return text;
  };

  scrollToSource = () => {
    let currentMark = document.getElementById('source');
    if (currentMark !== null) currentMark.scrollIntoView(); //{ block: 'start', behavior: 'smooth' });
  };

  scrollToDestination = () => {
    let currentMark = document.getElementById('dest');
    if (currentMark !== null) currentMark.scrollIntoView(); //{ block: 'start', behavior: 'smooth' });
  };

  getDuplicates = (currentDoc) => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getTotalParts(currentDoc.docId)
      .then((response) => {
        let totalparts = response.data;
        ApiDiContainer.ProxyApiClickHouse.checkDuplicate({
          docId: this.state.currentDoc.docId,
          percent: 40,
          sourcesFilter: this.state.currentSource.map((el) => el.children),
          typesFilter: this.state.currentType.map((el) => el.children),
          fromDate: this.state.fromDate,
          toDate: this.state.toDate,
        })
          .then((response) => {
            let sum = 0;
            response.data.forEach((element) => {
              sum += element.partCount;
            });

            let statisticData = [];

            statisticData.push({
              type: 'Общий процент плагиата',
              value: Math.round((totalparts.dubles * 100) / totalparts.total),
            });

            let element_index = 1;
            response.data.forEach((element) => {
              console.log(element.partCount * 100 + ' / ' + totalparts.total);
              let percent =
                Math.round((element.partCount * 100) / totalparts.total) > 100
                  ? 100
                  : Math.round((element.partCount * 100) / totalparts.total);

              statisticData.push({
                type: element_index + '. ' + element.destDocLabel,
                value: percent,
              });
              element_index++;
            });
            this.setState({
              totalparts: totalparts,
              duplicates: response.data,
              statisticData: statisticData,
              ripOffPercent: sum,
              loading: false,
            });
            this.setBarData();
          })
          .catch((error) => {
            if (error.response) {
              this.props.logged();

              if (error.response.status === 500) {
                message.error(error.response.data);
              }
            } else if (error.request) {
              message.error('Ошибка при загрузке похожих документов');
              // The request was made but no response was received
              console.log(error.request);
            } else {
              message.error('Ошибка при загрузке похожих документов');
              // Something happened in setting up the request that triggered an Error
              console.log('Error', error.message);
            }
            this.setState({
              loading: false,
            });
          });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных об абзацах');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных об абзацах');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  updateCurrentDoc = (el, selector) => {
    let currentDoc = '';
    if (selector !== undefined)
      currentDoc = this.state.docs.filter((el) => el.docId === selector.value)?.pop();
    this.setState({
      currentDoc: currentDoc,
      searchDoc: '',
      searchLaunched: false,
    });
  };

  updateCurrentSource = (el, selector) => {
    this.setState({
      searchLaunched: false,
      currentSource: selector,
    });
  };

  updateCurrentType = (el, selector) => {
    this.setState({
      searchLaunched: false,
      currentType: selector,
    });
  };

  resetState = () => {
    this.setState({
      duplicates: [],
    });
  };

  // фильтры для таблицы
  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Введите запрос`}
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: 'block',
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}>
            Поиск
          </Button>
          <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Сброс
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },

    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: '#ffc069',
            padding: 0,
          }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  handleSearchSelect = (value) => {
    if (value) {
      this.setState({ searchDoc: value });
    } else {
      this.setState({ searchDoc: '' });
    }
  };

  onChangeDateTimeFilter = (dates, dateStrings) => {
    if (dates) {
      this.setState({
        searchLaunched: false,
        fromDate: dateStrings[0],
        toDate: dateStrings[1],
      });
    } else {
      console.log('Clear');
    }
    this.setState({ searchLaunched: false });
  };

  getRipOffSize = () => {
    let size =
      this.state.ripOffPercent > this.state.totalparts.total
        ? this.state.totalparts.total
        : this.state.ripOffPercent;
    return size;
  };

  setBarData = () => {
    let barData = [];
    this.state.duplicates.forEach((element) => {
      let percent =
        Math.round((element.partCount / this.state.totalparts.total) * 100) > 100
          ? 100
          : Math.round((element.partCount / this.state.totalparts.total) * 100);
      barData.push({
        doc: element.destDocId,
        percent: percent,
      });
    });
    this.setState({
      barData: barData,
    });
  };

  showReportModal = (visible) => {
    this.setState({
      reportModal: visible,
    });
  };

  render() {
    const {
      columns,
      docs,
      currentDoc,
      currentSource,
      currentType,
      fromDate,
      toDate,
      duplicates,
      statisticData,
      totalparts,
    } = this.state;
    return (
      <Card style={{ minHeight: 'calc(100vh - 140px)', minWidth: '1300px' }}>
        <div style={{ display: 'grid' }}>
          <Title style={{ marginBottom: '5' }}>Поиск совпадений в документах</Title>
          <div>
            <Row justify="space-between">
              <Col>
                <Space>
                  Документ
                  <Select
                    mode="single"
                    showSearch
                    allowClear={this.state.currentDoc !== ''}
                    filterOption={false}
                    onSearch={this.handleSearchSelect}
                    notFoundContent={null}
                    style={{ width: '100%', minWidth: 240 }}
                    value={
                      currentDoc !== ''
                        ? currentDoc.docLabel +
                          (currentDoc.docId === ''
                            ? ''
                            : '(' + currentDoc.docId.substring(0, 7) + '...)')
                        : 'Не выбрано'
                    }
                    onChange={this.updateCurrentDoc}>
                    {docs
                      .filter(
                        (el) =>
                          el.docId
                            .toLowerCase()
                            .includes(this.state.searchDoc.toLowerCase() || '') ||
                          el.docLabel
                            .toLowerCase()
                            .includes(this.state.searchDoc.toLowerCase() || ''),
                      )
                      .sort((a, b) => (a.docId > b.docId ? 1 : -1))
                      .map((el) => (
                        <Option key={el.docId} value={el.docId}>
                          {el.docLabel} ({el.docId})
                        </Option>
                      ))}
                  </Select>
                  <Button
                    type="primary"
                    icon={<SearchOutlined />}
                    disabled={currentDoc === ''}
                    onClick={() => {
                      this.resetState();
                      this.getDuplicates(currentDoc);
                      this.setState({ searchLaunched: true });
                    }}>
                    Найти
                  </Button>
                  <Button
                    type="primary"
                    disabled={duplicates.length === 0 || !this.state.searchLaunched}
                    onClick={() => {
                      this.setState({
                        reportModal: true,
                      });
                    }}>
                    Полный отчет
                  </Button>
                </Space>
              </Col>
              <Col>
                <DocReportModal
                  visible={this.state.reportModal}
                  setVisible={this.showReportModal}
                  id={currentDoc.docId}
                  label={currentDoc.docLabel}
                  date={currentDoc.docDate}
                  source={currentDoc.docSource}
                  type={currentDoc.docType}
                  sourceFilter={currentSource}
                  typeFilter={currentType}
                  fromDate={fromDate}
                  toDate={toDate}
                  duplicates={duplicates}
                  totalParts={totalparts}
                  statisticData={statisticData}
                  ripOffSize={this.getRipOffSize()}
                />
              </Col>
            </Row>
            <Divider orientation="left">Фильтры на искомые документы</Divider>
            <Space align="start">
              <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
                Источник
                <Select
                  mode="multiple"
                  allowClear
                  showSearch
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().includes(input.toLowerCase())
                  }
                  style={{ width: '100%', minWidth: 200, maxWidth: 500 }}
                  onChange={this.updateCurrentSource}
                  placeholder="Из всех источников">
                  {this.state.sources.map((t) => (
                    <Option key={t.filterItemId} value={t.filterItemId}>
                      {t.filterValue}
                    </Option>
                  ))}
                </Select>
              </Space>
              <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
                Тип
                <Select
                  mode="multiple"
                  allowClear
                  showSearch
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().includes(input.toLowerCase())
                  }
                  style={{ width: '100%', minWidth: 200, maxWidth: 500 }}
                  onChange={this.updateCurrentType}
                  placeholder="Для всех типов">
                  {this.state.types.map((t) => (
                    <Option key={t.filterItemId} value={t.filterItemId}>
                      {t.filterValue}
                    </Option>
                  ))}
                </Select>
              </Space>
              <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
                Дата добавления
                <RangePicker
                  onChange={this.onChangeDateTimeFilter}
                  ranges={{
                    Сегодня: [moment(), moment()],
                    Месяц: [moment().startOf('month'), moment().endOf('month')],
                    Год: [moment().startOf('year'), moment()],
                  }}
                />
              </Space>
            </Space>
            <Divider />
          </div>
          {this.state.loading && <Spin size="large" />}
          {this.state.searchLaunched && !this.state.loading && duplicates.length !== 0 && (
            <div>
              <Title style={{ marginBottom: '15px', marginTop: '15px' }}>Результаты</Title>
              <h3>
                Общее число заимствованных абзацев: {this.state.totalparts.dubles} из{' '}
                {this.state.totalparts.total}. Общий процент заимствования:{' '}
                {Math.round((this.state.totalparts.dubles * 100) / this.state.totalparts.total)}%
              </h3>
              <Table
                rowKey="destDocId"
                columns={columns}
                dataSource={duplicates}
                pagination={duplicates.length > 10 && null}
                loading={this.state.loading}
              />
            </div>
          )}
          {this.state.searchLaunched && !this.state.loading && duplicates.length === 0 && (
            <h3>Ничего не найдено...</h3>
          )}
        </div>
        <Modal
          title={`Сравнение документов`}
          visible={this.state.viewText}
          width="90%"
          onOk={() => {
            this.setState({
              viewText: false,
              sourceText: '',
              destText: '',
              actionDoc: '',
              currentParagraph: 0,
            });
          }}
          onCancel={() => {
            this.setState({
              viewText: false,
              sourceText: '',
              destText: '',
              actionDoc: '',
              currentParagraph: 0,
            });
          }}
          footer={[
            <Button
              key="back"
              onClick={() =>
                this.setState({
                  viewText: false,
                  sourceText: '',
                  destText: '',
                  actionDoc: '',
                  currentParagraph: 0,
                })
              }>
              Закрыть
            </Button>,
          ]}>
          <Row justify="start" align="middle">
            <Col>
              <Text>
                Навигация (номер фрагмента: {this.state.currentParagraph + 1}{' '}
                {this.state.actionDoc !== '' && ' из ' + this.state.actionDoc?.items?.length}):
              </Text>
            </Col>
            <Col>
              <Space>
                <Button
                  style={{ marginLeft: 10 }}
                  type="primary"
                  icon={<DoubleLeftOutlined />}
                  onClick={() => {
                    this.setState(
                      {
                        currentParagraph: 0,
                      },
                      () => {
                        this.markupText();
                      },
                    );
                  }}
                />
                <Button
                  type="primary"
                  icon={<LeftOutlined />}
                  onClick={() => {
                    this.setState(
                      {
                        currentParagraph: this.state.currentParagraph - 1,
                      },
                      () => {
                        this.markupText();
                      },
                    );
                  }}
                />
                <Button
                  type="primary"
                  icon={<RightOutlined />}
                  onClick={() => {
                    this.setState(
                      {
                        currentParagraph: this.state.currentParagraph + 1,
                      },
                      () => {
                        this.markupText();
                      },
                    );
                  }}
                />
                <Button
                  type="primary"
                  icon={<DoubleRightOutlined />}
                  onClick={() => {
                    this.setState(
                      {
                        currentParagraph: this.state.actionDoc.items.length - 1,
                      },
                      () => {
                        this.markupText();
                      },
                    );
                  }}
                />
              </Space>
            </Col>
          </Row>
          <br />
          <Row wrap={false}>
            <Col span={12}>
              <Divider style={{ margin: 0, padding: 0 }}>
                Содержимое документа {this.state.currentDoc.docLabel}
              </Divider>
              <div
                className={styles.scrollable}
                dangerouslySetInnerHTML={{ __html: this.state.sourceText }}
              />
            </Col>
            <Col span={12}>
              <Divider style={{ margin: 0, padding: 0 }}>
                Содержимое документа {this.state.actionDoc.destDocLabel}
              </Divider>
              <div
                className={styles.scrollable}
                dangerouslySetInnerHTML={{ __html: this.state.destText }}
              />
            </Col>
          </Row>
        </Modal>
      </Card>
    );
  }
}

export default SearchDocModule;
