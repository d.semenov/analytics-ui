import React from 'react';
import styles from './workspace.module.css';
import { Button, Card, Divider, Input, message, Modal, Row, Space, Table, Tooltip } from 'antd';
import Title from 'antd/es/typography/Title';
import Highlighter from 'react-highlight-words';
import {
  PlusOutlined,
  DeleteOutlined,
  SearchOutlined,
  SyncOutlined,
  DownloadOutlined,
  FileTextOutlined,
  ToTopOutlined,
  SelectOutlined,
} from '@ant-design/icons';
import UploadFileFormModal from '../../Forms/uploadFileForm/uploadFileFormModal';
import AnalyticsModal from '../analyticsModal/analyticsModal';
import ApiDiContainer from '../../../service/apiService/apiDiContainer';

const { confirm } = Modal;

class WorkspaceModule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      docs: [],
      columns: [],
      docLabelHighlighting: '',
      docSourceHighlighting: '',
      docTypeHighlighting: '',
      docDateHighlighting: '',
      eventTimeHighlighting: '',
      loading: false,
      analyticsModal: false,
      uploadModal: false,
      viewText: false,
      currentDoc: '',
      docText: '',
      user: '',
      clickedDocument: '',
    };
  }

  componentDidMount() {
    const { docId } = this.props.match.params;

    if (typeof docId != 'undefined') {
      this.setState(
        {
          user: this.props.user,
        },
        () => {
          let docInfo = {
            user: this.props.user,
            docId: docId,
          };

          ApiDiContainer.ProxyApiClickHouse.uploadUserDocFromDb(docInfo)
            .then((response) => {
              this.getDocsPage();
            })
            .catch((error) => {
              if (error.response) {
                if (error.response.status === 403) {
                  message.error('Сессия закрыта');
                  this.setState({
                    loading: false,
                  });
                }

                if (error.response.status === 500) {
                  message.error(error.response.data);
                }
              } else if (error.request) {
                message.error('Ошибка при загрузке данных из БД');
                console.log(error.request);
              } else {
                message.error('Ошибка при загрузке данных из БД');
                console.log('Error', error.message);
              }
              this.setState({
                loading: false,
              });
            });
        },
      );
    }

    this.setState(
      {
        user: this.props.user,
      },
      () => {
        this.getDocsPage();
      },
    );
  }

  getOriginalDoc = (id) => {
    var request = {
      docId: id,
    };
    ApiDiContainer.ProxyApiClickHouse.getOriginalDoc(request)
      .then((response) => {
        const win = window.open(response.data, '_blank');
        if (win != null) {
          win.focus();
        }
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных из БД');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных из БД');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  setUploadModal = (visible) => {
    this.setState({
      uploadModal: visible,
    });
  };

  setAnalyticsModal = (visible) => {
    this.setState({
      analyticsModal: visible,
    });
  };

  // фильтры для таблицы
  tableSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Введите запрос`}
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: 'block',
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}>
            Поиск
          </Button>
          <Button
            onClick={() => this.handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}>
            Сброс
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },

    render: (text) => {
      return (
        <Highlighter
          highlightStyle={{
            backgroundColor: '#ffc069',
            padding: 0,
          }}
          searchWords={this.getHighlightingText(dataIndex)}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      );
    },
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setHighlightingText(dataIndex, selectedKeys[0]);
  };

  handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    confirm();
    this.setHighlightingText(dataIndex, '');
  };

  getHighlightingText = (dataIndex) => {
    switch (dataIndex) {
      case 'docLabel':
        return this.state.docLabelHighlighting !== '' ? [this.state.docLabelHighlighting] : [];
      case 'docSource':
        return this.state.docSourceHighlighting !== '' ? [this.state.docSourceHighlighting] : [];
      case 'docType':
        return this.state.docTypeHighlighting !== '' ? [this.state.docTypeHighlighting] : [];
      case 'docDate':
        return this.state.docDateHighlighting !== '' ? [this.state.docDateHighlighting] : [];
      case 'eventTime':
        return this.state.eventTimeHighlighting !== '' ? [this.state.eventTimeHighlighting] : [];
      default:
        return [];
    }
  };

  setHighlightingText = (dataIndex, text) => {
    switch (dataIndex) {
      case 'docLabel':
        this.setState(
          {
            docLabelHighlighting: text,
          },
          this.getDocsPage(),
        );
        break;
      case 'docSource':
        this.setState(
          {
            docSourceHighlighting: text,
          },
          this.getDocsPage(),
        );
        break;
      case 'docType':
        this.setState(
          {
            docTypeHighlighting: text,
          },
          this.getDocsPage(),
        );
        break;
      case 'docDate':
        this.setState(
          {
            docDateHighlighting: text,
          },
          this.getDocsPage(),
        );
        break;
      case 'eventTime':
        this.setState(
          {
            eventTimeHighlighting: text,
          },
          this.getDocsPage(),
        );
        break;
      default:
        break;
    }
  };

  getDocsPage = () => {
    this.setState({ loading: true });
    let columns = [
      // {
      //   dataIndex: 'docId',
      //   title: 'Идентификатор',
      //   ...this.tableSearchProps('docId'),
      //   sorter: (a, b) => a.docId - b.docId,
      // },
      {
        dataIndex: 'docLabel',
        title: 'Название',
        ...this.tableSearchProps('docLabel'),
        sorter: (a, b) => a.docLabel.length - b.docLabel.length,
      },
      {
        dataIndex: 'docSource',
        title: 'Источник',
        ...this.tableSearchProps('docSource'),
        sorter: (a, b) => a.docSource.length - b.docSource.length,
      },
      {
        dataIndex: 'docType',
        title: 'Тип',
        ...this.tableSearchProps('docType'),
        sorter: (a, b) => a.docType.length - b.docType.length,
      },
      {
        dataIndex: 'docDate',
        title: 'Дата документа',
        ...this.tableSearchProps('docDate'),
        sorter: (a, b) => {
          function transformDate(d) {
            var dateTime = d.split(' ');
            var date = dateTime[0].split('.');
            return date[2] + '-' + date[1] + '-' + date[0] + 'T' + dateTime[1];
          }
          return new Date(transformDate(a.eventTime)) - new Date(transformDate(b.eventTime));
        },
      },
      {
        dataIndex: 'eventTime',
        title: 'Дата обращения',
        ...this.tableSearchProps('eventTime'),
        sorter: (a, b) => {
          function transformDate(d) {
            var dateTime = d.split(' ');
            var date = dateTime[0].split('.');
            return date[2] + '-' + date[1] + '-' + date[0] + 'T' + dateTime[1];
          }
          console.log(new Date(transformDate(a.eventTime)) - new Date(transformDate(b.eventTime)));
          return new Date(transformDate(a.eventTime)) - new Date(transformDate(b.eventTime));
        },
        defaultSortOrder: 'descend',
      },
      {
        dataIndex: 'link',
        title: 'Ссылка',
        width: '6%',
        render: (_, document) =>
          'docId' in document && document.docId !== '' ? (
            <Button
              type="link"
              icon={<DownloadOutlined />}
              onClick={() => {
                this.getOriginalDoc(document.docId); // message.info('Сервис которого еще нет (Скачивание: ' + document.docLabel + ')');
              }}>
              Скачать
            </Button>
          ) : null,
      },
      {
        dataIndex: 'todo',
        title: 'Действия',
        width: 120,
        render: (_, document) => (
          <Space>
            <Tooltip title="Выбрать для анализа">
              <Button
                shape="circle"
                type="text"
                icon={<ToTopOutlined />}
                onClick={() => {
                  this.props.setCurrentDoc(document);
                }}></Button>
            </Tooltip>
            <Tooltip title="Применить правило">
              <Button
                style={{ color: '#2d4db4' }}
                shape="circle"
                type="text"
                icon={<SelectOutlined />}
                onClick={() => {
                  this.setState({
                    analyticsModal: true,
                    clickedDocument: document,
                  });
                }}></Button>
            </Tooltip>
            <Tooltip title="Просмотр документа">
              <Button
                style={{ color: '#31aa2d' }}
                shape="circle"
                type="text"
                icon={<FileTextOutlined />}
                onClick={() => {
                  ApiDiContainer.ProxyApiClickHouse.getDoc({
                    docUid: document.uid,
                  })
                    .then((response) => {
                      this.setState({
                        docText: response.data,
                        viewText: true,
                      });
                    })
                    .catch((error) => {
                      if (error.response) {
                        if (error.response.status === 403) {
                          this.props.logged();
                        }

                        if (error.response.status === 500) {
                          message.error(error.response.data);
                        }
                      } else if (error.request) {
                        message.error('Ошибка при загрузке данных документа');
                        console.log(error.request);
                      } else {
                        message.error('Ошибка при загрузке данных документа');
                        console.log('Error', error.message);
                      }
                      this.setState({
                        loading: false,
                      });
                    });
                }}></Button>
            </Tooltip>
            <Tooltip title="Удалить">
              <Button
                style={{ color: '#a12e12' }}
                shape="circle"
                type="text"
                icon={<DeleteOutlined />}
                onClick={() => {
                  this.showDeleteConfirm(document);
                }}></Button>
            </Tooltip>
          </Space>
        ),
      },
    ];
    ApiDiContainer.ProxyApiClickHouse.getUserDocs(this.state.user)
      .then((response) => {
        let data = response.data;
        function transformDate(d) {
          var dateTime = d.split('T');
          var date = dateTime[0].split('-');
          return date[2] + '.' + date[1] + '.' + date[0] + ' ' + dateTime[1];
        }
        data.forEach((item) => {
          item.eventTime = transformDate(item.eventTime);
          item.docDate = transformDate(item.docDate);
        });
        this.setState({
          columns: columns,
          docs: data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  showDeleteConfirm = (document) => {
    const deleteDoc = this.deleteDoc;
    confirm({
      title: 'Вы действительно хотите удалить документ (' + document.docLabel + ')?',
      okText: 'Да',
      okType: 'danger',
      cancelText: 'Нет',
      onOk() {
        deleteDoc({ uid: document.uid });
      },
      onCancel() {},
    });
  };

  viewText = () => {
    this.setState({
      viewText: true,
    });
  };

  viewRules = () => {
    console.log('tut');
    this.setState({
      analyticsModal: true,
    });
  };

  deleteDoc = (document) => {
    ApiDiContainer.ProxyApiClickHouse.deleteUserDoc(document)
      .then((response) => {
        message.success('Документ успешно удален из БД');
        this.getDocsPage();
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при удалении документа из БД');
          console.log(error.request);
        } else {
          message.error('Ошибка при удалении документа из БД');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  render() {
    const { columns, docs, uploadModal, viewText, analyticsModal, clickedDocument } = this.state;

    return (
      <Card style={{ minHeight: 'calc(100vh - 140px)' }}>
        <div style={{ display: 'grid' }}>
          <div className={styles.centerTitle}>
            <Title style={{ marginBottom: '0' }}>Документы</Title>
            <Space>
              <Button
                type="dashed"
                icon={<SyncOutlined spin={this.state.loading} />}
                onClick={() => this.getDocsPage()}
                className={styles.greenButton}>
                {' '}
                {this.state.loading ? 'Обновление' : 'Обновить'}
              </Button>
              <Button
                type="primary"
                icon={<PlusOutlined />}
                onClick={() => {
                  //this.resetState();
                  this.setState({
                    uploadModal: true,
                  });
                }}>
                Добавить
              </Button>
              <UploadFileFormModal
                visible={uploadModal}
                setVisible={this.setUploadModal}
                user={this.state.user}
              />
            </Space>
          </div>
          <Table
            rowKey="uid"
            columns={columns}
            dataSource={docs}
            loading={this.state.loading}
            pagination={docs.length > 10 ? true : false}
          />
        </div>
        <Modal
          title={'Просмотр'}
          open={viewText}
          width={1500}
          onCancel={() => {
            this.setState({
              viewText: false,
              docText: '',
            });
          }}
          footer={[
            <Button
              key="back"
              onClick={() => {
                this.setState({
                  viewText: false,
                  docText: '',
                });
              }}>
              Закрыть
            </Button>,
          ]}
          cancelText={'Отмена'}>
          <Divider orientation="">Содержимое документа</Divider>
          <Row gutter={16}>{this.state.docText}</Row>
        </Modal>
        <AnalyticsModal
          visible={analyticsModal}
          logout={this.props.logged}
          setVisible={this.setAnalyticsModal}
          docId={clickedDocument}
        />
      </Card>
    );
  }
}

export default WorkspaceModule;
