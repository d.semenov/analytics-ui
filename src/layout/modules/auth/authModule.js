import React from 'react';
import './auth.css';
import { Alert, Button, Card, Form, Input } from 'antd';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import logo from './logotype.png';

import ApiDiContainer from '../../../service/apiService/apiDiContainer';

class AuthModule extends React.Component {
  constructor(props) {
    super(props);
  }

  inputOnChange = (event) => {
    this.setState({
      inputValue: event.target.value,
    });
  };

  componentDidMount = () => {};

  render() {
    return (
      <div className={'background'}>
        <Card className={'card'}>
          <img src={logo} className={'logo'} alt="logo" style={{ marginBottom: '35px' }} />
          <Form
            onFinish={(e) => {
              this.props.loginUser(e.username, e.password, e.store || ' ');
            }}>
            {this.props.errorCode < 0 && (
              <div>
                <Alert message="Неверный логин или пароль." type="error" />
              </div>
            )}
            {this.props.errorCode > 0 && (
              <div>
                <Alert message="Вы вышли из системы." type="success" />
              </div>
            )}
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  message: 'Поле не может быть пустым!',
                },
              ]}>
              <Input placeholder="Логин" prefix={<UserOutlined className={'icons'} />} />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Поле не может быть пустым!',
                },
              ]}>
              <Input.Password
                className={'inputs'}
                placeholder="Пароль"
                prefix={<LockOutlined className={'icons'} />}
              />
            </Form.Item>
            <Button className={'button'} size={'large'} htmlType="submit" type="primary">
              Войти
            </Button>
          </Form>
          <input name="_csrf" type="hidden" value="7fb66604-3027-48f6-bcd3-394563ec2a7f" />
        </Card>
      </div>
    );
  }
}

export default AuthModule;
