import React from 'react';
import {
  Button,
  Card,
  Dropdown,
  Divider,
  List,
  Menu,
  message,
  Modal,
  Table,
  Tooltip,
  Space,
} from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { withRouter } from 'react-router';
import Title from 'antd/es/typography/Title';
import { DeleteOutlined, FileTextOutlined } from '@ant-design/icons';
import ApiDiContainer from '../../../service/apiService/apiDiContainer';

const { confirm } = Modal;

class FiltersListModule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      columns: [],
      userFilters: [],
      filterTypes: [],
      filterEntities: [],
      filterSources: [],
      watchFilterModal: false,
      watchFilterRecord: null,
      createFilterModal: false,
      editFilterModal: false,
    };
  }

  componentDidMount() {
    this.getFilters();
    this.getTypesList();
    this.getEntitiesList();
    this.getSourcesList();
  }

  getTypesList = () => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getTypesList()
      .then((response) => {
        this.setState({
          filterTypes: response.data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке типов документов');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке типов документов');
          console.log('Error', error.message);
        }
        this.setState({ loading: false });
      });
  };

  getEntitiesList = () => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getEntitiesList()
      .then((response) => {
        this.setState({
          filterEntities: response.data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке сущностей');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке сущностей');
          console.log('Error', error.message);
        }
        this.setState({ loading: false });
      });
  };

  getSourcesList = () => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getSourcesList()
      .then((response) => {
        this.setState({
          filterSources: response.data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке источников');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке источников');
          console.log('Error', error.message);
        }
        this.setState({ loading: false });
      });
  };

  showDeleteConfirm = (rule) => {
    const remove = this.removeFilter;
    confirm({
      title: 'Вы действительно хотите удалить правило (' + rule.requestName + ')?',
      okText: 'Да',
      okType: 'danger',
      cancelText: 'Нет',
      onOk() {
        remove(rule);
      },
      onCancel() {},
    });
  };

  getFilters = () => {
    this.setState({
      loading: true,
    });
    let columns = [
      {
        id: 1,
        key: 'requestName',
        dataIndex: 'requestName',
        title: 'Имя правила',
      },
      {
        id: 2,
        key: 'requestDescription',
        dataIndex: 'requestDescription',
        title: 'Описание',
      },
      {
        id: 3,
        key: 'eventTime',
        dataIndex: 'eventTime',
        title: 'Дата и время создания',
        render: (text) => {
          var dateTime = text.split('T');
          var date = dateTime[0].split('-');
          return date[2] + '.' + date[1] + '.' + date[0] + ' ' + dateTime[1];
        },
      },
      {
        id: 4,
        width: 120,
        key: 'actions',
        dataIndex: 'actions',
        title: 'Действия',
        render: (_, rule) => (
          <Space>
            <Tooltip title="Просмотр правила">
              <Button
                style={{ color: '#31aa2d' }}
                shape="circle"
                type="text"
                icon={<FileTextOutlined />}
                onClick={() => {
                  this.setState({
                    watchFilterModal: true,
                    watchFilterRecord: rule,
                  });
                }}></Button>
            </Tooltip>
            <Tooltip title="Удалить">
              <Button
                style={{ color: '#a12e12' }}
                shape="circle"
                type="text"
                icon={<DeleteOutlined />}
                onClick={() => {
                  this.showDeleteConfirm(rule);
                }}></Button>
            </Tooltip>
          </Space>
        ),
      },
    ];
    ApiDiContainer.ProxyApiClickHouse.getFiltersList()
      .then((response) => {
        this.setState({
          columns: columns,
          userFilters: response.data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке правил');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке правил');
          console.log('Error', error.message);
        }
        this.setState({ loading: false });
      });
  };

  getAlias = (alias) => {
    var data = [];
    if (this.state.watchFilterRecord !== null)
      for (const item of this.state.watchFilterRecord.requestItems) {
        if (item.filterItem.filterTypes.filterTypeAlias === alias)
          data.push(item.filterItem.filterValue);
      }
    return data;
  };

  removeFilter = (filter) => {
    ApiDiContainer.ProxyApiClickHouse.deleteFilter(filter)
      .then((response) => {
        message.success('Правило успешно удалено');
        this.getFilters();
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при удалении правила');
          console.log(error.request);
        } else {
          message.error('Ошибка при удалении правила');
          console.log('Error', error.message);
        }
      });
  };

  pagination = {
    current: 1,
    pageSize: 10,
    total: 10,
  };

  updateTable = (pagination, filter, sorter) => {
    this.pagination = pagination;
  };

  setCreateFilterModal = (visible) => {
    this.setState({
      createFilterModal: visible,
    });
  };

  render() {
    const { columns, userFilters } = this.state;

    return (
      <Card style={{ minHeight: 'calc(100vh - 140px)' }}>
        <div style={{ display: 'grid' }}>
          <Title>Список пользовательских правил</Title>
          <Table
            rowKey="requestId"
            columns={columns}
            dataSource={userFilters}
            loading={this.state.loading}
            pagination={this.pagination.total > this.pagination.pageSize ? this.pagination : false}
            onChange={(pagination, filter, sort) => this.updateTable(pagination, filter, sort)}
          />
        </div>
        <Modal
          title={'Атрибуты правила'}
          centered
          open={this.state.watchFilterModal}
          footer={[]}
          onOk={() => {
            this.setState({
              watchFilterModal: false,
              watchFilterRecord: null,
            });
          }}
          onCancel={() => {
            this.setState({
              watchFilterModal: false,
              watchFilterRecord: null,
            });
          }}>
          <Divider orientation="left">Типы документов</Divider>
          <List
            size="small"
            bordered
            dataSource={this.getAlias('docType')}
            renderItem={(item) => <List.Item>{item}</List.Item>}
          />
          <Divider orientation="left">Сущности</Divider>
          <List
            size="small"
            bordered
            dataSource={this.getAlias('entityType')}
            renderItem={(item) => <List.Item>{item}</List.Item>}
          />
          <Divider orientation="left">Источники</Divider>
          <List
            size="small"
            bordered
            dataSource={this.getAlias('docSource')}
            renderItem={(item) => <List.Item>{item}</List.Item>}
          />
          <Divider orientation="left">Текст</Divider>
          <List
            size="small"
            bordered
            dataSource={this.getAlias('paragraph')}
            renderItem={(item) => <List.Item>{item}</List.Item>}
          />
        </Modal>
      </Card>
    );
  }
}

export default withRouter(FiltersListModule);
