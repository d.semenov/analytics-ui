import React, { useEffect, useState } from 'react';
import {
  Table,
  Row,
  Col,
  Card,
  Button,
  Badge,
  Collapse,
  Descriptions,
  Space,
  List,
  Divider,
  Typography,
} from 'antd';
import StatisticPie from '../../../components/statistic/statisticPie';
import JsPdfWrapper from '../tools/jsPdfWrapper/jsPdfWrapper';
import styles from './RuleCheckReport.module.css';

const RuleCheckReport = ({ report }) => {
  const { Panel } = Collapse;
  const { Title, Text } = Typography;

  const [activeKey, setActiveKey] = useState(1);
  // useEffect(() => {
  //   if (activeKey === 1) {
  //     saveReport();
  //   }
  // });

  function saveReport() {
    const pdfWrapper = new JsPdfWrapper();
    const targetElement = document.getElementById('pdfContainer');
    const pdfFileName = 'Отчет';
    pdfWrapper.DownloadFromHTML(targetElement, pdfFileName);
  }

  const docInfoColumns = [
    {
      dataIndex: 'docLabel',
      title: 'Название',
    },
    {
      dataIndex: 'docFile',
      title: 'Файл документа',
    },
    {
      dataIndex: 'docSource',
      title: 'Источник',
    },
    {
      dataIndex: 'docType',
      title: 'Тип',
    },
    {
      dataIndex: 'docDate',
      title: 'Дата документа',
      render: (date) => {
        function transformDate(d) {
          var dateTime = d.split('T');
          var date = dateTime[0].split('-');
          return date[2] + '.' + date[1] + '.' + date[0] + ' ' + dateTime[1];
        }
        return <Space>{transformDate(date)}</Space>;
      },
    },
    {
      dataIndex: 'startDate',
      title: 'Дата анализа',
      render: (date) => {
        function transformDate(d) {
          var dateTime = d.split('T');
          var date = dateTime[0].split('-');
          return date[2] + '.' + date[1] + '.' + date[0] + ' ' + dateTime[1];
        }
        return <Space>{transformDate(date)}</Space>;
      },
    },
  ];
  const docInfoData = [
    {
      docLabel: report.docInfo.docLabel,
      docFile: report.docInfo.docFile,
      docSource: report.docInfo.docSource,
      docType: report.docInfo.docType,
      docDate: report.docInfo.docDate,
      startDate: report.startDate,
    },
  ];

  const filterInfoColumns = [
    {
      dataIndex: 'sourcesFilter',
      title: 'Фильтры по источникам',
      render: (_, { sourcesFilter }) => (
        <>
          {sourcesFilter.map((filter) => {
            return (
              <>
                <Text>{filter}</Text>
                <br />
              </>
            );
          })}
        </>
      ),
    },
    {
      dataIndex: 'typesFilter',
      title: 'Фильтры по типам',
      render: (_, { typesFilter }) => (
        <>
          {typesFilter.map((filter) => {
            return (
              <>
                <Text>{filter}</Text>
                <br />
              </>
            );
          })}
        </>
      ),
    },
    {
      dataIndex: 'entitiesFilter',
      title: 'Фильтры по сущностям',
      render: (_, { entitiesFilter }) => (
        <>
          {entitiesFilter.map((filter) => {
            return (
              <>
                <Text>{filter}</Text>
                <br />
              </>
            );
          })}
        </>
      ),
    },
  ];

  const filterInfoData = [
    {
      sourcesFilter: report.report.sourcesFilter,
      typesFilter: report.report.typesFilter,
      entitiesFilter: report.report.entitiesFilter,
    },
  ];

  const duplicatesColumns = [
    // {
    //   dataIndex: 'destDocId',
    //   title: 'Идентификатор',
    // },
    {
      dataIndex: 'destDocLabel',
      title: 'Название',
    },
    {
      dataIndex: 'destDocSource',
      title: 'Источник',
    },
    {
      dataIndex: 'destDocType',
      title: 'Тип',
    },
    {
      dataIndex: 'destDocDate',
      title: 'Дата документа',
      render: (date) => {
        function transformDate(d) {
          var dateTime = d.split('T');
          var date = dateTime[0].split('-');
          return date[2] + '.' + date[1] + '.' + date[0] + ' ' + dateTime[1];
        }

        return <Space>{transformDate(date)}</Space>;
      },
    },
    {
      title: 'Заимствования',
      children: [
        {
          dataIndex: 'paragraphCount',
          title: 'Части',
        },
        {
          dataIndex: 'partCount',
          title: 'Абзацы',
        },
        {
          dataIndex: 'percent',
          title: 'Процент совпадения',
          render: (percent) => (
            <Row justify="space-around" align="middle">
              <StatisticPie percent={percent} /> {percent}%
            </Row>
          ),
        },
      ],
    },
  ];

  const getDuplicatesData = () => {
    let result = [];
    let duplicates = report.report.dublicatesResult;
    for (const item of duplicates) {
      result.push({
        destDocId: item.destDocId,
        destDocLabel: item.destDocLabel,
        destDocSource: item.destDocSource,
        destDocType: item.destDocType,
        destDocDate: item.destDocDate,
        partCount: item.partCount,
        paragraphCount: item.paragraphCount,
        percent: Math.round(item.items.reduce((acc, item) =>  acc + item.percent, 0)/item.items.length)
      });
    }
    return result;
  };

  const getDocId = () => {
    return report.docInfo.docId;
  };

  const getDocLabel = () => {
    return report.docInfo.docLabel;
  };

  return (
    <div className={styles.wrapper}>
      <Row justify="end">
        <Button type="primary" onClick={() => saveReport()}>
          Сохранить отчет
        </Button>
      </Row>
      <div id="pdfContainer" className={styles.body}>
        <Row gutter={[0, 32]}>
          <Col span={24}>
            <Title level={4}>Данные документа</Title>
            <Table columns={docInfoColumns} dataSource={docInfoData} pagination={false}></Table>
          </Col>
          <Col span={24}>
            <Title level={4}>Примененные фильтры</Title>
            <Table
              columns={filterInfoColumns}
              dataSource={filterInfoData}
              pagination={false}></Table>
          </Col>
          <Col span={24}>
            <Title level={4}>Данные по дубликатам текста</Title>
            <Table
              columns={duplicatesColumns}
              dataSource={getDuplicatesData()}
              pagination={false}></Table>
          </Col>
          <Col span={24}>
            {getDocId() !== '' ? (
              <Title level={4}>Поиск абзацев в документе {getDocLabel()}</Title>
            ) : (
              <Title level={4}>Поиск абзацев в документах</Title>
            )}
            {report.report.searchTextResult.map((searchResult) => {
              return (
                <Row gutter={[0, 16]}>
                  <Col span={24}>
                    {searchResult.dublicates.length === 0 ? (
                      <Space>
                        <Badge status="error" />
                        <span>
                          {'Абзац: ' + searchResult.paragraphName + '. Найденных совпадений нет'}
                        </span>
                      </Space>
                    ) : (
                      <Space>
                        <Badge status="success" />
                        <span>Абзац: "{searchResult.paragraphName}", найдены совпадения</span>
                      </Space>
                    )}
                  </Col>
                  <Col span={24}>
                    <Collapse activeKey={activeKey}>
                      <Panel header="Показать текст" key="1">
                        <p>{searchResult.text}</p>
                      </Panel>
                    </Collapse>
                  </Col>
                  {searchResult.dublicates.length !== 0 && (
                    <>
                      <p style={{ marginBottom: 0 }}>Список совпадений:</p>
                      <Col span={24}>
                        <Space direction="vertical" style={{ display: 'flex' }}>
                          {searchResult.dublicates.map((duplicate) => {
                            return (
                              <Descriptions size="small" bordered={true}>
                                <Descriptions.Item label="Идентификатор">
                                  {duplicate.destDocId}
                                </Descriptions.Item>
                                <Descriptions.Item label="Имя документа">
                                  {duplicate.destDocLabel}
                                </Descriptions.Item>
                                <Descriptions.Item label="Файл документа">
                                  {duplicate.destDocFile}
                                </Descriptions.Item>
                                <Descriptions.Item label="Тип документа">
                                  {duplicate.destDocType}
                                </Descriptions.Item>
                                <Descriptions.Item label="Источник документа">
                                  {duplicate.destDocSource}
                                </Descriptions.Item>
                                <Descriptions.Item label="Количество абзацев">
                                  {duplicate.paragraphCount}
                                </Descriptions.Item>
                                <Descriptions.Item label="Количество частей">
                                  {duplicate.partCount}
                                </Descriptions.Item>
                                <Descriptions.Item label="Процент совпадения">
                                  {duplicate?.items[0].percent}%
                                </Descriptions.Item>
                              </Descriptions>
                            );
                          })}
                        </Space>
                      </Col>
                    </>
                  )}
                  <Divider></Divider>
                </Row>
              );
            })}
          </Col>
          <Col span={24}>
            <Title level={4}>Данные по найденным сущностям</Title>
            <List
              grid={{
                gutter: 16,
                xs: 1,
                sm: 2,
                md: 4,
                lg: 4,
                xl: 6,
                xxl: 3,
              }}
              dataSource={report.report.entitiesResult}
              renderItem={(item) => (
                <List.Item>
                  <Card size="small" title={item.type}>
                    Количество совпадений: {item.info.length}
                  </Card>
                </List.Item>
              )}
            />
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default RuleCheckReport;
