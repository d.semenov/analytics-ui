import React, { useState, useEffect } from 'react';
import { Button, Card, message, Row, Col, Select, Space, Divider, Typography, Upload } from 'antd';
import Title from 'antd/es/typography/Title';
import {
  FilePdfOutlined,
  FileWordOutlined,
  FunctionOutlined,
  SearchOutlined,
  UploadOutlined,
} from '@ant-design/icons';
import ApiDiContainer from '../../../service/apiService/apiDiContainer';
import InsertTextFormModal from '../../Forms/insertTextForm/insertTextFormModal';

const { Option } = Select;
const { Text } = Typography;

const QualifierModule = (props) => {
  const [result, setResult] = useState([]);
  const [loading, setLoading] = useState(false);
  const [mainText, setMainText] = useState('');
  const [mainTextLoading, setMainTextLoading] = useState(false);
  const [insertTextModal, setInsertTextModal] = useState(false);
  const [types, setTypes] = useState([]);
  const [selectedType, setSelectedType] = useState({});
  const [fileList, setFileList] = useState([]);
  const [docs, setDocs] = useState([]);
  const [searchDoc, setSearchDoc] = useState('');
  const [currentDoc, setCurrentDoc] = useState('');

  useEffect(() => {
    setCurrentDoc(props.currentDoc);
    getTypesList();
    getDocs();
  }, []);

  const getTypesList = () => {
    setLoading(true);
    ApiDiContainer.ProxyApiClickHouse.getTypesList()
      .then((response) => {
        setTypes(response.data);
        setLoading(false);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
            /*message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при получении списка типов документов');
          console.log(error.request);
        } else {
          message.error('Ошибка при получении списка типов документов');
          console.log('Error', error.message);
        }
        setLoading(false);
      });
  };

  const getDocs = () => {
    setLoading(true);
    ApiDiContainer.ProxyApiClickHouse.getDocs()
      .then((response) => {
        setDocs(response.data);
        setLoading(false);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при получении списка документов');
          console.log(error.request);
        } else {
          message.error('Ошибка при получении списка документов');
          console.log('Error', error.message);
        }
        setLoading(false);
      });
  };

  const getRubric = () => {
    setLoading(true);
    ApiDiContainer.ProxyApiClickHouse.getRubric({
      text: mainText,
    })
      .then((response) => {
        setResult(response.data);
        if (result.length === 0) {
          message.info('Не удалось классифицировать документ ' + currentDoc.docLabel);
        }
        setLoading(false);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
          }
          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке рубрик');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке рубрик');
          console.log('Error', error.message);
        }
        setLoading(false);
      });
  };

  const getDocText = (doc) => {
    setMainTextLoading(true);
    if (doc.uid !== null) {
      ApiDiContainer.ProxyApiClickHouse.getUserDoc(doc.uid)
        .then((response) => {
          setMainText(response.data.text.replace(/(\r\n|\n|\r)/gm, ' '));
          setMainTextLoading(false);
        })
        .catch((error) => {
          if (error.response) {
            if (error.response.status === 403) {
              props.logged();
            }

            if (error.response.status === 500) {
              message.error(error.response.data);
            }
          } else if (error.request) {
            message.error('Ошибка при загрузке данных документа');
            console.log(error.request);
          } else {
            message.error('Ошибка при загрузке данных документа');
            console.log('Error', error.message);
          }
          setMainTextLoading(false);
        });
    } else {
      ApiDiContainer.ProxyApiClickHouse.getDoc({ docId: doc.docId })
        .then((response) => {
          setMainText(response.data.replace(/(\r\n|\n|\r)/gm, ' '));
          setMainTextLoading(false);
        })
        .catch((error) => {
          if (error.response) {
            if (error.response.status === 403) {
              props.logged();
            }

            if (error.response.status === 500) {
              message.error(error.response.data);
            }
          } else if (error.request) {
            message.error('Ошибка при загрузке данных документа');
            console.log(error.request);
          } else {
            message.error('Ошибка при загрузке данных документа');
            console.log('Error', error.message);
          }
          setMainTextLoading(false);
        });
    }
  };

  const setDocText = (text) => {
    setMainText(text.replace(/(\r\n|\n|\r)/gm, ' '));
    setCurrentDoc('');
    setInsertTextModal(false);
  };

  const updateCurrentType = (el, selector) => {
    setSelectedType(selector);
  };

  const handleSearchSelect = (value) => {
    if (value) {
      setSearchDoc(value);
    } else {
      setSearchDoc('');
    }
  };

  const updateCurrentDoc = (el, selector) => {
    let currentDoc = '';
    if (selector !== undefined)
      currentDoc = docs.filter((el) => el.docId === selector.value)?.pop();
    setCurrentDoc(currentDoc);
    setSearchDoc('');
  };

  const uploadProps = {
    multiple: true,
    listType: 'picture',

    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      setFileList(newFileList);
    },
    iconRender: (file) => {
      if (file.name.indexOf('pdf') > 0) {
        return <FilePdfOutlined />;
      }
      if (file.name.indexOf('docx') > 0) {
        return <FileWordOutlined />;
      }
    },
    beforeUpload: (file) => {
      if (file.name.indexOf('pdf') > 0 || file.name.indexOf('docx') > 0) {
        return false;
      }
      return Upload.LIST_IGNORE;
    },
    onChange: ({ fileList }) => setFileList(fileList.filter((file) => file.status !== 'error')),
    fileList,
  };

  return (
    <Card style={{ minHeight: 'calc(100vh - 140px)' }}>
      {props.isAdmin ? (
        <>
          <Title style={{ marginBottom: 5 }}>Обучение</Title>
          <div style={{ minHeight: 300 }}>
            <span style={{ marginRight: 8, marginTop: 8 }}>Выберите тип документа:</span>
            <Select
              showSearch
              disabled={loading}
              style={{ minWidth: 250, marginRight: 28, marginTop: 8 }}
              value={selectedType?.value}
              onChange={updateCurrentType}
              filterOption={(input, option) =>
                option.value.toLowerCase().includes(input.toLowerCase())
              }
              filterSort={(optionA, optionB) =>
                optionA.value.toLowerCase().localeCompare(optionB.value.toLowerCase())
              }>
              {types.map((el) => (
                <Option key={el.filterItemId} value={el.filterValue}>
                  {el.filterValue}
                </Option>
              ))}
            </Select>
            <span style={{ marginRight: 8, marginTop: 8 }}>Выберите файлы документов:</span>
            <Upload className=".ant-upload-list-scroll" {...uploadProps}>
              <Button icon={<UploadOutlined />}>Выбор файла</Button>
            </Upload>
          </div>
          <Button
            disabled={fileList.length === 0 || selectedType === {}}
            style={{ marginRight: 16, marginTop: 8 }}
            type="primary"
            icon={<FunctionOutlined />}
            onClick={() => {
              const formData = new FormData();
              let files = [];
              fileList.forEach((file) => {
                formData.append(`files`, file.originFileObj, file.name);
              });

              formData.append(`type`, selectedType.value);

              ApiDiContainer.ProxyApiClickHouse.trainClassifier(formData)
                .then((response) => {
                  message.success('Модель обучена');
                  setLoading(false);
                })
                .catch((error) => {
                  if (error.response) {
                    if (error.response.status === 403) {
                      props.logged();
                    }

                    if (error.response.status === 500) {
                      message.error(error.response.data);
                    }
                  } else if (error.request) {
                    message.error('Ошибка запроса');
                    console.log(error.request);
                  } else {
                    message.error('Неизвестная ошибка');
                    console.log('Error', error.message);
                  }
                  setLoading(false);
                });
            }}>
            Обучить
          </Button>
        </>
      ) : (
        <></>
      )}
      <Divider></Divider>
      <div style={{ display: 'grid' }}>
        <Title style={{ marginBottom: '5' }}>Классификация документа</Title>
        <Row wrap={false} justify="space-between">
          <Col>
            <Space>
              <span>Выберите документ:</span>
              <Select
                mode="single"
                showSearch
                allowClear={currentDoc !== ''}
                disabled={loading}
                style={{ width: 350, minWidth: 230 }}
                filterOption={false}
                onSearch={handleSearchSelect}
                notFoundContent={null}
                value={
                  currentDoc !== ''
                    ? currentDoc.docLabel +
                      (currentDoc.docId === '' ? '' : '(' + currentDoc.docId + ')')
                    : 'Не выбрано'
                }
                onChange={updateCurrentDoc}>
                {docs
                  .filter(
                    (el) =>
                      el.docId.toLowerCase().includes(searchDoc.toLowerCase() || '') ||
                      el.docLabel.toLowerCase().includes(searchDoc.toLowerCase() || ''),
                  )
                  .sort((a, b) => (a.docId > b.docId ? 1 : -1))
                  .map((el) => (
                    <Option key={el.docId} value={el.docId}>
                      {el.docLabel} ({el.docId})
                    </Option>
                  ))}
              </Select>
              <Button
                type="primary"
                disabled={currentDoc === '' || loading}
                loading={mainTextLoading}
                onClick={() => {
                  getDocText(currentDoc);
                }}>
                Получить текст
              </Button>
              <Button
                disabled={loading}
                type="primary"
                onClick={() => {
                  setInsertTextModal(true);
                }}>
                Вставить текст из буфера
              </Button>
              <InsertTextFormModal
                title={'Вставьте текст из буфера обмена [CTRL-V]'}
                visible={insertTextModal}
                setVisible={setInsertTextModal}
                setText={setDocText}
              />
            </Space>
          </Col>
          <Col>
            <Space>
              <Button
                disabled={mainText === '' || loading}
                type="primary"
                icon={<SearchOutlined />}
                onClick={() => {
                  getRubric();
                }}>
                Классифицировать
              </Button>
            </Space>
          </Col>
        </Row>

        <Row style={{ marginTop: 16 }} gutter={[0, 16]}>
          <Col span={24}>
            {result.length > 0 ? (
              <span style={{ marginRight: 10 }}>Результаты проверки:</span>
            ) : (
              <></>
            )}
            {result.length > 0 ? (
              result
                .sort((a, b) => {
                  return b.value - a.value;
                })
                .map((el) => (
                  <Row style={{ fontWeight: 700, fontSize: 16, color: 'green' }}>
                    {el.name}: {el.value}%
                  </Row>
                ))
            ) : (
              <></>
            )}
          </Col>
          <Col span={24}>
            <div style={{ minHeight: 300, maxHeight: 'calc(100vh - 600px)', overflowY: 'auto' }}>
              {mainText === '' ? (
                <Text type="secondary">
                  Получите текст из документа или вставьте его в поле из буфера обмена
                </Text>
              ) : (
                <p>{mainText}</p>
              )}
            </div>
          </Col>
        </Row>
      </div>
    </Card>
  );
};

export default QualifierModule;
