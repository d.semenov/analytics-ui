import React from 'react';
import styles from './results.module.css';
import {
  Button,
  Card,
  Input,
  message,
  Modal,
  Space,
  Typography,
  Table,
  Tooltip,
  Divider,
  Row,
} from 'antd';
import Title from 'antd/es/typography/Title';
import Highlighter from 'react-highlight-words';
import {
  DeleteOutlined,
  FileTextOutlined,
  SearchOutlined,
  SyncOutlined,
  PieChartOutlined,
  DownloadOutlined,
} from '@ant-design/icons';
import ApiDiContainer from '../../../service/apiService/apiDiContainer';
import RuleCheckReport from '../ruleCheckReport/RuleCheckReport';

const { Text } = Typography;
const { confirm } = Modal;

class ResultsModule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      docs: [],
      columns: [],
      docLabelHighlighting: '',
      docSourceHighlighting: '',
      docTypeHighlighting: '',
      requestNameHighlighting: '',
      loading: false,
      viewText: false,
      viewReport: false,
      docText: '',
      interval: null,
      reportObject: null,
    };
  }

  componentDidMount() {
    this.setState(
      {
        user: this.props.user,
      },
      () => {
        this.getDocsPage();
        this.startUpdater();
      },
    );
  }

  componentWillUnmount() {
    this.stopUpdater();
  }

  startUpdater = () => {
    let interval = setInterval(() => this.getDocsPage(), 60000);
    this.setState({ interval: interval });
  };

  stopUpdater = () => {
    clearInterval(this.state.interval);
    this.setState({ interval: null });
  };

  // фильтры для таблицы
  tableSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Введите запрос`}
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: 'block',
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}>
            Поиск
          </Button>
          <Button
            onClick={() => this.handleReset(clearFilters, confirm, dataIndex)}
            size="small"
            style={{ width: 90 }}>
            Сброс
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },

    render: (text) => {
      return (
        <Highlighter
          highlightStyle={{
            backgroundColor: '#ffc069',
            padding: 0,
          }}
          searchWords={this.getHighlightingText(dataIndex)}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      );
    },
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setHighlightingText(dataIndex, selectedKeys[0]);
  };

  handleReset = (clearFilters, confirm, dataIndex) => {
    clearFilters();
    confirm();
    this.setHighlightingText(dataIndex, '');
  };

  getHighlightingText = (dataIndex) => {
    switch (dataIndex) {
      case 'docLabel':
        return this.state.docLabelHighlighting !== '' ? [this.state.docLabelHighlighting] : [];
      case 'docSource':
        return this.state.docSourceHighlighting !== '' ? [this.state.docSourceHighlighting] : [];
      case 'docType':
        return this.state.docTypeHighlighting !== '' ? [this.state.docTypeHighlighting] : [];
      case 'requestName':
        return this.state.requestNameHighlighting !== ''
          ? [this.state.requestNameHighlighting]
          : [];
      default:
        return [];
    }
  };

  setHighlightingText = (dataIndex, text) => {
    switch (dataIndex) {
      case 'docLabel':
        this.setState(
          {
            docLabelHighlighting: text,
          },
          this.getDocsPage(),
        );
        break;
      case 'docSource':
        this.setState(
          {
            docSourceHighlighting: text,
          },
          this.getDocsPage(),
        );
        break;
      case 'docType':
        this.setState(
          {
            docTypeHighlighting: text,
          },
          this.getDocsPage(),
        );
        break;
      case 'requestName':
        this.setState(
          {
            requestNameHighlighting: text,
          },
          this.getDocsPage(),
        );
        break;
      default:
        break;
    }
  };

  transformDate = (d) => {
    if (d != null) {
      var dateTime = d.split('T');
      var date = dateTime[0].split('-');
      return date[2] + '.' + date[1] + '.' + date[0] + ' ' + dateTime[1];
    } else {
      return '';
    }
  };

  colorStatus = (status) => {
    if (status === 'Завершена') {
      return <Text type="success">{status}</Text>;
    }
    if (status === 'В работе') {
      return <Text type="warning">{status}</Text>;
    }
    if (status === 'Ошибка') {
      return <Text type="danger">{status}</Text>;
    }
    return <Text>{status}</Text>;
  };

  getDocsPage = () => {
    this.setState({ loading: true });
    let columns = [
      // {
      //   dataIndex: 'docId',
      //   title: 'Идентификатор',
      //   ...this.tableSearchProps('docId'),
      //   sorter: (a, b) => a.docId - b.docId,
      // },
      {
        dataIndex: 'docLabel',
        title: 'Название',
        ...this.tableSearchProps('docLabel'),
        sorter: (a, b) => a.docLabel.length - b.docLabel.length,
      },
      {
        dataIndex: 'docSource',
        title: 'Источник',
        ...this.tableSearchProps('docSource'),
        sorter: (a, b) => a.docSource.length - b.docSource.length,
      },
      {
        dataIndex: 'docType',
        title: 'Тип',
        ...this.tableSearchProps('docType'),
        sorter: (a, b) => a.docType.length - b.docType.length,
      },
      {
        dataIndex: 'requestName',
        title: 'Название правила',
        ...this.tableSearchProps('requestName'),
        sorter: (a, b) => a.requestName.length - b.requestName.length,
      },
      {
        dataIndex: 'docDate',
        title: 'Дата документа',
        //...this.tableSearchProps('docDate'),
        sorter: (a, b) => {
          return new Date(a.eventTime) - new Date(b.eventTime);
        },
      },
      {
        dataIndex: 'startDate',
        title: 'Дата анализа',
        //...this.tableSearchProps('eventTime'),
        sorter: (a, b) => {
          return new Date(a.eventTime) - new Date(b.eventTime);
        },
        defaultSortOrder: 'descend',
      },
      {
        dataIndex: 'link',
        title: 'Ссылка',
        width: '6%',
        render: (_, document) =>
          'docId' in document && document.docId !== '' ? (
            <Button
              type="link"
              icon={<DownloadOutlined />}
              onClick={() => {
                this.getOriginalDoc(document.docId); // message.info('Сервис которого еще нет (Скачивание: ' + document.docLabel + ')');
              }}>
              Скачать
            </Button>
          ) : null,
      },
      {
        dataIndex: 'status',
        title: 'Статус работы',
        render: (status) => this.colorStatus(status),
      },
      {
        dataIndex: 'todo',
        title: 'Действия',
        render: (_, document) => (
          <Space>
            <Tooltip title="Просмотр документа">
              <Button
                className={styles.greenButton}
                shape="circle"
                type="text"
                icon={<FileTextOutlined />}
                onClick={() => {
                  ApiDiContainer.ProxyApiClickHouse.getDoc({
                    docUid: document.docInfo.uid,
                  })
                    .then((response) => {
                      this.setState({
                        docText: response.data,
                        viewText: true,
                      });
                    })
                    .catch((error) => {
                      if (error.response) {
                        if (error.response.status === 403) {
                          this.props.logged();
                        }

                        if (error.response.status === 500) {
                          message.error(error.response.data);
                        }
                      } else if (error.request) {
                        message.error('Ошибка при загрузке данных документа');
                        console.log(error.request);
                      } else {
                        message.error('Ошибка при загрузке данных документа');
                        console.log('Error', error.message);
                      }
                    });
                }}></Button>
            </Tooltip>

            <Tooltip title="Просмотр результатов">
              <Button
                shape="circle"
                type="text"
                className={styles.blueButton}
                icon={<PieChartOutlined />}
                disabled={document.status === 'В работе'}
                onClick={() => {
                  this.setState({ loading: true });
                  ApiDiContainer.ProxyApiClickHouse.getResult(document.resultId)
                    .then((response) => {
                      this.setState({
                        loading: false,
                        viewReport: true,
                        reportObject: response.data,
                      });
                    })
                    .catch((error) => {
                      if (error.response) {
                        if (error.response.status === 403) {
                          this.props.logged();
                        }

                        if (error.response.status === 500) {
                          message.error(error.response.data);
                        }
                      } else if (error.request) {
                        message.error('Ошибка при загрузке данных документа');
                        console.log(error.request);
                      } else {
                        message.error('Ошибка при загрузке данных документа');
                        console.log('Error', error.message);
                      }
                      this.setState({
                        loading: false,
                      });
                    });
                }}></Button>
            </Tooltip>

            <Tooltip title="Удалить">
              <Button
                style={{ color: '#a12e12' }}
                shape="circle"
                type="text"
                icon={<DeleteOutlined />}
                onClick={() => {
                  this.showDeleteConfirm(document);
                }}></Button>
            </Tooltip>
          </Space>
        ),
      },
    ];
    ApiDiContainer.ProxyApiClickHouse.getResults()
      .then((response) => {
        let data = response.data;
        data.forEach((item) => {
          item.docId = item.docInfo.docId;
          item.docFile = item.docInfo.docFile;
          item.docType = item.docInfo.docType;
          item.docLabel = item.docInfo.docLabel;
          item.docSource = item.docInfo.docSource;
          item.docDate = this.transformDate(item.docInfo.docDate);
          item.startDate = this.transformDate(item.startDate);
          item.requestName = item.request.requestName;
        });

        this.setState({
          columns: columns,
          docs: data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  getOriginalDoc = (id) => {
    var request = {
      docId: id,
    };
    ApiDiContainer.ProxyApiClickHouse.getOriginalDoc(request)
      .then((response) => {
        const win = window.open(response.data, '_blank');
        if (win != null) {
          win.focus();
        }
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных из БД');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных из БД');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  showDeleteConfirm = (document) => {
    const deleteDoc = this.deleteDoc;
    confirm({
      title: 'Вы действительно хотите удалить документ (' + document.docLabel + ')?',
      okText: 'Да',
      okType: 'danger',
      cancelText: 'Нет',
      onOk() {
        deleteDoc(document);
      },
      onCancel() {},
    });
  };

  deleteDoc = (document) => {
    ApiDiContainer.ProxyApiClickHouse.deleteReport({
      resultId: document.resultId,
    })
      .then((response) => {
        message.success('Документ успешно удален из БД');
        this.getDocsPage();
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке данных документа');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке данных документа');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  render() {
    const { columns, docs } = this.state;

    return (
      <Card style={{ minHeight: 'calc(100vh - 140px)' }}>
        <div style={{ display: 'grid' }}>
          <div className={styles.centerTitle}>
            <Title>Результаты анализа</Title>
            <Space>
              <Button
                type="dashed"
                icon={<SyncOutlined spin={this.state.loading} />}
                onClick={() => this.getDocsPage()}
                className={styles.greenButton}>
                {' '}
                {this.state.loading ? 'Обновление' : 'Обновить'}
              </Button>
            </Space>
          </div>
          <Table
            rowKey="uid"
            columns={columns}
            dataSource={docs}
            loading={this.state.loading}
            pagination={docs.length > 10 ? true : false}
          />
        </div>
        <Modal
          title={'Просмотр'}
          open={this.state.viewText}
          width={1500}
          onCancel={() => {
            this.setState({
              viewText: false,
              docText: '',
            });
          }}
          footer={[
            <Button
              key="back"
              onClick={() => {
                this.setState({
                  viewText: false,
                  docText: '',
                });
              }}>
              Закрыть
            </Button>,
          ]}
          cancelText={'Отмена'}>
          <Divider orientation="">Содержимое документа</Divider>
          <Row gutter={16}>{this.state.docText}</Row>
        </Modal>
        <Modal
          title={'Просмотр отчета'}
          visible={this.state.viewReport}
          width={1500}
          onOk={() => {
            this.setState({
              viewReport: false,
            });
          }}
          onCancel={() => {
            this.setState({
              viewReport: false,
            });
          }}
          footer={[
            <Button
              key="back"
              onClick={() =>
                this.setState({
                  viewReport: false,
                })
              }>
              Закрыть
            </Button>,
          ]}>
          <RuleCheckReport report={this.state.reportObject} />
        </Modal>
      </Card>
    );
  }
}

export default ResultsModule;
