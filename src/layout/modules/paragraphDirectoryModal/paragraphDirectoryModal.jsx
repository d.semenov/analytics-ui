import React, { useState, useEffect } from 'react';
import { Modal, Table, Space, Button, message, Divider, Row } from 'antd';
import ApiDiContainer from '../../../service/apiService/apiDiContainer';

const ParagraphDirectoryModal = (props) => {
  const [loading, setLoading] = useState(false);
  const [pagination, setPagination] = useState({});
  const [paragraphs, setParagraphs] = useState([]);
  const [paragraphTitle, setParagraphTitle] = useState('');
  const [paragraphText, setParagraphText] = useState('');
  const [paragraphTextLoading, setParagraphTextLoading] = useState(false);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  useEffect(() => {
    getParagraphsList();
  }, []);

  const getParagraphsList = () => {
    setLoading(true);
    ApiDiContainer.ProxyApiClickHouse.getParagraphsList()
      .then((response) => {
        setParagraphs(response.data);
        setLoading(false);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
            /*message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке списка абзацев');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке списка абзацев');
          console.log('Error', error.message);
        }
        setLoading(false);
      });
  };

  const openParagraph = (id) => {
    setParagraphTextLoading(true);
    ApiDiContainer.ProxyApiClickHouse.getParagraph(id)
      .then((response) => {
        setParagraphText(response.data.itemOptionalValue);
        setParagraphTextLoading(false);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
            /*message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке текста абзаца');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке текста абзаца');
          console.log('Error', error.message);
        }
        setParagraphTextLoading(false);
      });
  };

  const setText = (id) => {
    ApiDiContainer.ProxyApiClickHouse.getParagraph(id)
      .then((response) => {
        props.setText(response.data.itemOptionalValue);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
            /*message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке текста абзаца');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке текста абзаца');
          console.log('Error', error.message);
        }
      });
  };

  const paragraphsColumns = [
    {
      title: 'Название абзаца',
      dataIndex: 'filterValue',
    },
    {
      title: 'Пользователь',
      dataIndex: 'user',
    },
    {
      title: 'Время добавления',
      dataIndex: 'eventTime',
      render: (text) => {
        var dateTime = text.split('T');
        var date = dateTime[0].split('-');
        return date[2] + '.' + date[1] + '.' + date[0] + ' ' + dateTime[1];
      },
    },
    {
      title: 'Действия',
      render: (_, record) => (
        <Space>
          <Button
            loading={paragraphTextLoading}
            onClick={() => {
              setParagraphTitle(record.filterValue);
              openParagraph(record.filterItemId);
            }}>
            Просмотр
          </Button>
        </Space>
      ),
    },
  ];

  const onSelectChange = (newSelectedRowKeys, newSelectedRows) => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    type: 'radio',
  };

  const checkSelected = () => {
    return selectedRowKeys.length !== 0;
  };

  return (
    <Modal
      title={'Выбрать абзац'}
      centered
      width={1500}
      closable={false}
      visible={props.visible}
      okText={'Выбрать абзац'}
      onOk={() => {
        if (checkSelected()) {
          setText(selectedRowKeys[0]);
          props.setVisible(false);
        } else {
          message.error('Абзац не выбран');
        }
      }}
      onCancel={() => {
        props.setVisible(false);
      }}>
      <Table
        rowKey="filterItemId"
        rowSelection={rowSelection}
        columns={paragraphsColumns}
        dataSource={paragraphs}
        pagination={paragraphs.length < 10 ? false : pagination}
      />
      {paragraphText.length === 0 ? null : (
        <div>
          <Divider orientation="left">Текст абзаца: {paragraphTitle}</Divider>
          <div style={{ maxHeight: '200px', overflowY: 'auto' }}>
            <p>{paragraphText}</p>
          </div>

          <Row justify="end">
            <Button
              onClick={() => {
                setParagraphTitle('');
                setParagraphText('');
              }}>
              Скрыть текст
            </Button>
          </Row>
        </div>
      )}
    </Modal>
  );
};

export default ParagraphDirectoryModal;
