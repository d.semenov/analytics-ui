import React, { useState, useEffect } from 'react';
import 'antd/dist/antd.css';
import { withRouter } from 'react-router';
import {
  Card,
  Form,
  Input,
  Select,
  Space,
  Button,
  Modal,
  Table,
  Tooltip,
  Row,
  Col,
  Switch,
  Typography,
  message,
} from 'antd';
import { DeleteOutlined, FileTextOutlined, EditOutlined } from '@ant-design/icons';
import Title from 'antd/es/typography/Title';
import AddParagraphFormModal from '../../Forms/addParagraphForm/addParagraphFormModal';
import ApiDiContainer from '../../../service/apiService/apiDiContainer';
import TextArea from 'antd/lib/input/TextArea';

const { Text } = Typography;
const { Option } = Select;
const { confirm } = Modal;

const FilterCreateModule = (props) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [editFilterValue, setEditFilterValue] = useState('Выберите фильтр для редактирования');
  const [removingFilter, setRemovingFilter] = useState(null);
  const [filters, setFilters] = useState([]);
  const [sources, setSources] = useState([]);
  const [paragraphs, setParagraphs] = useState([]);
  const [entities, setEntities] = useState([]);
  const [types, setTypes] = useState([]);
  const [paragraphText, setParagraphText] = useState('');
  const [paragraphModal, setParagraphModal] = useState(false);
  const [paragraphModalLoading, setParagraphModalLoading] = useState(false);

  const [paragraphEdit, setParagraphEdit] = useState(null);
  const [paragraphEditText, setParagraphEditText] = useState('');
  const [paragraphEditModal, setParagraphEditModal] = useState(false);
  const [paragraphEditModalLoading, setParagraphEditModalLoading] = useState(false);

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [pagination, setPagination] = useState({});
  const [addParagraphModal, setAddParagraphModal] = useState(false);

  const getFilterList = () => {
    setLoading(true);
    ApiDiContainer.ProxyApiClickHouse.getFiltersList()
      .then((response) => {
        setFilters(response.data);
        setLoading(false);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
            /*message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке списка правил');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке списка правил');
          console.log('Error', error.message);
        }
        setLoading(false);
      });
  };

  const getTypesList = () => {
    setLoading(true);
    ApiDiContainer.ProxyApiClickHouse.getTypesList()
      .then((response) => {
        setTypes(response.data);
        setLoading(false);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
            /*message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке списка абзацев');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке списка абзацев');
          console.log('Error', error.message);
        }
        setLoading(false);
      });
  };

  const getEntitiesList = () => {
    setLoading(true);
    ApiDiContainer.ProxyApiClickHouse.getEntitiesList()
      .then((response) => {
        setEntities(response.data);
        setLoading(false);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
            /*message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке списка сущностей');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке списка сущностей');
          console.log('Error', error.message);
        }
        setLoading(false);
      });
  };

  const getSourcesList = () => {
    setLoading(true);
    ApiDiContainer.ProxyApiClickHouse.getSourcesList()
      .then((response) => {
        setSources(response.data);
        setLoading(false);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
            /*message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке списка источников');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке списка источников');
          console.log('Error', error.message);
        }
        setLoading(false);
      });
  };

  const getParagraphsList = () => {
    setLoading(true);
    ApiDiContainer.ProxyApiClickHouse.getParagraphsList()
      .then((response) => {
        setParagraphs(response.data);
        setLoading(false);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
            /*message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке списка абзацев');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке списка абзацев');
          console.log('Error', error.message);
        }
        setLoading(false);
      });
  };

  const openParagraph = (id) => {
    ApiDiContainer.ProxyApiClickHouse.getParagraph(id)
      .then((response) => {
        setParagraphText(response.data.itemOptionalValue);
        setParagraphModal(true);
        setParagraphModalLoading(false);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке текста абзаца');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке текста абзаца');
          console.log('Error', error.message);
        }
        setParagraphModalLoading(false);
      });
  };

  const openEditParagraph = (id) => {
    ApiDiContainer.ProxyApiClickHouse.getParagraph(id)
      .then((response) => {
        setParagraphEditText(response.data.itemOptionalValue);
        setParagraphEditModal(true);
        setParagraphEditModalLoading(false);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
            /*message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });*/
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке текста абзаца');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке текста абзаца');
          console.log('Error', error.message);
        }
        setParagraphEditModalLoading(false);
      });
  };

  const commitParagraph = () => {
    let paragraph = {
      title: paragraphEdit.filterValue,
      text: paragraphEditText,
    };
    removeParagraph(paragraphEdit, true);
    addNewParagraph(paragraph);
    closeEditParagraph();
  };

  const removeFilter = (filter) => {
    ApiDiContainer.ProxyApiClickHouse.deleteFilter(removingFilter)
      .then((response) => {
        setEditMode(false);
        setEditFilterValue('Выберите правило для редактирования');
        setRemovingFilter(null);
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            this.props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при обновлении правила');
          console.log(error.request);
        } else {
          message.error('Ошибка при обновлении правила');
          console.log('Error', error.message);
        }
      });
  };

  const saveFilter = (filter) => {
    ApiDiContainer.ProxyApiClickHouse.saveFilter(filter)
      .then((response) => {
        editMode
          ? message.success('Правило успешно изменено')
          : message.success('Правило успешно создано');
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
          }
          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          editMode
            ? message.error('Ошибка при изменении правила')
            : message.error('Ошибка при изменении правила');

          console.log(error.request);
        } else {
          editMode
            ? message.error('Ошибка при изменении правила')
            : message.error('Ошибка при изменении правила');
          console.log('Error', error.message);
        }
      });
  };

  const addNewParagraph = (paragraph) => {
    ApiDiContainer.ProxyApiClickHouse.addParagraph(paragraph)
      .then((response) => {
        if (paragraphEdit !== null) {
          message.success('Абзац успешно изменен');
        } else {
          message.success('Абзац успешно добавлен');
        }
        getParagraphsList();
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
          }

          if (error.response.status === 500) {
            console.log(error.response.data.error);
            message.error('Внутренняя ошибка сервера');
          }
        } else if (error.request) {
          message.error('Ошибка при сохранении нового абзаца');
          console.log(error.request);
        } else {
          message.error('Ошибка при сохранении нового абзаца');
          console.log('Error', error.message);
        }
      });
  };

  const removeParagraph = (paragraph, changeFlag) => {
    console.log(paragraph);
    setLoading(true);
    ApiDiContainer.ProxyApiClickHouse.removeParagraph(paragraph)
      .then((response) => {
        if (!changeFlag) {
          message.success('Абзац успешно удален');
          getParagraphsList();
        }
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            props.logged();
          }

          if (error.response.status === 500) {
            message.error(error.response.data);
          }
        } else if (error.request) {
          message.error('Ошибка при удалении абзаца');
          console.log(error.request);
        } else {
          message.error('Ошибка при удалении абзаца');
          console.log('Error', error.message);
        }
        setLoading(false);
      });
  };

  const closeParagraph = () => {
    setParagraphModal(false);
    setParagraphText('');
  };

  const closeEditParagraph = () => {
    setParagraphEditModal(false);
    setParagraphEditText('');
    setParagraphEdit(null);
  };

  const showDeleteConfirm = (paragraph) => {
    confirm({
      title: 'Вы действительно хотите удалить абзац (' + paragraph.filterValue + ')?',
      okText: 'Да',
      okType: 'danger',
      cancelText: 'Нет',
      onOk() {
        removeParagraph(paragraph, false);
      },
      onCancel() {},
    });
  };

  const paragraphsColumns = [
    {
      title: 'Название абзаца',
      dataIndex: 'filterValue',
    },
    {
      title: 'Пользователь',
      dataIndex: 'user',
    },
    {
      title: 'Время добавления',
      dataIndex: 'eventTime',
      render: (text) => {
        var dateTime = text.split('T');
        var date = dateTime[0].split('-');
        return date[2] + '.' + date[1] + '.' + date[0] + ' ' + dateTime[1];
      },
    },
    {
      title: 'Действия',
      width: 120,
      render: (_, record) => (
        <Space>
          <Tooltip title="Просмотр абзаца">
            <Button
              style={{ color: '#31aa2d' }}
              shape="circle"
              type="text"
              icon={<FileTextOutlined />}
              loading={paragraphModalLoading}
              onClick={() => {
                openParagraph(record.filterItemId);
              }}></Button>
          </Tooltip>
          <Tooltip title="Редактировать абзац">
            <Button
              style={{ color: '#2d4db4' }}
              shape="circle"
              type="text"
              icon={<EditOutlined />}
              loading={paragraphModalLoading}
              onClick={() => {
                setParagraphEdit(record);
                openEditParagraph(record.filterItemId);
              }}></Button>
          </Tooltip>
          <Tooltip title="Удалить">
            <Button
              style={{ color: '#a12e12' }}
              shape="circle"
              type="text"
              icon={<DeleteOutlined />}
              onClick={() => {
                showDeleteConfirm(record);
              }}></Button>
          </Tooltip>
          <Modal
            title={'Текст абзаца'}
            visible={paragraphModal}
            width={1500}
            onCancel={closeParagraph}
            onOk={closeParagraph}
            footer={[
              <Button key="back" onClick={closeParagraph}>
                Закрыть
              </Button>,
            ]}>
            <Row gutter={16}>{paragraphText}</Row>
          </Modal>
          <Modal
            title={'Текст абзаца для редактирования'}
            visible={paragraphEditModal}
            width={1500}
            onCancel={closeEditParagraph}
            onOk={commitParagraph}
            okText="Сохранить изменения"
            cancelText="Отмена">
            <Row gutter={16}>
              <TextArea
                rows={30}
                value={paragraphEditText}
                onChange={(e) => setParagraphEditText(e.target.value)}
              />
            </Row>
          </Modal>
        </Space>
      ),
    },
  ];

  const onSelectChange = (newSelectedRowKeys, newSelectedRows) => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
    form.setFieldsValue({ paragraph: newSelectedRowKeys });
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  const setDefaultValues = (id) => {
    let filter = filters.find((f) => f.requestId === id);
    setEditFilterValue(filter.requestName);
    setRemovingFilter(filter);

    var initialValues = {
      name: '',
      description: '',
      type: [],
      entity: [],
      source: [],
    };
    initialValues.name = filter.requestName;
    initialValues.description = filter.requestDescription;
    let tableSelectorArray = [];
    for (const item of filter.requestItems) {
      if (item.filterItem.filterTypes.filterTypeAlias === 'docSource') {
        initialValues.source.push(item.filterItem.filterItemId);
      }
      if (item.filterItem.filterTypes.filterTypeAlias === 'entityType') {
        initialValues.entity.push(item.filterItem.filterItemId);
      }
      if (item.filterItem.filterTypes.filterTypeAlias === 'docType') {
        initialValues.type.push(item.filterItem.filterItemId);
      }
      if (item.filterItem.filterTypes.filterTypeAlias === 'paragraph') {
        tableSelectorArray.push(item.filterItem.filterItemId);
      }
    }
    form.setFieldsValue(initialValues);
    onSelectChange(tableSelectorArray);
  };

  useEffect(() => {
    getFilterList();
    getTypesList();
    getEntitiesList();
    getSourcesList();
    getParagraphsList();
  }, []);

  const changeMode = (flag) => {
    if (flag === false) {
      form.resetFields();
      onSelectChange([]);
      setEditFilterValue('Выберите фильтр для редактирования');
    }
    setEditMode(flag);
  };
  const createFilter = () => {
    form
      .validateFields()
      .then((values) => {
        onFinish(values);
        form.resetFields();
        setSelectedRowKeys([]);
      })
      .catch((info) => {
        console.log('Validate Failed:', info);
      });
  };

  const onFinish = (values) => {
    console.log('Received values of form: ', values);
    var requestObject = {};

    requestObject['requestName'] = values.name;
    requestObject['userId'] = 1;

    if (values.description !== undefined) {
      requestObject['requestDescription'] = values.description;
    }

    var requestItems = [];
    var requestItem = {};
    var filterItem = {};
    if (values.type !== undefined) {
      for (const typeIns of values.type) {
        filterItem['filterItemId'] = typeIns;
        requestItem['filterItem'] = filterItem;
        requestItems.push(requestItem);
        requestItem = {};
        filterItem = {};
      }
    }

    if (values.entity !== undefined) {
      for (const entityIns of values.entity) {
        filterItem['filterItemId'] = entityIns;
        requestItem['filterItem'] = filterItem;
        requestItems.push(requestItem);
        requestItem = {};
        filterItem = {};
      }
    }

    if (values.source !== undefined) {
      for (const sourceIns of values.source) {
        filterItem['filterItemId'] = sourceIns;
        requestItem['filterItem'] = filterItem;
        requestItems.push(requestItem);
        requestItem = {};
        filterItem = {};
      }
    }

    if (values.paragraph !== undefined) {
      for (const parIns of values.paragraph) {
        filterItem['filterItemId'] = parIns;
        requestItem['filterItem'] = filterItem;
        requestItems.push(requestItem);
        requestItem = {};
        filterItem = {};
      }
    }

    requestObject['requestItems'] = requestItems;
    saveFilter(requestObject);

    if (editMode) {
      removeFilter(removingFilter);
    }
    getFilterList();
  };

  const formItemLayout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 5,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 19,
      },
    },
  };

  return (
    <Card loading={loading} style={{ minHeight: 'calc(100vh - 140px)' }}>
      <div style={{ display: 'grid' }}>
        {editMode ? <Title>Редактирование правила</Title> : <Title>Создание нового правила</Title>}

        <Row gutter={[0, 48]}>
          <Col span={24}>
            <Row align="middle" gutter={16}>
              <Col flex="0 0 20.83333333%">
                <Space>
                  <Text>Режим редактирования:</Text>
                  <Switch checked={editMode} onChange={changeMode} />
                </Space>
              </Col>
              <Col flex="auto">
                <Select
                  disabled={!editMode}
                  placeholder="Выберите существующее правило для изменения"
                  style={{ width: '100%' }}
                  value={editFilterValue}
                  onChange={setDefaultValues}>
                  {filters.map((f) => (
                    <Option value={f.requestId}>{f.requestName}</Option>
                  ))}
                </Select>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Form
              {...formItemLayout}
              form={form}
              name="register"
              onFinish={onFinish}
              scrollToFirstError>
              <Form.Item
                name="name"
                label="Название правила"
                rules={[
                  {
                    required: true,
                    message: 'Пожалуйста введите название правила',
                  },
                ]}>
                <Input />
              </Form.Item>
              <Form.Item name="description" label="Описание правила">
                <Input />
              </Form.Item>
              <Form.Item name="type" label="Типы документов">
                <Select mode="multiple" allowClear placeholder="Выберите типы документов">
                  {types.map((t) => (
                    <Option value={t.filterItemId}>{t.filterValue}</Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item name="entity" label="Сущности">
                <Select mode="multiple" allowClear placeholder="Выберите сущности">
                  {entities.map((e) => (
                    <Option value={e.filterItemId}>{e.filterValue}</Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item name="source" label="Источники документов">
                <Select mode="multiple" allowClear placeholder="Выберите источники документов">
                  {sources.map((s) => (
                    <Option value={s.filterItemId}>{s.filterValue}</Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item name="paragraph" label="Выбор абзаца">
                <Table
                  rowKey="filterItemId"
                  rowSelection={rowSelection}
                  columns={paragraphsColumns}
                  dataSource={paragraphs}
                  pagination={paragraphs.length < 10 ? false : pagination}
                />
              </Form.Item>
              <Row justify="end">
                <Button type="primary" onClick={() => setAddParagraphModal(true)}>
                  Создать новый абзац
                </Button>
                <AddParagraphFormModal
                  title={'Вставьте текст абзаца'}
                  visible={addParagraphModal}
                  setVisible={setAddParagraphModal}
                  save={addNewParagraph}
                />
              </Row>
              <Form.Item>
                <Button type="primary" onClick={createFilter}>
                  {editMode ? 'Обновить правило' : 'Создать правило'}
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </div>
    </Card>
  );
};

export default withRouter(FilterCreateModule);
