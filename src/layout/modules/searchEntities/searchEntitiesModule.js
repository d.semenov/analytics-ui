import React from 'react';
import { Button, Card, message, Row, Col, Select, Tree, Space, Divider, Typography } from 'antd';
import Title from 'antd/es/typography/Title';
import {
  DoubleLeftOutlined,
  DoubleRightOutlined,
  LeftOutlined,
  RightOutlined,
  SearchOutlined,
} from '@ant-design/icons';
import ApiDiContainer from '../../../service/apiService/apiDiContainer';
import InsertTextFormModal from '../../Forms/insertTextForm/insertTextFormModal';
import './searchEntities.module.css';

const { Option } = Select;
const { Text } = Typography;

class SearchEntitiesModule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mainText: '',
      currentMark: -1,
      previousMark: -1,
      markupCoords: [],
      selectedEntity: [],
      columns: [],
      entities: [],

      loading: false,
      mainTextLoading: false,
      treeLoading: false,
      insertTextModal: false,
      disableAll: false,

      docs: [],
      searchDoc: '',
      currentDoc: '',

      entitiesTree: [],
    };
  }

  componentDidMount() {
    this.setState(
      {
        currentDoc: this.props.currentDoc,
      },
      function () {
        if (this.state.currentDoc !== '') {
          this.getDocText(this.state.currentDoc);
        }
      },
    );

    this.getEntitiesList();
    this.getDocs();
  }

  getEntitiesList = () => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getEntitiesList()
      .then((response) => {
        this.setState({
          entities: response.data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });
          }
        } else if (error.request) {
          message.error('Ошибка при списка сущностей');
          console.log(error.request);
        } else {
          message.error('Ошибка при списка сущностей');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  getDocs = () => {
    this.setState({ loading: true });
    ApiDiContainer.ProxyApiClickHouse.getDocs()
      .then((response) => {
        this.setState({
          docs: response.data,
          loading: false,
        });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });
          }
        } else if (error.request) {
          message.error('Ошибка при загрузке списка документов');
          console.log(error.request);
        } else {
          message.error('Ошибка при загрузке списка документов');
          console.log('Error', error.message);
        }
        this.setState({
          loading: false,
        });
      });
  };

  getDocText = (doc) => {
    this.setState({ mainTextLoading: true });
    if (doc.uid !== null) {
      ApiDiContainer.ProxyApiClickHouse.getUserDoc(doc.uid)
        .then((response) => {
          this.setState({
            mainText: response.data.text.replace(/(\r\n|\n|\r)/gm, ' '),
            mainTextLoading: false,
          });
        })
        .catch((error) => {
          if (error.response) {
            if (error.response.status === 403) {
              message.error('Сессия закрыта');
              this.setState({
                loading: false,
                logged: false,
              });
            }
          } else if (error.request) {
            message.error('Ошибка при загрузке данных документа');
            console.log(error.request);
          } else {
            message.error('Ошибка при загрузке данных документа');
            console.log('Error', error.message);
          }
          this.setState({ mainTextLoading: false });
        });
    } else {
      ApiDiContainer.ProxyApiClickHouse.getDoc({ docId: doc.docId })
        .then((response) => {
          this.setState({
            mainText: response.data.replace(/(\r\n|\n|\r)/gm, ' '),
            mainTextLoading: false,
          });
        })
        .catch((error) => {
          if (error.response) {
            if (error.response.status === 403) {
              message.error('Сессия закрыта');
              this.setState({
                loading: false,
                logged: false,
              });
            }
          } else if (error.request) {
            message.error('Ошибка при загрузке данных документа');
            console.log(error.request);
          } else {
            message.error('Ошибка при загрузке данных документа');
            console.log('Error', error.message);
          }
          this.setState({ mainTextLoading: false });
        });
    }
  };

  setDocText = (text) => {
    this.setState({
      mainText: text.replace(/(\r\n|\n|\r)/gm, ' '),
      currentDoc: '',
      insertTextModal: false,
    });
  };

  setInsertModal = (visible) => {
    this.setState({
      insertTextModal: visible,
    });
  };

  resetState = () => {
    this.setState({
      searchDoc: '',
      currentDoc: '',
      selectedEntity: [],
    });
  };

  searchEntities = () => {
    if (this.state.mainText === '') {
      message.error('Пожалуйста заполните основное поле текстом');
      return;
    }
    if (this.state.selectedEntity.length === 0) {
      message.error('Пожалуйста выберите сущности для поиска');
      return;
    }

    this.setState({ treeLoading: true, disableAll: true });

    let request = {
      entities: this.state.selectedEntity.map((el) => el.value),
      text: this.state.mainText,
    };

    ApiDiContainer.ProxyApiClickHouse.getEntyties(request)
      .then((response) => {
        this.responseToTree(response.data);
        this.setState({ treeLoading: false, disableAll: false });
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 403) {
            message.error('Сессия закрыта');
            this.setState({
              loading: false,
              logged: false,
            });
          }
        } else if (error.request) {
          message.error('Ошибка при поиске сущностей документа');
          console.log(error.request);
        } else {
          message.error('Ошибка при поиске сущностей документа');
          console.log('Error', error.message);
        }
        this.setState({ treeLoading: false, disableAll: false });
      });
  };

  updateCurrentEntity = (el, selector) => {
    console.log(selector);
    this.setState({ selectedEntity: selector });
  };

  handleSearchSelect = (value) => {
    if (value) {
      this.setState({ searchDoc: value });
    } else {
      this.setState({ searchDoc: '' });
    }
  };

  updateCurrentDoc = (el, selector) => {
    let currentDoc = '';
    if (selector !== undefined)
      currentDoc = this.state.docs.filter((el) => el.docId === selector.value)?.pop();
    this.setState({
      currentDoc: currentDoc,
      searchDoc: '',
    });
  };

  onTreeSelect = (selectedKeys, info) => {
    let isTreeLeaf = (selectedKeys[0] + '').includes('-');
    if (isTreeLeaf) {
      this.setState(
        {
          markupCoords: info.selectedNodes[0].search,
        },
        function () {
          this.createMarkup();
          this.resetScroll();
        },
      );
    }
  };

  responseToTree = (response) => {
    let entitiesTree = [];

    let typeKey = 0;
    for (const typeInstance of response) {
      let treeTypeInstance = {};
      treeTypeInstance.title = typeInstance.type;
      treeTypeInstance.key = typeKey;
      treeTypeInstance.children = [];
      let infoKey = 0;

      for (const infoInstance of typeInstance.info) {
        //tree
        let infoTreeInstance = {};
        infoTreeInstance.title = infoInstance.value;
        infoTreeInstance.key = typeKey + '-' + infoKey;
        infoTreeInstance.search = infoInstance.intext;
        treeTypeInstance.children.push(infoTreeInstance);
        infoKey++;
      }
      if (treeTypeInstance.children.length !== 0) {
        entitiesTree.push(treeTypeInstance);
        typeKey++;
      }
    }
    console.log(entitiesTree);
    this.setState({
      entitiesTree: entitiesTree,
    });
  };

  createMarkup = () => {
    let text = this.state.mainText;
    let markupArray = this.state.markupCoords;
    let shift = 0;
    let id = 0;

    for (const i of markupArray) {
      let markHead = '<mark-text id="mark-' + id + '">';
      let markTail = '</mark-text>';
      text = [text.slice(0, i.start + shift), markHead, text.slice(i.start + shift)].join('');
      shift += markHead.length;
      text = [text.slice(0, i.stop + shift), markTail, text.slice(i.stop + shift)].join('');
      shift += markTail.length;
      id++;
    }

    return { __html: text };
  };

  scrollFirst = () => {
    if (this.state.markupCoords.length === 0) return;
    this.setState(
      {
        previousMark: this.state.currentMark,
        currentMark: 0,
      },
      function () {
        this.paintBlue(this.state.previousMark);
        this.paintYellow(this.state.currentMark);
        this.scrollTo(this.state.currentMark);
      },
    );
  };

  scrollLast = () => {
    if (this.state.markupCoords.length === 0) return;
    this.setState(
      {
        previousMark: this.state.currentMark,
        currentMark: this.state.markupCoords.length - 1,
      },
      function () {
        this.paintBlue(this.state.previousMark);
        this.paintYellow(this.state.currentMark);
        this.scrollTo(this.state.currentMark);
      },
    );
  };

  scrollNext = () => {
    if (this.state.currentMark >= this.state.markupCoords.length - 1) {
      this.scrollFirst();
      return;
    }
    this.setState(
      {
        previousMark: this.state.currentMark,
        currentMark: this.state.currentMark + 1,
      },
      function () {
        this.paintBlue(this.state.previousMark);
        this.paintYellow(this.state.currentMark);
        this.scrollTo(this.state.currentMark);
      },
    );
  };

  scrollPrev = () => {
    if (this.state.currentMark <= 0) {
      this.scrollLast();
      return;
    }
    this.setState(
      {
        previousMark: this.state.currentMark,
        currentMark: this.state.currentMark - 1,
      },
      function () {
        this.paintBlue(this.state.previousMark);
        this.paintYellow(this.state.currentMark);
        this.scrollTo(this.state.currentMark);
      },
    );
  };

  scrollTo = (id) => {
    const markPrefix = 'mark-';
    let currentMark = document.getElementById(markPrefix + id);
    if (currentMark !== null) currentMark.scrollIntoView({ block: 'center', behavior: 'smooth' });
  };

  paintYellow = (id) => {
    if (id === -1) return;
    var markPrefix = 'mark-';
    var markElement = document.getElementById(markPrefix + id);

    var spanElement = document.createElement('view-text');
    spanElement.setAttribute('id', markPrefix + id);
    var markText = document.createTextNode(markElement.textContent);
    spanElement.appendChild(markText);

    markElement.replaceWith(spanElement);
  };

  paintBlue = (id) => {
    if (id === -1) return;
    var markPrefix = 'mark-';
    var markElement = document.getElementById(markPrefix + id);

    var spanElement = document.createElement('mark-text');
    spanElement.setAttribute('id', markPrefix + id);
    var markText = document.createTextNode(markElement.textContent);
    spanElement.appendChild(markText);

    markElement.replaceWith(spanElement);
  };

  resetScroll = () => {
    this.setState({
      previousMark: -1,
      currentMark: -1,
    });
  };

  render() {
    const { selectedEntity, entities, currentDoc, docs } = this.state;

    return (
      <Card style={{ minHeight: 'calc(100vh - 140px)' }}>
        <div style={{ display: 'grid' }}>
          <Title style={{ marginBottom: '5' }}>Выявление сущностей</Title>
          <Row wrap={false} justify="space-between">
            <Col>
              <Space>
                <span>Выберите документ:</span>
                <Select
                  disabled={this.state.disableAll}
                  mode="single"
                  showSearch
                  allowClear={this.state.currentDoc !== ''}
                  style={{ width: 350, minWidth: 230 }}
                  filterOption={false}
                  onSearch={this.handleSearchSelect}
                  notFoundContent={null}
                  value={
                    currentDoc !== ''
                      ? currentDoc.docLabel +
                        (currentDoc.docId === '' ? '' : '(' + currentDoc.docId + ')')
                      : 'Не выбрано'
                  }
                  onChange={this.updateCurrentDoc}>
                  {docs
                    .filter(
                      (el) =>
                        el.docId.toLowerCase().includes(this.state.searchDoc.toLowerCase() || '') ||
                        el.docLabel
                          .toLowerCase()
                          .includes(this.state.searchDoc.toLowerCase() || ''),
                    )
                    .sort((a, b) => (a.docId > b.docId ? 1 : -1))
                    .map((el) => (
                      <Option key={el.docId} value={el.docId}>
                        {el.docLabel} ({el.docId})
                      </Option>
                    ))}
                </Select>
                <Button
                  type="primary"
                  disabled={this.state.disableAll || this.state.currentDoc === ''}
                  loading={this.state.mainTextLoading}
                  onClick={() => {
                    this.getDocText(currentDoc);
                    this.setState({
                      entitiesTree: [],
                      markupCoords: [],
                    });
                  }}>
                  Получить текст
                </Button>
                <Button
                  type="primary"
                  disabled={this.state.disableAll}
                  onClick={() => {
                    this.setState({
                      insertTextModal: true,
                      entitiesTree: [],
                      markupCoords: [],
                    });
                  }}>
                  Вставить текст из буфера
                </Button>
                <InsertTextFormModal
                  title={'Вставьте текст из буфера обмена [CTRL-V]'}
                  visible={this.state.insertTextModal}
                  setVisible={this.setInsertModal}
                  setText={this.setDocText}
                />
              </Space>
            </Col>
            <Col>
              <Space>
                <span>Выберите сущность: </span>
                <Select
                  disabled={this.state.disableAll}
                  mode="multiple"
                  showSearch
                  style={{ width: 350, minWidth: 230 }}
                  value={selectedEntity?.value}
                  onChange={this.updateCurrentEntity}
                  filterOption={(input, option) =>
                    option.value.toLowerCase().includes(input.toLowerCase())
                  }
                  filterSort={(optionA, optionB) =>
                    optionA.value.toLowerCase().localeCompare(optionB.value.toLowerCase())
                  }>
                  {entities.map((el) => (
                    <Option key={el.filterItemId} value={el.filterValue}>
                      {el.filterValue}
                    </Option>
                  ))}
                </Select>
                <Button
                  type="primary"
                  loading={this.state.treeLoading}
                  icon={<SearchOutlined />}
                  onClick={() => {
                    console.log(this.state.currentDoc);
                    this.searchEntities();
                  }}>
                  Найти
                </Button>
              </Space>
            </Col>
          </Row>
          <Divider></Divider>
          <Row gutter={20}>
            <Col span={16}>
              {this.state.mainText === '' ? (
                <Text type="secondary">Получите текст из документа</Text>
              ) : (
                <div style={{ maxHeight: 'calc(100vh - 340px)', overflowY: 'auto' }}>
                  <div dangerouslySetInnerHTML={this.createMarkup()}></div>
                </div>
              )}
            </Col>
            <Col span={8}>
              <Row justify="start" align="middle" gutter={8}>
                <Col span={5}>
                  <Text>Навигация:</Text>
                </Col>
                <Col span={19}>
                  <Space>
                    <Button
                      type="primary"
                      disabled={this.state.markupCoords.length === 0}
                      icon={<DoubleLeftOutlined />}
                      onClick={() => this.scrollFirst()}
                    />
                    <Button
                      type="primary"
                      disabled={this.state.markupCoords.length === 0}
                      icon={<LeftOutlined />}
                      onClick={() => this.scrollPrev()}
                    />
                    <Button
                      type="primary"
                      disabled={this.state.markupCoords.length === 0}
                      icon={<RightOutlined />}
                      onClick={() => this.scrollNext()}
                    />
                    <Button
                      type="primary"
                      disabled={this.state.markupCoords.length === 0}
                      icon={<DoubleRightOutlined />}
                      onClick={() => this.scrollLast()}
                    />
                  </Space>
                </Col>
              </Row>
              <br />
              <Row>
                <h3>Результаты проверки:</h3>
              </Row>
              <Row>
                {this.state.entitiesTree.length === 0 ? (
                  <Text type="secondary">Пусто</Text>
                ) : (
                  <Tree
                    height={'calc(100vh - 420px)'}
                    showLine={true}
                    onSelect={this.onTreeSelect}
                    treeData={this.state.entitiesTree}
                  />
                )}
              </Row>
            </Col>
          </Row>
        </div>
      </Card>
    );
  }
}
export default SearchEntitiesModule;
