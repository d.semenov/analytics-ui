import React, { useState } from 'react';
import 'antd/dist/antd.css';
import { Modal, Button, message, Form, Input, Upload } from 'antd';
import { FilePdfOutlined, FileWordOutlined, UploadOutlined } from '@ant-design/icons';
import ApiDiContainer from '../../../service/apiService/apiDiContainer';

const UploadFileFormModal = (props) => {
  const [form] = Form.useForm();
  const [fileList, setFileList] = useState([]);
  const [uploading, setUploading] = useState(false);

  const onFinish = async (values) => {
    //setUploading(false);
    //props.setVisible(false);
  };

  const handleUpload = async (values) => {
    const formData = new FormData();
    formData.append(`file`,fileList[0], fileList[0].name);
    formData.append(`user`,props.user);
    formData.append(`type`,values.type);
    formData.append(`label`,values.name);
    //setUploading(true);

    await ApiDiContainer.ProxyApiClickHouse.uploadUserDocFromFile(formData)
      .then((res) => {
        message.success('Файл загружен')
        //setUploading(false);
        //setFileList([]);
      })
      .catch((err) => {
        message.error('Файл не загружен')
      })
  }

  const uploadProps = {
    multiple: false,
    onRemove: (file) => {
      setFileList([]);
    },
    iconRender: (file) => {
      if (file.name.indexOf('pdf') > 0) {
        return <FilePdfOutlined />;
      }

      if (file.name.indexOf('docx') > 0) {
        return <FileWordOutlined />;
      }
    },
    beforeUpload: (file) => {
      if ((file.name.indexOf('pdf') > 0) || (file.name.indexOf('docx') > 0)) {
        setFileList([...fileList, file]);
      } else message.error(`${file.name} поддерживаются файлы с расширением docx или pdf`)
      return false;
    },
    fileList,
  };

  const normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e?.fileList;
  };

  return (
    <Modal
      title={'Добавление пользовательского документа'}
      centered
      width={1000}
      closable={false}
      visible={props.visible}
      okText={'Начать выгрузку'}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            handleUpload(values);
            form.resetFields();
            onFinish(values);
          })
          .catch((info) => {
            console.log('Validate Failed:', info);
          });
      }}
      onCancel={() => {
        props.setVisible(false);
      }}>
      <Form form={form} name="upload" onFinish={onFinish} scrollToFirstError>
        <Form.Item
          name="name"
          label="Имя документа"
          rules={[
            {
              required: true,
              message: 'Пожалуйста введите имя документа',
            },
          ]}>
          <Input />
        </Form.Item>
        <Form.Item
          name="type"
          label="Тип документа"
          rules={[
            {
              required: true,
              message: 'Пожалуйста введите тип документа',
            },
          ]}>
          <Input />
        </Form.Item>
        <Form.Item
          name="file"
          label="Файл документа"
          valuePropName="fileList"
          getValueFromEvent={normFile}
          rules={[
            {
              required: true,
              message: 'Пожалуйста выберите файл документа',
            },
          ]}>
          <Upload {...uploadProps} 
                  listType="picture"
                  maxCount={1}>
            <Button icon={<UploadOutlined />}>Выбор файла</Button>
          </Upload>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default UploadFileFormModal;
