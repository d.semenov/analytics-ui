import React from 'react';
import { Modal, Form, Input } from 'antd';

const { TextArea } = Input;

const AddParagraphFormModal = (props) => {
  const [form] = Form.useForm();

  const onFinish = (values) => {
    props.save(values);
    props.setVisible(false);
  };

  return (
    <Modal
      title={props.title}
      centered
      width={1500}
      closable={false}
      visible={props.visible}
      okText={'Добавить абзац'}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            form.resetFields();
            onFinish(values);
          })
          .catch((info) => {
            console.log('Validate Failed:', info);
          });
      }}
      onCancel={() => {
        props.setVisible(false);
      }}>
      <Form
        form={form}
        layout="vertical"
        name="new_paragraph"
        onFinish={onFinish}
        scrollToFirstError>
        <Form.Item
          name="title"
          label="Имя абзаца"
          rules={[
            {
              required: true,
              message: 'Пожалуйста введите название абзаца',
            },
          ]}>
          <Input />
        </Form.Item>
        <Form.Item
          name="text"
          label="Содержимое абзаца"
          rules={[
            {
              required: true,
              message: 'Пожалуйста введите текст абзаца',
            },
          ]}>
          <TextArea rows={25} />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default AddParagraphFormModal;
