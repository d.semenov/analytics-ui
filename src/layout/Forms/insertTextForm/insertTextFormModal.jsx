import React, { useState } from 'react';
import { Modal, Button, Input } from 'antd';

const { TextArea } = Input;

const InsertTextFormModal = (props) => {
  const [text, setText] = useState('');

  const onChange = (e) => {
    setText(e.target.value);
  };

  const saveText = () => {
    props.setText(text);
    props.setVisible(false);
  };

  return (
    <Modal
      title={props.title}
      centered
      width={1500}
      closable={false}
      visible={props.visible}
      onOk={() => {
        props.setVisible(false);
      }}
      onCancel={() => {
        props.setVisible(false);
      }}
      footer={[
        <Button
          type="primary"
          onClick={() => {
            saveText();
          }}>
          Сохранить
        </Button>,
      ]}>
      <TextArea rows={25} onChange={onChange} />
    </Modal>
  );
};

export default InsertTextFormModal;
