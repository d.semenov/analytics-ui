import React from 'react';
import 'antd/dist/antd.css';
import { Modal, Form, Input, Select } from 'antd';
import './createFilterForm.modal.css';

const { Option } = Select;

const CreateFilterFormModal = (props) => {
  const [form] = Form.useForm();

  const onFinish = (values) => {
    console.log('Received values of form: ', values);
    var requestObject = {};

    requestObject['requestName'] = values.name;
    requestObject['userId'] = 1;

    var requestItems = [];
    var requestItem = {};
    var filterItem = {};
    for (const typeIns of values.type) {
      filterItem['filterItemId'] = typeIns;
      requestItem['filterItem'] = filterItem;
      requestItems.push(requestItem);
      requestItem = {};
      filterItem = {};
    }

    for (const entityIns of values.entity) {
      filterItem['filterItemId'] = entityIns;
      requestItem['filterItem'] = filterItem;
      requestItems.push(requestItem);
      requestItem = {};
      filterItem = {};
    }

    for (const sourceIns of values.source) {
      filterItem['filterItemId'] = sourceIns;
      requestItem['filterItem'] = filterItem;
      requestItems.push(requestItem);
      requestItem = {};
      filterItem = {};
    }
    requestObject['requestItems'] = requestItems;

    console.log(requestObject);
    props.setVisible(false);
  };

  const formItemLayout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 5,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 19,
      },
    },
  };

  return (
    <Modal
      title={'Создание нового фильтра'}
      centered
      width={1000}
      closable={false}
      visible={props.visible}
      okText={'Создать фильтр'}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            form.resetFields();
            onFinish(values);
          })
          .catch((info) => {
            console.log('Validate Failed:', info);
          });
      }}
      onCancel={() => {
        props.setVisible(false);
      }}>
      <Form {...formItemLayout} form={form} name="register" onFinish={onFinish} scrollToFirstError>
        <Form.Item
          name="name"
          label="Название фильтра"
          rules={[
            {
              required: true,
              message: 'Пожалуйста введите название фильтра',
            },
          ]}>
          <Input />
        </Form.Item>

        <Form.Item
          name="type"
          label="Типы документов"
          rules={[
            {
              required: true,
              message: 'Пожалуйста выберите типы документов для фильтра',
            },
          ]}>
          <Select mode="multiple" allowClear placeholder="Выберите типы документов">
            {props.filterTypes.map((t) => (
              <Option value={t.filterItemId}>{t.filterValue}</Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          name="entity"
          label="Сущности"
          rules={[
            {
              required: true,
              message: 'Пожалуйста выберите сущности для фильтра',
            },
          ]}>
          <Select mode="multiple" allowClear placeholder="Выберите сущности">
            {props.filterEntities.map((e) => (
              <Option value={e.filterItemId}>{e.filterValue}</Option>
            ))}
          </Select>
        </Form.Item>

        {/* <Form.List name="entity">
          {(fields, { add, remove }) => (
            <>
              {fields.map((field) => (
                <Space key={field.key} align="baseline">
                  <Form.Item
                    shouldUpdate={(prevValues, curValues) =>
                      prevValues.name !== curValues.name || prevValues.value !== curValues.value
                    }>
                    {() => (
                      <Form.Item
                        {...field}
                        label="Имя сущности"
                        name={[field.name, 'name']}
                        rules={[
                          {
                            required: true,
                            message: 'Не выбрано имя сущности',
                          },
                        ]}>
                        <Select
                          style={{
                            width: 130,
                          }}>
                          {props.filterEntities.map((e) => (
                            <Option value={e}>{e}</Option>
                          ))}
                        </Select>
                      </Form.Item>
                    )}
                  </Form.Item>
                  <Form.Item
                    {...field}
                    label="Значение"
                    name={[field.name, 'value']}
                    rules={[
                      {
                        required: true,
                        message: 'Не введено значение сущности',
                      },
                    ]}>
                    <Input />
                  </Form.Item>

                  <MinusCircleOutlined onClick={() => remove(field.name)} />
                </Space>
              ))}

              <Form.Item>
                <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                  Добавить сущность
                </Button>
              </Form.Item>
            </>
          )}
        </Form.List> */}

        <Form.Item
          name="source"
          label="Источники документов"
          rules={[
            {
              required: true,
              message: 'Пожалуйста введите источники документов',
            },
          ]}>
          <Select mode="multiple" allowClear placeholder="Выберите источники документов">
            {props.filterSources.map((s) => (
              <Option value={s.filterItemId}>{s.filterValue}</Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          name="text"
          label="Текст документа"
          rules={[
            {
              required: true,
              message: 'Пожалуйста введите текст',
            },
          ]}>
          <Input.TextArea showCount maxLength={100} />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default CreateFilterFormModal;
