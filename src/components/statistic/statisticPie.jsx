import React from 'react';
import { Pie } from '@ant-design/plots';

const StatisticPie = (props) => {
  const config = {
    height: 50,
    width: 50,
    autoFit: true,
    appendPadding: 6,
    angleField: 'value',
    colorField: 'type',
    radius: 0.8,
    label: {
      type: 'inner',
      style: 'none',
    },
    legend: false,
  };

  const data = [
    {
      type: 'Общие',
      value: props.percent,
    },
    {
      type: 'Уникальные',
      value: 100 - props.percent,
    },
  ];

  return <Pie {...config} data={data} />;
};

export default StatisticPie;
