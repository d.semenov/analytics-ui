import React from 'react';
import { Column } from '@ant-design/plots';

const StatisticColumn = (props) => {
  const Green = '#3f9632';
  const config = {
    width: 1300,
    xField: 'type',
    yField: 'value',
    xAxis: {
      label: {
        autoRotate: false,
      },
    },
    scrollbar: {
      type: 'horizontal',
    },
    label: {
      position: 'middle',
      style: {
        fill: '#000000',
        opacity: 0.8,
      },
    },
    colorField: 'value',
    color: (obj) => {
      if (obj.type !== 'Общий процент плагиата') {
        return Green;
      }
    },

    minColumnWidth: 50,
    maxColumnWidth: 50,
  };

  return <Column {...config} data={props.data} />;
};

export default StatisticColumn;
