import React from 'react';
import { Bar } from '@ant-design/plots';

const StatisticBar = (props) => {
  const config = {
    xField: 'percent',
    yField: 'doc',
    seriesField: 'doc',
    legend: {
      position: 'top-left',
    },
  };
  return <Bar {...config} data={props.data} />;
};

export default StatisticBar;
