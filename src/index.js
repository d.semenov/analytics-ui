import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'antd/dist/antd.css';
import ru_RU from 'antd/lib/locale/ru_RU';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import { BackTop, ConfigProvider } from 'antd';

ReactDOM.render(
  <BrowserRouter basename="/">
    <ConfigProvider locale={ru_RU}>
      <BackTop />
      <App />
    </ConfigProvider>
  </BrowserRouter>,
  document.getElementById('root'),
);


