import ApiConnector from './restClient';
const camunda = 'user-arm';
class ApiAuth {
  static getAuth(pass, store, log) {
    return ApiConnector.postAxios(`${camunda}/auth`, {
      password: pass,
      login: (/\s/g.test(store) ? '' : store + '\\') + log,
    });
  }
  static authType() {
    return ApiConnector.getAxios(`${camunda}/auth-type`);
  }

  static login(login, pass) {
    return ApiConnector.postAxios(`security/login`, {
      username: login,
      password: pass
    });
  }
 
  static validate() {
    return ApiConnector.getAxios(`security/validate`);
  }

  static refreshToken(refreshToken) {
    return ApiConnector.getAxios(`security/refresh-token?refreshToken=${refreshToken}`);
  }
}

export default ApiAuth;
