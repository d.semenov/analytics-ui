import RestClient from './restClient';
class ApiAnalytics {
  static checkDublicate(data) {
    return RestClient.postAnalytics(`check_dublicate_doc_in_db`, data);
  }

  static reindex() {
    return RestClient.postAxios(`indexing-from-db`);
  }

  static indexStatus() {
    return RestClient.getAxios(`indexing-info`);
  }

  static runAnalytic(data) {
    return RestClient.postAxios(`report-build`, data);
  }
}
export default ApiAnalytics;
