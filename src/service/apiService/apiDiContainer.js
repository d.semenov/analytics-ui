import ApiAnalytics from './apiAnalytics';
import ApiAuth from './apiAuth';
import ApiClickHouse from './apiClickHouse';
class ApiDiContainer {
  static ProxyApiClickHouse = ApiClickHouse;
  static ProxyApiAnalytics = ApiAnalytics;
  static ProxyApiAuth = ApiAuth;
}
export default ApiDiContainer;
