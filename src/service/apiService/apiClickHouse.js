import ApiConnector from './restClient';

class ApiClickHouse {
  static getDocs() {
    return ApiConnector.getAxios('docs');
  }

  static getUserDocs(user) {
    return ApiConnector.getAxios('user-docs?user=' + user);
  }

  static getResults() {
    return ApiConnector.getAxios('reports');
  }

  static getResult(uuid) {
    return ApiConnector.getAxios(`report?uuid=${uuid}`, uuid);
  }

  static getOriginalDoc(data) {
    return ApiConnector.postAxios('original-doc', data);
  }
  static getRubric(text) {
    return ApiConnector.postAxios('doc-classifier', text);
  }

  static trainClassifier(data) {
    return ApiConnector.postMultipartAxios(`train-classifier`, data);
  }

  static deleteReport(document) {
    return ApiConnector.deleteAxios('report', document);
  }

  static getEntytyOrg(text) {
    return ApiConnector.postAxios('entity-org', text);
  }

  static getEntyties(request) {
    return ApiConnector.postAxios('entities', request);
  }

  static getDocsCountInDb() {
    return ApiConnector.getAxios('total-in-db');
  }

  static addParagraph(paragraph) {
    return ApiConnector.postAxios('paragraph', paragraph);
  }

  static deleteParagraph(paragraph) {
    return ApiConnector.deleteAxios('request-item', paragraph);
  }

  static getParagraph(filterItem) {
    return ApiConnector.getAxios('filter-items-option?filterItemId=' + filterItem);
  }

  static startIndexing() {
    return ApiConnector.postAxios('indexing-from-db');
  }

  static getDocsCountIndexed() {
    return ApiConnector.getAxios('indexed-docs-count');
  }

  static getDocsPage(pageNumber, pageSize, sortField, sortOrder, searchParams) {
    let params =
      '?pageNumber=' +
      pageNumber +
      '&pageSize=' +
      pageSize +
      '&sortField=' +
      sortField +
      '&sortOrder=' +
      sortOrder;
    for (const param of searchParams) {
      params += '&searchKey=' + param.searchKey;
    }
    for (const param of searchParams) {
      params += '&searchValue=' + param.searchValue;
    }
    return ApiConnector.getAxios('docs' + params);
  }

  static getDoc(id) {
    return ApiConnector.postAxios('text', id);
  }

  static getFiltersList() {
    return ApiConnector.getAxios('requests');
  }

  static saveFilter(filter) {
    return ApiConnector.postAxios('request', filter);
  }

  static deleteFilter(filter) {
    return ApiConnector.deleteAxios('request', filter);
  }

  static removeParagraph(paragraph) {
    return ApiConnector.deleteAxios('filter-item', paragraph);
  }

  static getTypesList() {
    return ApiConnector.getAxios('doc-types');
  }

  static getEntitiesList() {
    return ApiConnector.getAxios('doc-entities');
  }

  static getSourcesList() {
    return ApiConnector.getAxios('doc-sources');
  }

  static getParagraphsList() {
    return ApiConnector.getAxios('doc-paragraph');
  }

  static getDocAnalytics(id) {
    return ApiConnector.postAxios('analytics', id);
  }

  static findText(data) {
    return ApiConnector.postAxios('search', data);
  }

  static checkDuplicate(data) {
    return ApiConnector.postAxios(`dublicates`, data);
  }

  static textFromFile(data) {
    return ApiConnector.postMultipartAxios(`text-from-file`, data);
  }

  static uploadUserDocFromFile(data) {
    return ApiConnector.postMultipartAxios(`userdoc-upload`, data);
  }

  static uploadUserDocFromDb(data) {
    return ApiConnector.postAxios(`userdoc-upload-from-db`, data);
  }

  static getUserDoc(uid) {
    return ApiConnector.getAxios(`user-doc?uid=${uid}`, uid);
  }

  static getTotalParts(id) {
    return ApiConnector.getAxios(`parts-in-docs-count?docId=${id}`, id);
  }

  static deleteUserDoc(document) {
    return ApiConnector.deleteAxios('user-doc', document);
  }
}
export default ApiClickHouse;
