import config from '../config/config.json';
// import { notification } from 'antd';
import axios from 'axios';
import { baseUrl } from '../../../package.json';

window.externalConfigChecked = false; // flag for externalized config check
window.externalConfigAlerted = false;

const headersForSearch = new Headers({
  'content-type': 'application/json',
  Authorization: 'Basic ' + btoa('asdf:ewevvyBwfhtGNDCpPfnXAj'),
});

export const getExternalConfig = async () => {
  console.log(`boolean`, window.externalConfigChecked);
  console.log('base url', baseUrl);
  if (!window.externalConfigChecked) {
    await fetch(baseUrl + 'conf/config.json') // this maybe should be additional nginx with confs
      .then(async (resp) => {
        await console.log(`resp:`, resp);
        if (resp.ok) {
          try {
            window.externalConfig = await resp.json();
            console.log(window.externalConfig);
          } catch {
            console.log('Got external config for api but Json.parse of it fails');
          }
          return window.externalConfig;
        } else console.warn('Fetch external config failed');
      })
      .catch((error) => {
        console.log('error', error);
        console.log(error);
      });
    window.externalConfigChecked = true;
  } else {
    console.log('External config fetch already performed', window.externalConfig);
    return window.externalConfig;
  }
};

// getExternalConfig();

function makeUrls() {
  // await getExternalConfig();
  try {
    if (
      window.externalConfig.mainApi &&
      window.externalConfig.mainApi.host &&
      window.externalConfig.mainApi.server &&
      window.externalConfig.mainApi.port &&
      window.externalConfig.mainApi.port.length &&
      window.externalConfig.mainApi.api
    ) {
      //console.log('External api config provided. Will use external config', window.externalConfig);
      return (
        window.externalConfig.mainApi.host +
        '://' +
        window.externalConfig.mainApi.server +
        (window.externalConfig.mainApi.port.length > 0
          ? ':' + window.externalConfig.mainApi.port + '/'
          : '/') +
        window.externalConfig.mainApi.api +
        '/'
      );
    }
  } catch {
    if (!window.externalConfigAlerted) {
      console.warn(
        'There is some problems with external mainApi config, it may be empty, or not well formed. Check src/service/config/config.json for example of valid config file',
      );
      window.externalConfigAlerted = true;
    }
  }

  return (
    config.mainApi.host +
    '://' +
    config.mainApi.server +
    (config.mainApi.port.length > 0 ? ':' + config.mainApi.port + '/' : '/') +
    config.mainApi.api +
    '/'
  );
}

function makeUrlsForAnalytics() {
  try {
    if (
      window.externalConfig.analyticsApi &&
      window.externalConfig.analyticsApi.host &&
      window.externalConfig.analyticsApi.server &&
      window.externalConfig.analyticsApi.port &&
      window.externalConfig.analyticsApi.port.length &&
      window.externalConfig.analyticsApi.api
    ) {
      return (
        window.externalConfig.analyticsApi.host +
        '://' +
        window.externalConfig.analyticsApi.server +
        (window.externalConfig.analyticsApi.port.length > 0
          ? ':' + window.externalConfig.analyticsApi.port + '/'
          : '/') +
        window.externalConfig.analyticsApi.api +
        '/'
      );
    }
  } catch {
    if (!window.externalConfigAlerted) {
      console.warn(
        'There is some problems with external search config, it may be empty, or not well formed. Check src/service/config/config.json for example of valid config file',
      );
      window.externalConfigAlerted = true;
    }
  }
  return (
    config.analyticsApi.host +
    '://' +
    config.analyticsApi.server +
    (config.analyticsApi.port.length > 0 ? ':' + config.analyticsApi.port + '/' : '/') +
    (config.analyticsApi.api.length > 0 ? config.analyticsApi.api + '/' : '')
  );
}

function makeUrlsForSearch() {
  try {
    if (
      window.externalConfig.searchApi &&
      window.externalConfig.searchApi.host &&
      window.externalConfig.searchApi.server &&
      window.externalConfig.searchApi.port &&
      window.externalConfig.searchApi.port.length &&
      window.externalConfig.searchApi.api
    ) {
      return (
        window.externalConfig.searchApi.host +
        '://' +
        window.externalConfig.searchApi.server +
        (window.externalConfig.searchApi.port.length > 0
          ? ':' + window.externalConfig.searchApi.port + '/'
          : '/') +
        window.externalConfig.searchApi.api +
        '/'
      );
    }
  } catch {
    if (!window.externalConfigAlerted) {
      console.warn(
        'There is some problems with external search config, it may be empty, or not well formed. Check src/service/config/config.json for example of valid config file',
      );
      window.externalConfigAlerted = true;
    }
  }
  return (
    config.searchApi.host +
    '://' +
    config.searchApi.server +
    (config.searchApi.port.length > 0 ? ':' + config.searchApi.port + '/' : '/') +
    config.searchApi.api +
    '/'
  );
}

function makeUrlsForRedirectToSearch() {
  try {
    if (
      window.externalConfig.searchApi &&
      window.externalConfig.searchApi.host &&
      window.externalConfig.searchApi.server &&
      window.externalConfig.searchApi.port &&
      window.externalConfig.searchApi.port.length &&
      window.externalConfig.searchApi.guiUrlPrefix &&
      window.externalConfig.searchApi.guiUrlPrefix.length
    ) {
      return (
        window.externalConfig.searchApi.host +
        '://' +
        window.externalConfig.searchApi.server +
        (window.externalConfig.searchApi.port.length > 0
          ? ':' + window.externalConfig.searchApi.port + '/'
          : '/') +
        (window.externalConfig.searchApi.guiUrlPrefix.length > 0
          ? window.externalConfig.searchApi.guiUrlPrefix + '/'
          : '')
      );
    }
  } catch {
    if (!window.externalConfigAlerted) {
      console.warn(
        'There is some problems with external search config, it may be empty, or not well formed. Check src/service/config/config.json for example of valid config file',
      );
      window.externalConfigAlerted = true;
    }
  }
  return (
    config.searchApi.host +
    '://' +
    config.searchApi.server +
    (config.searchApi.port.length > 0 ? ':' + config.searchApi.port + '/' : '/') +
    (config.searchApi.guiUrlPrefix.length > 0 ? config.searchApi.guiUrlPrefix + '/' : '')
  );
}

function makeUrlsForRedirectToDiffer() {
  try {
    if (
      window.externalConfig.differApi &&
      window.externalConfig.differApi.host &&
      window.externalConfig.differApi.server &&
      window.externalConfig.differApi.port &&
      window.externalConfig.differApi.port.length //&&
      // window.externalConfig.differApi.guiUrlPrefix &&
      // window.externalConfig.differApi.guiUrlPrefix.length
    ) {
      return (
        window.externalConfig.differApi.host +
        '://' +
        window.externalConfig.differApi.server +
        (window.externalConfig.differApi.port.length > 0
          ? ':' + window.externalConfig.differApi.port + '/'
          : '/') +
        (window.externalConfig.differApi.guiUrlPrefix.length > 0
          ? window.externalConfig.differApi.guiUrlPrefix + '/'
          : '')
      );
    }
  } catch {
    if (!window.externalConfigAlerted) {
      console.warn(
        'There is some problems with external search config, it may be empty, or not well formed. Check src/service/config/config.json for example of valid config file',
      );
      window.externalConfigAlerted = true;
    }
  }
  return (
    config.differApi.host +
    '://' +
    config.differApi.server +
    (config.differApi.port.length > 0 ? ':' + config.differApi.port + '/' : '/') +
    (config.differApi.guiUrlPrefix.length > 0 ? config.differApi.guiUrlPrefix + '/' : '')
  );
}

function makeUrlsForRedirectToSimilar() {
  try {
    if (
      window.externalConfig.similarApi &&
      window.externalConfig.similarApi.host &&
      window.externalConfig.similarApi.server &&
      window.externalConfig.similarApi.port &&
      window.externalConfig.similarApi.port.length //&&
      // window.externalConfig.similarApi.guiUrlPrefix &&
      // window.externalConfig.similarApi.guiUrlPrefix.length
    ) {
      return (
        window.externalConfig.similarApi.host +
        '://' +
        window.externalConfig.similarApi.server +
        (window.externalConfig.similarApi.port.length > 0
          ? ':' + window.externalConfig.similarApi.port + '/'
          : '/') +
        (window.externalConfig.similarApi.guiUrlPrefix.length > 0
          ? window.externalConfig.similarApi.guiUrlPrefix + '/'
          : '')
      );
    }
  } catch {
    if (!window.externalConfigAlerted) {
      console.warn(
        'There is some problems with external search config, it may be empty, or not well formed. Check src/service/config/config.json for example of valid config file',
      );
      window.externalConfigAlerted = true;
    }
  }
  return (
    config.similarApi.host +
    '://' +
    config.similarApi.server +
    (config.similarApi.port.length > 0 ? ':' + config.similarApi.port + '/' : '/') +
    (config.similarApi.guiUrlPrefix.length > 0 ? config.similarApi.guiUrlPrefix + '/' : '')
  );
}

class RestClient {
  static get(url) {
    return fetch(makeUrls() + url, {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('authenticatedToken'),
      },
    })
      .then((response) => response.json())
      .catch(
        (err) => ({}),
        // notification.error({
        //   message: err.message,
        // }),
      );
  }

  static getFile(url) {
    return fetch(makeUrls() + url, {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('authenticatedToken'),
      },
    });
  }

  static getAxios(url) {
    const config = {
      method: 'get',
      url: `${makeUrls() + url}`,
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('authenticatedToken'),
      },
    };

    return axios(config);
  }

  static getAxiosBlob(url) {
    const config = {
      method: 'get',
      responseType: 'blob',
      url: `${makeUrls() + url}`,
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('authenticatedToken'),
        'Access-Control-Expose-Headers': 'Content-Disposition',
      },
    };
    return axios(config);
  }

  static postAxiosBlob(url, id) {
    const config = {
      method: 'post',
      responseType: 'blob',
      url: `${makeUrls() + url}`,
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('authenticatedToken'),
      },
      data: id,
    };
    return axios(config);
  }

  static postAxios(url, id) {
    const config = {
      method: 'post',
      mode: 'no-cors',
      url: `${makeUrls() + url}`,

      headers: localStorage.getItem('authenticatedToken')
        ? {
            Authorization: 'Bearer ' + localStorage.getItem('authenticatedToken'),
            
          }
        : null,
      data: id,
    };
    return axios(config);
  }

  static patchAxios(url, id) {
    const config = {
      method: 'patch',
      url: `${makeUrls() + url}`,

      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('authenticatedToken'),
      },
      data: id,
    };
    return axios(config);
  }

  static async putAxios(url, id) {
    const config = {
      method: 'put',
      url: `${makeUrls() + url}`,
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('authenticatedToken'),
      },
      data: id,
    };
    return axios(config);
  }

  static async deleteAxios(url, id) {
    const config = {
      method: 'delete',
      url: `${makeUrls() + url}`,
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('authenticatedToken'),
      },
      data: id,
    };
    return axios(config);
  }

  static postMultipartAxios(url, data) {
    console.log(`are you here`, url, data);
    const config = {
      method: 'post',
      url: `${makeUrls() + url}`,
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + localStorage.getItem('authenticatedToken'),
      },
      data: data,
    };

    return axios(config);
  }

  //For Another Server Connection
  static getAnalytics(url) {
    return fetch(makeUrlsForAnalytics() + url, {
      method: 'get',
      headers: headersForSearch,
    }).then((response) => response.json());
  }

  static postAnalytics(url, body) {
    return fetch(makeUrlsForAnalytics() + url, {
      method: 'post',
      mode: 'cors',
      headers: headersForSearch,
      body: JSON.stringify(body),
    }).then((response) => response.json());
  }

  //For Another Server Connection
  static getSearch(url) {
    return fetch(makeUrlsForSearch() + url, {
      method: 'get',
      headers: headersForSearch,
    }).then((response) => response.json());
  }

  static postSearch(url, body) {
    return fetch(makeUrlsForSearch() + url, {
      method: 'post',
      mode: 'cors',
      headers: headersForSearch,
      body: JSON.stringify(body),
    }).then((response) => response.json());
  }

  static getFileName(response) {
    const fileNameStartIndex = response.indexOf("''") + 2;
    return decodeURI(response.slice(fileNameStartIndex));
  }

  static getSearchUrl() {
    return makeUrlsForSearch();
  }

  static getSearchRedirectUrl() {
    return makeUrlsForRedirectToSearch();
  }

  static getDifferRedirectUrl() {
    return makeUrlsForRedirectToDiffer();
  }

  static getSimilarRedirectUrl() {
    return makeUrlsForRedirectToSimilar();
  }

  static getAnalyticsUrl() {
    return makeUrlsForAnalytics();
  }
}

export default RestClient;
