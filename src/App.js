import React from 'react';
import './App.css';
import AnalyticMain from './layout/AnalyticMain/AnalyticMain';
import { withRouter } from 'react-router-dom';

import { getExternalConfig } from './service/apiService/restClient';
import AuthModule from './layout/modules/auth/authModule';

import { Spin } from 'antd';
import ApiDiContainer from './service/apiService/apiDiContainer';
import QualifierModule from './layout/modules/qualifier/qualifierModule';

class App extends React.Component {
  state = {
    showLogin: false,
    loading: false,
    errorCode: '',
    logged: false,
    firstName: '',
    user: '',
    fatherName: '',
    lastName: '',
    login: '',
  };
  componentDidMount = async () => {
    //    await getExternalConfig(); // on start wait for loading of externalized config. If it not present local will be used

    this.validate();
  };
  validate = () => {
    if (localStorage.getItem('authenticatedToken')) {
      ApiDiContainer.ProxyApiAuth.validate()
        .then((res) => {
          this.setState({
            logged: true,
            user: res.data.username,
            isAdmin: (res.data.authorities.filter(f => f.authority.includes("admin")).length > 0)
          });
        })
        .catch((error) => {
          if (error.response) {
            // Request made and server responded
            if (error.response.status === 403) {
              this.setState({
                logged: false,
              });
            }
          } else if (error.request) {
            // The request was made but no response was received
            console.log(error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log('Error', error.message);
          }
        });
    }
  };
  loginUser = (log, pass, store) => {
    this.setState({ loading: true });

    ApiDiContainer.ProxyApiAuth.login(log, pass, store)
      .then(async (res) => {
        await localStorage.setItem('authenticatedToken', res.data.access_token);
        await localStorage.setItem('refreshToken', res.data.refresh_token);

        this.validate();
      })
      .catch((er) => {
        console.log(er);
        this.setState({ errorCode: -20, loading: false });
      });
  };
  exitUser = () => {
    if (localStorage.getItem('refreshToken')) {
      ApiDiContainer.ProxyApiAuth.refreshToken(localStorage.getItem('refreshToken'))
        .then(async (res) => {
          console.log('refresh token');
          await localStorage.setItem('authenticatedToken', res.data.access_token);
        })
        .catch((error) => {
          if (error.response) {
            // Request made and server responded
            console.log(error.response);
          } else if (error.request) {
            // The request was made but no response was received
            console.log(error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log('Error', error.message);
          }
          this.setState({
            logged: false,
          });
          localStorage.removeItem('authenticatedToken');
          localStorage.removeItem('refreshToken');
        });
    } else {
      this.setState({
        logged: false,
      });
      localStorage.removeItem('authenticatedToken');
      localStorage.removeItem('refreshToken');
    }
  };

  logout = () => {
    localStorage.removeItem('authenticatedToken');
    localStorage.removeItem('refreshToken');
    this.setState({
      logged: false,
    });
  };

  render() {
    return (
      <>
        {this.state.logged ? (
          <AnalyticMain user={this.state.user} 
                        exitUser={this.exitUser} 
                        logout={this.logout} 
                        validate={this.validate} 
                        isAdmin={this.state.isAdmin}
          />
        ) : (
          <AuthModule loginUser={this.loginUser} errorCode={this.state.errorCode} />
        )}
      </>
    );
  }
}

export default withRouter(App);
